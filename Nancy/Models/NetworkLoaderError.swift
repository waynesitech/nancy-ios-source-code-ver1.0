//
//  NetworkLoaderError.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 18/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

class NetworkLoaderError {
    var networkLoaderErrorTitle: String = ""
    var networkLoaderErrorBody: String = ""
    
    init (networkLoaderErrorTitle: String, networkLoaderErrorBody: String) {
        self.networkLoaderErrorTitle = networkLoaderErrorTitle
        self.networkLoaderErrorBody = networkLoaderErrorBody
    }
}
