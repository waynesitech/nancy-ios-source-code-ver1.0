//
//  SecurityDeposit.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 10/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class SecurityDeposit : NSObject, NSCoding {
    
    var amount: Double = 0.0
    var name: String = ""
    
    func encode(with aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encode(self.amount, forKey: "SecurityDepositAmount")
        keyedArchiver.encode(self.name, forKey: "SecurityDepositName")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let amount = keyedUnarchiver.decodeDouble(forKey: "SecurityDepositAmount")
        let name = keyedUnarchiver.decodeObject(forKey: "SecurityDepositName") as! String
        
        self.init(name: name, amount: amount)
    }
    
    func convertToJSON() -> [String: Any] {
        let dictionary = [
            "name":  self.name,
            "amount":  self.amount
        ] as [String : Any]
        return dictionary
    }
    
    init(name: String, amount: Double) {
        self.name = name
        self.amount = amount
    }
    
    override init() {
    }
    
    convenience init?(jsonResult: [String : Any]) {
        self.init()
        if let amount = jsonResult["amount"] as? Double {
            self.amount = amount
        }
        if let name = jsonResult["name"] as? String {
            self.name = name
        }
    }
}
