//
//  LandlordTenant.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 10/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

class LandlordTenant : NSObject, NSCoding {
    
    var isCompany: Bool = false
    var companyName: String = ""
    var companyRegNum: String = ""
    var name: String = ""
    var icNum: String = ""
    var address: String = ""
    var countryCode: Country = Country(code: "MY")
    var contactNum: String = ""
    var email: String = ""
    
    func encode(with aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encode(self.isCompany, forKey: "IsCompany")
        keyedArchiver.encode(self.companyName, forKey: "CompanyName")
        keyedArchiver.encode(self.companyRegNum, forKey: "CompanyRegistrationNumber")
        keyedArchiver.encode(self.name, forKey: "PeopleName")
        keyedArchiver.encode(self.icNum, forKey: "PeopleICNumber")
        keyedArchiver.encode(self.address, forKey: "PeopleAddress")
        keyedArchiver.encode(self.countryCode, forKey: "CountryCode")
        keyedArchiver.encode(self.contactNum, forKey: "PeopleContactNumber")
        keyedArchiver.encode(self.email, forKey: "PeopleEmail")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let isCompany = keyedUnarchiver.decodeBool(forKey: "IsCompany")
        let companyName = keyedUnarchiver.decodeObject(forKey: "CompanyName") as! String
        let companyRegNum = keyedUnarchiver.decodeObject(forKey: "CompanyRegistrationNumber") as! String
        let name = keyedUnarchiver.decodeObject(forKey: "PeopleName") as! String
        let icNum = keyedUnarchiver.decodeObject(forKey: "PeopleICNumber") as! String
        let address = keyedUnarchiver.decodeObject(forKey: "PeopleAddress") as! String
        let countryCode = keyedUnarchiver.decodeObject(forKey: "CountryCode") as! Country
        let contactNum = keyedUnarchiver.decodeObject(forKey: "PeopleContactNumber") as! String
        let email = keyedUnarchiver.decodeObject(forKey: "PeopleEmail") as! String
        self.init(isCompany: isCompany, companyName: companyName, companyRegNum: companyRegNum,
                  name: name, icNum: icNum, address: address, countryCode: countryCode, contactNum: contactNum, email: email)
    }
    
    func convertToJSON() -> [String: String] {
        let dictionary = [
            "isCompany":  self.isCompany.description,
            "companyName":  self.companyName,
            "companyRegNum":  self.companyRegNum,
            "name":  self.name,
            "icNum":  self.icNum,
            "address":  self.address,
            "contactNum":  self.getValidPhoneNumber(validNumber: self.contactNum),
            "email":  self.email
        ]
        return dictionary
    }
    
    func getValidPhoneNumber(validNumber: String) -> String {
        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil!.parse(validNumber, defaultRegion: self.countryCode.code)
            let formattedString: String = try phoneUtil!.format(phoneNumber, numberFormat: .E164)
            return formattedString
        } catch {
            return ""
        }
    }
    
    init(isCompany: Bool, companyName: String, companyRegNum: String,
         name: String, icNum: String, address: String, countryCode: Country, contactNum: String, email: String) {
        self.isCompany = isCompany
        self.companyName = companyName
        self.companyRegNum = companyRegNum
        self.name = name
        self.icNum = icNum
        self.address = address
        self.countryCode = countryCode
        self.contactNum = contactNum
        self.email = email
    }
    
    override init() {
        
    }
    
    convenience init?(jsonResult: [String : Any]) {
        self.init()
        if let isCompany = jsonResult["isCompany"] as? Bool {
            self.isCompany = isCompany
            if let companyName = jsonResult["companyName"] as? String {
                self.companyName = companyName
            }
            if let companyRegNum = jsonResult["companyRegNum"] as? String {
                self.companyRegNum = companyRegNum
            }
        }
        if let name = jsonResult["name"] as? String {
            self.name = name
        }
        if let icNum = jsonResult["icNum"] as? String {
            self.icNum = icNum
        }
        if let address = jsonResult["address"] as? String {
            self.address = address
        }
        if let contactNum = jsonResult["contactNum"] as? String {
            self.contactNum = contactNum
        }
        if let email = jsonResult["email"] as? String {
            self.email = email
        }
    }
}
