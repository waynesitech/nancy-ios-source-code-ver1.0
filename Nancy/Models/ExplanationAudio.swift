//
//  ExplanationAudio.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 14/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class ExplanationAudio {
    var explanationNumber: String = ""
    var explanationText: String = ""
    var explanationURL: String = ""
    
    init() {}
    
    convenience init?(jsonResult: [String : Any]) {
        self.init()
        if let explanationNumber = jsonResult["explanationNumber"] as? String {
            self.explanationNumber = explanationNumber
        }
        if let explanationText = jsonResult["explanationText"] as? String {
            self.explanationText = explanationText
        }
        if let explanationURL = jsonResult["explanationURL"] as? String {
            self.explanationURL = explanationURL
        }
    }
}
