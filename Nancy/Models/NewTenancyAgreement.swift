//
//  NewTenancyAgreement.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 10/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class NewTenancyAgreement : NSObject, NSCoding {
    
    // Is Edit Tenancy Agreement
    var isEditTA: Bool = false
    var orderID: String = ""
    
    // Upload Tenancy Agreement
    var tenancyAgreementPDF: Data?
    var uploadType: String = ""
    
    // Current Step of New Tenancy Agreement
    var currentStep: Int = -1
    
    // 1 - Property Details
    // 2 - isLandlord = Landlord / !isLandlord = Tenant
    // 3 - isLandlord = Tenant / !isLandlord = Landlord
    // 4 - Rental Details
    // 5 - Payment Details
    // 6 - Special Conditions
    // 7 - Fixtures
    // 8 - Preview
    var isLandlord: Bool = false
    
    // Property Details
    var streetName: String = ""
    var town: String = ""
    var postCode: String = ""
    var states: String = ""
    var isResidential: Bool = true
    
    // Rent Details
    var commencementDate: Date = Date()
    var termsOfTenancyMonths: Int = 0
    var termsOfTenancyYears: Int = 0
    
    // Options to Renew
    var isOptionToRenew: Bool = false
    var optionToRenewMonths: Int = 0
    var optionToRenewYears: Int = 0
    var isMarketPrevaillingRate: Bool = true
    var isIncreaseOf10percent: Bool = false
    var isIncreaseOf15percent: Bool = false
    var isMutuallyAgreedRate: Bool = false
    var isOtherPercent: Bool = false
    var otherTermsofRenewal: String = ""
    
    var rental: Double = 0.0
    var advanceRental: Double = 0.0
    
    // Security Deposit
    var securityDepositRent: Double = 0.0
    var securityDepositUtilities: Double = 0.0
    var otherSecurityDeposit: [SecurityDeposit] = []
    
    // Rental Free Related
    var isRentalFreePeriodDuration: Bool = true
    var rentalFreePeriodWeeks: Int = 0
    var rentalFreePeriodMonths: Int = 0
    var rentalFreePeriodYears: Int = 0
    var rentalFreePeriodStartDate: Date = Date()
    var rentalFreePeriodEndDate: Date = Date()
    
    // Car Park Related
    var carParkLots: [String] = []
    
    // Payment Method
    var paymentAccountName: String = ""
    var bank: String = ""
    var bankAccountNum: String = ""
    
    var paymentMethod: Int = 0
    // 0 = isDirectBankIn
    // 1 = isPostDatedCheque
    // 2 = isCollectionByLandlord
    
    var notificationMethod: Int = 0
    // 0 = isEmail
    // 1 = isSMS
    // 2 = isWhatsapp
    
    var postDatedMethod: Int = 0
    // 0 = is6Months
    // 1 = is12Months
    // 2 = is24Months
    
    // LandLords
    var landlords: [LandlordTenant] = []
    
    // Tenants
    var tenants: [LandlordTenant] = []
    
    // Special Conditions
    var specialRequest: [String] = [""]
    
    // Fixtures
    var fixturesFitting: [String] = [""]
    
    
    func encode(with aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encode(self.isEditTA, forKey:"IsEditTA")
        keyedArchiver.encode(self.orderID, forKey:"OrderID")
        keyedArchiver.encode(self.tenancyAgreementPDF, forKey: "TenancyAgreementPDF")
        keyedArchiver.encode(self.uploadType, forKey: "UploadType")
        keyedArchiver.encode(self.currentStep, forKey: "CurrentStep")
        keyedArchiver.encode(self.isLandlord, forKey: "IsLandlord")
        keyedArchiver.encode(self.streetName, forKey: "StreetName")
        keyedArchiver.encode(self.town, forKey: "Town")
        keyedArchiver.encode(self.postCode, forKey: "PostCode")
        keyedArchiver.encode(self.states, forKey: "States")
        keyedArchiver.encode(self.isResidential, forKey: "IsResidential")
        keyedArchiver.encode(self.commencementDate, forKey: "CommencementDate")
        keyedArchiver.encode(self.termsOfTenancyMonths, forKey: "TermsOfTenancyMonths")
        keyedArchiver.encode(self.termsOfTenancyYears, forKey: "TermsOfTenancyYears")
        keyedArchiver.encode(self.isOptionToRenew, forKey: "IsOptionToRenew")
        keyedArchiver.encode(self.optionToRenewMonths, forKey: "OptionToRenewMonths")
        keyedArchiver.encode(self.optionToRenewYears, forKey: "OptionToRenewYears")
        keyedArchiver.encode(self.isMarketPrevaillingRate, forKey: "IsMarketPrevaillingRate")
        keyedArchiver.encode(self.isIncreaseOf10percent, forKey: "IsIncreaseOf10percent")
        keyedArchiver.encode(self.isIncreaseOf15percent, forKey: "IsIncreaseOf15percent")
        keyedArchiver.encode(self.isMutuallyAgreedRate, forKey: "IsMutuallyAgreedRate")
        keyedArchiver.encode(self.isOtherPercent, forKey: "IsOtherPercent")
        keyedArchiver.encode(self.otherTermsofRenewal, forKey: "OtherTermsofRenewal")
        keyedArchiver.encode(self.rental, forKey: "Rental")
        keyedArchiver.encode(self.advanceRental, forKey: "AdvanceRental")
        keyedArchiver.encode(self.securityDepositRent, forKey: "SecurityDepositRent")
        keyedArchiver.encode(self.securityDepositUtilities, forKey: "SecurityDepositUtilities")
        keyedArchiver.encode(self.otherSecurityDeposit, forKey:"OtherSecurityDeposit")
        keyedArchiver.encode(self.isRentalFreePeriodDuration, forKey:"IsRentalFreePeriodDuration")
        keyedArchiver.encode(self.rentalFreePeriodWeeks, forKey:"RentalFreePeriodWeeks")
        keyedArchiver.encode(self.rentalFreePeriodMonths, forKey: "RentalFreePeriodMonths")
        keyedArchiver.encode(self.rentalFreePeriodYears, forKey: "RentalFreePeriodYears")
        keyedArchiver.encode(self.rentalFreePeriodStartDate, forKey: "RentalFreePeriodStartDate")
        keyedArchiver.encode(self.rentalFreePeriodEndDate, forKey: "RentalFreePeriodEndDate")
        keyedArchiver.encode(self.carParkLots, forKey: "CarParkLots")
        keyedArchiver.encode(self.paymentAccountName, forKey: "PaymentAccountName")
        keyedArchiver.encode(self.bank, forKey: "Bank")
        keyedArchiver.encode(self.bankAccountNum, forKey: "BankAccountNum")
        keyedArchiver.encode(self.paymentMethod, forKey: "PaymentMethod")
        keyedArchiver.encode(self.notificationMethod, forKey: "NotificationMethod")
        keyedArchiver.encode(self.postDatedMethod, forKey: "PostDatedMethod")
        keyedArchiver.encode(self.landlords, forKey: "Landlords")
        keyedArchiver.encode(self.tenants, forKey: "Tenants")
        keyedArchiver.encode(self.specialRequest, forKey: "SpecialRequest")
        keyedArchiver.encode(self.fixturesFitting, forKey: "FixturesFitting")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let currentStep = keyedUnarchiver.decodeInteger(forKey: "CurrentStep")
        let isLandlord = keyedUnarchiver.decodeBool(forKey: "IsLandlord")
        let streetName = keyedUnarchiver.decodeObject(forKey: "StreetName") as! String
        let town = keyedUnarchiver.decodeObject(forKey: "Town") as! String
        let postCode = keyedUnarchiver.decodeObject(forKey: "PostCode") as! String
        let states = keyedUnarchiver.decodeObject(forKey: "States") as! String
        let isResidential = keyedUnarchiver.decodeBool(forKey: "IsResidential")
        let commencementDate = keyedUnarchiver.decodeObject(forKey: "CommencementDate") as! Date
        let termsOfTenancyMonths = keyedUnarchiver.decodeInteger(forKey: "TermsOfTenancyMonths")
        let termsOfTenancyYears = keyedUnarchiver.decodeInteger(forKey: "TermsOfTenancyYears")
        let isOptionToRenew = keyedUnarchiver.decodeBool(forKey: "IsOptionToRenew")
        let optionToRenewMonths = keyedUnarchiver.decodeInteger(forKey: "OptionToRenewMonths")
        let optionToRenewYears = keyedUnarchiver.decodeInteger(forKey: "OptionToRenewYears")
        let isMarketPrevaillingRate = keyedUnarchiver.decodeBool(forKey: "IsMarketPrevaillingRate")
        let isIncreaseOf10percent = keyedUnarchiver.decodeBool(forKey: "IsIncreaseOf10percent")
        let isIncreaseOf15percent = keyedUnarchiver.decodeBool(forKey: "IsIncreaseOf15percent")
        let isMutuallyAgreedRate = keyedUnarchiver.decodeBool(forKey: "IsMutuallyAgreedRate")
        let isOtherPercent = keyedUnarchiver.decodeBool(forKey: "IsOtherPercent")
        let otherTermsofRenewal = keyedUnarchiver.decodeObject(forKey: "OtherTermsofRenewal") as! String
        let rental = keyedUnarchiver.decodeDouble(forKey: "Rental")
        let advanceRental = keyedUnarchiver.decodeDouble(forKey: "AdvanceRental")
        let securityDepositRent = keyedUnarchiver.decodeDouble(forKey: "SecurityDepositRent")
        let securityDepositUtilities = keyedUnarchiver.decodeDouble(forKey: "SecurityDepositUtilities")
        let otherSecurityDeposit = keyedUnarchiver.decodeObject(forKey: "OtherSecurityDeposit") as! [SecurityDeposit]
        let isRentalFreePeriodDuration = keyedUnarchiver.decodeBool(forKey: "IsRentalFreePeriodDuration")
        let rentalFreePeriodWeeks = keyedUnarchiver.decodeInteger(forKey: "RentalFreePeriodWeeks")
        let rentalFreePeriodMonths = keyedUnarchiver.decodeInteger(forKey: "RentalFreePeriodMonths")
        let rentalFreePeriodYears = keyedUnarchiver.decodeInteger(forKey: "RentalFreePeriodYears")
        var rentalFreePeriodStartDate = Date()
        if let tempDate = keyedUnarchiver.decodeObject(forKey: "RentalFreePeriodStartDate") as? Date {
            rentalFreePeriodStartDate = tempDate
        }
        var rentalFreePeriodEndDate = Date()
        if let tempDate = keyedUnarchiver.decodeObject(forKey: "RentalFreePeriodEndDate") as? Date {
            rentalFreePeriodEndDate = tempDate
        }
        let carParkLots = keyedUnarchiver.decodeObject(forKey: "CarParkLots") as! [String]
        let paymentAccountName = keyedUnarchiver.decodeObject(forKey: "PaymentAccountName") as! String
        let bank = keyedUnarchiver.decodeObject(forKey: "Bank") as! String
        let bankAccountNum = keyedUnarchiver.decodeObject(forKey: "BankAccountNum") as! String
        let paymentMethod = keyedUnarchiver.decodeInteger(forKey: "PaymentMethod")
        let notificationMethod = keyedUnarchiver.decodeInteger(forKey: "NotificationMethod")
        let postDatedMethod = keyedUnarchiver.decodeInteger(forKey: "PostDatedMethod")
        let landlords = keyedUnarchiver.decodeObject(forKey: "Landlords") as! [LandlordTenant]
        let tenants = keyedUnarchiver.decodeObject(forKey: "Tenants") as! [LandlordTenant]
        let specialRequest = keyedUnarchiver.decodeObject(forKey: "SpecialRequest") as! [String]
        let fixturesFitting = keyedUnarchiver.decodeObject(forKey: "FixturesFitting") as! [String]
        
        
        let isEditTA: Bool = keyedUnarchiver.decodeBool(forKey: "IsEditTA")
        var orderID: String = ""
        if let tempOrderID = keyedUnarchiver.decodeObject(forKey: "OrderID") as? String {
            orderID = tempOrderID
        }
        
        if let tenancyAgreementPDF = keyedUnarchiver.decodeObject(forKey: "TenancyAgreementPDF") as? Data {
            let uploadType = keyedUnarchiver.decodeObject(forKey: "UploadType") as! String
            self.init(tenancyAgreementPDF: tenancyAgreementPDF, uploadType: uploadType, isEditTA: isEditTA, orderID: orderID, currentStep: currentStep, isLandlord: isLandlord, streetName: streetName, town: town, postCode: postCode, states: states, isResidential: isResidential, commencementDate: commencementDate, termsOfTenancyMonths: termsOfTenancyMonths, termsOfTenancyYears: termsOfTenancyYears, isOptionToRenew: isOptionToRenew, optionToRenewMonths: optionToRenewMonths, optionToRenewYears: optionToRenewYears, isMarketPrevaillingRate: isMarketPrevaillingRate, isIncreaseOf10percent: isIncreaseOf10percent, isIncreaseOf15percent: isIncreaseOf15percent, isMutuallyAgreedRate: isMutuallyAgreedRate, isOtherPercent: isOtherPercent, otherTermsofRenewal: otherTermsofRenewal, rental: rental, advanceRental: advanceRental, securityDepositRent: securityDepositRent, securityDepositUtilities: securityDepositUtilities, otherSecurityDeposit: otherSecurityDeposit, isRentalFreePeriodDuration: isRentalFreePeriodDuration, rentalFreePeriodStartDate: rentalFreePeriodStartDate, rentalFreePeriodEndDate: rentalFreePeriodEndDate, rentalFreePeriodWeeks: rentalFreePeriodWeeks, rentalFreePeriodMonths: rentalFreePeriodMonths, rentalFreePeriodYears: rentalFreePeriodYears, carParkLots: carParkLots, paymentAccountName: paymentAccountName, bank: bank, bankAccountNum: bankAccountNum, paymentMethod: paymentMethod, notificationMethod: notificationMethod, postDatedMethod: postDatedMethod, landlords: landlords, tenants: tenants, specialRequest: specialRequest, fixturesFitting: fixturesFitting)
        } else {
            self.init(isEditTA: isEditTA, orderID: orderID, currentStep: currentStep, isLandlord: isLandlord, streetName: streetName, town: town, postCode: postCode, states: states, isResidential: isResidential, commencementDate: commencementDate, termsOfTenancyMonths: termsOfTenancyMonths, termsOfTenancyYears: termsOfTenancyYears, isOptionToRenew: isOptionToRenew, optionToRenewMonths: optionToRenewMonths, optionToRenewYears: optionToRenewYears, isMarketPrevaillingRate: isMarketPrevaillingRate, isIncreaseOf10percent: isIncreaseOf10percent, isIncreaseOf15percent: isIncreaseOf15percent, isMutuallyAgreedRate: isMutuallyAgreedRate, isOtherPercent: isOtherPercent, otherTermsofRenewal: otherTermsofRenewal, rental: rental, advanceRental: advanceRental, securityDepositRent: securityDepositRent, securityDepositUtilities: securityDepositUtilities, otherSecurityDeposit: otherSecurityDeposit, isRentalFreePeriodDuration: isRentalFreePeriodDuration, rentalFreePeriodStartDate: rentalFreePeriodStartDate, rentalFreePeriodEndDate: rentalFreePeriodEndDate, rentalFreePeriodWeeks: rentalFreePeriodWeeks, rentalFreePeriodMonths: rentalFreePeriodMonths, rentalFreePeriodYears: rentalFreePeriodYears, carParkLots: carParkLots, paymentAccountName: paymentAccountName, bank: bank, bankAccountNum: bankAccountNum, paymentMethod: paymentMethod, notificationMethod: notificationMethod, postDatedMethod: postDatedMethod, landlords: landlords, tenants: tenants, specialRequest: specialRequest, fixturesFitting: fixturesFitting)
        }
        
    }
    
    init(isEditTA: Bool, orderID: String, currentStep: Int, isLandlord: Bool, streetName: String, town: String, postCode: String, states: String, isResidential: Bool, commencementDate: Date, termsOfTenancyMonths: Int, termsOfTenancyYears: Int, isOptionToRenew: Bool, optionToRenewMonths: Int, optionToRenewYears: Int, isMarketPrevaillingRate: Bool, isIncreaseOf10percent: Bool, isIncreaseOf15percent: Bool, isMutuallyAgreedRate: Bool, isOtherPercent: Bool, otherTermsofRenewal: String, rental: Double, advanceRental: Double, securityDepositRent: Double, securityDepositUtilities: Double, otherSecurityDeposit: [SecurityDeposit], isRentalFreePeriodDuration: Bool, rentalFreePeriodStartDate: Date, rentalFreePeriodEndDate: Date,rentalFreePeriodWeeks: Int, rentalFreePeriodMonths: Int, rentalFreePeriodYears: Int, carParkLots: [String], paymentAccountName: String, bank: String, bankAccountNum: String, paymentMethod: Int, notificationMethod: Int, postDatedMethod: Int, landlords: [LandlordTenant], tenants: [LandlordTenant], specialRequest: [String], fixturesFitting: [String]) {
        
        self.isEditTA = isEditTA
        self.orderID = orderID
        self.currentStep = currentStep
        self.isLandlord = isLandlord
        self.streetName = streetName
        self.town = town
        self.postCode = postCode
        self.states = states
        self.isResidential = isResidential
        self.commencementDate = commencementDate
        self.termsOfTenancyMonths = termsOfTenancyMonths
        self.termsOfTenancyYears = termsOfTenancyYears
        self.isOptionToRenew = isOptionToRenew
        self.optionToRenewMonths = optionToRenewMonths
        self.optionToRenewYears = optionToRenewYears
        self.isMarketPrevaillingRate = isMarketPrevaillingRate
        self.isIncreaseOf10percent = isIncreaseOf10percent
        self.isIncreaseOf15percent = isIncreaseOf15percent
        self.isMutuallyAgreedRate = isMutuallyAgreedRate
        self.isOtherPercent = isOtherPercent
        self.otherTermsofRenewal = otherTermsofRenewal
        self.rental = rental
        self.advanceRental = advanceRental
        self.securityDepositRent = securityDepositRent
        self.securityDepositUtilities = securityDepositUtilities
        self.otherSecurityDeposit = otherSecurityDeposit
        self.isRentalFreePeriodDuration = isRentalFreePeriodDuration
        self.rentalFreePeriodWeeks = rentalFreePeriodWeeks
        self.rentalFreePeriodMonths = rentalFreePeriodMonths
        self.rentalFreePeriodYears = rentalFreePeriodYears
        self.rentalFreePeriodStartDate = rentalFreePeriodStartDate
        self.rentalFreePeriodEndDate = rentalFreePeriodEndDate
        self.carParkLots = carParkLots
        self.paymentAccountName = paymentAccountName
        self.bank = bank
        self.bankAccountNum = bankAccountNum
        self.paymentMethod = paymentMethod
        self.notificationMethod = notificationMethod
        self.postDatedMethod = postDatedMethod
        self.landlords = landlords
        self.tenants = tenants
        self.specialRequest = specialRequest
        self.fixturesFitting = fixturesFitting
    }
    
    init(tenancyAgreementPDF: Data, uploadType: String, isEditTA: Bool, orderID: String, currentStep: Int, isLandlord: Bool, streetName: String, town: String, postCode: String, states: String, isResidential: Bool, commencementDate: Date, termsOfTenancyMonths: Int, termsOfTenancyYears: Int, isOptionToRenew: Bool, optionToRenewMonths: Int, optionToRenewYears: Int, isMarketPrevaillingRate: Bool, isIncreaseOf10percent: Bool, isIncreaseOf15percent: Bool, isMutuallyAgreedRate: Bool, isOtherPercent: Bool, otherTermsofRenewal: String, rental: Double, advanceRental: Double, securityDepositRent: Double, securityDepositUtilities: Double, otherSecurityDeposit: [SecurityDeposit],isRentalFreePeriodDuration: Bool, rentalFreePeriodStartDate: Date, rentalFreePeriodEndDate: Date, rentalFreePeriodWeeks: Int, rentalFreePeriodMonths: Int, rentalFreePeriodYears: Int, carParkLots: [String], paymentAccountName: String, bank: String, bankAccountNum: String, paymentMethod: Int, notificationMethod: Int, postDatedMethod: Int, landlords: [LandlordTenant], tenants: [LandlordTenant], specialRequest: [String], fixturesFitting: [String]) {
        
        self.tenancyAgreementPDF = tenancyAgreementPDF
        self.uploadType = uploadType
        self.isEditTA = isEditTA
        self.orderID = orderID
        self.currentStep = currentStep
        self.isLandlord = isLandlord
        self.streetName = streetName
        self.town = town
        self.postCode = postCode
        self.states = states
        self.isResidential = isResidential
        self.commencementDate = commencementDate
        self.termsOfTenancyMonths = termsOfTenancyMonths
        self.termsOfTenancyYears = termsOfTenancyYears
        self.isOptionToRenew = isOptionToRenew
        self.optionToRenewMonths = optionToRenewMonths
        self.optionToRenewYears = optionToRenewYears
        self.isMarketPrevaillingRate = isMarketPrevaillingRate
        self.isIncreaseOf10percent = isIncreaseOf10percent
        self.isIncreaseOf15percent = isIncreaseOf15percent
        self.isMutuallyAgreedRate = isMutuallyAgreedRate
        self.isOtherPercent = isOtherPercent
        self.otherTermsofRenewal = otherTermsofRenewal
        self.rental = rental
        self.advanceRental = advanceRental
        self.securityDepositRent = securityDepositRent
        self.securityDepositUtilities = securityDepositUtilities
        self.otherSecurityDeposit = otherSecurityDeposit
        self.isRentalFreePeriodDuration = isRentalFreePeriodDuration
        self.rentalFreePeriodWeeks = rentalFreePeriodWeeks
        self.rentalFreePeriodMonths = rentalFreePeriodMonths
        self.rentalFreePeriodYears = rentalFreePeriodYears
        self.rentalFreePeriodStartDate = rentalFreePeriodStartDate
        self.rentalFreePeriodEndDate = rentalFreePeriodEndDate
        self.carParkLots = carParkLots
        self.paymentAccountName = paymentAccountName
        self.bank = bank
        self.bankAccountNum = bankAccountNum
        self.paymentMethod = paymentMethod
        self.notificationMethod = notificationMethod
        self.postDatedMethod = postDatedMethod
        self.landlords = landlords
        self.tenants = tenants
        self.specialRequest = specialRequest
        self.fixturesFitting = fixturesFitting
    }

    override init() {
        
    }
    
    convenience init?(jsonTA: [String : Any]) {
        self.init()
        if let order_id = jsonTA["id"] as? Int {
            self.orderID = String(order_id)
        }
        if let propertyStreet = jsonTA["propertyStreet"] as? String {
            self.streetName = propertyStreet
        }
        if let propertyTown = jsonTA["propertyTown"] as? String {
            self.town = propertyTown
        }
        if let propertyPostcode = jsonTA["propertyPostcode"] as? String {
            self.postCode = propertyPostcode
        }
        if let propertyState = jsonTA["propertyState"] as? String {
            self.states = propertyState
        }
        if let isResidential = jsonTA["isResidential"] as? Bool {
            self.isResidential = isResidential
        }
        if let isLandlord = jsonTA["isLandlord"] as? Bool {
            self.isLandlord = isLandlord
        }
        if let jsonTenants = jsonTA["tenants"] as? [[String: Any]] {
            var tmpTenants: [LandlordTenant] = []
            for jsonTenant in jsonTenants {
                tmpTenants.append(LandlordTenant(jsonResult: jsonTenant)!)
            }
            self.tenants = tmpTenants
        }
        if let jsonLandlords = jsonTA["landlords"] as? [[String: Any]] {
            var tmpLandlords: [LandlordTenant] = []
            for jsonLandlord in jsonLandlords {
                tmpLandlords.append(LandlordTenant(jsonResult: jsonLandlord)!)
            }
            self.landlords = tmpLandlords
        }
        if let commencementDateString = jsonTA["commencementDate"] as? String {
            self.commencementDate = convertStringToDate(dateString: commencementDateString)
        }
        if let termsOfTenancyMonths = jsonTA["termsOfTenancyMonths"] as? Int {
            self.termsOfTenancyMonths = termsOfTenancyMonths
        }
        if let termsOfTenancyYears = jsonTA["termsOfTenancyYears"] as? Int {
            self.termsOfTenancyYears = termsOfTenancyYears
        }
        if let isOptionToRenew = jsonTA["isOptionToRenew"] as? Bool {
            self.isOptionToRenew = isOptionToRenew
        }
        if let optionToRenewMonths = jsonTA["optionToRenewMonths"] as? Int {
            self.optionToRenewMonths = optionToRenewMonths
        }
        if let optionToRenewYears = jsonTA["optionToRenewYears"] as? Int {
            self.optionToRenewYears = optionToRenewYears
        }
        if let optionToRenewCondition = jsonTA["optionToRenewCondition"] as? String {
            self.isMutuallyAgreedRate = false
            if optionToRenewCondition == "isMutuallyAgreedRate" {
                self.isMutuallyAgreedRate = true
            } else if optionToRenewCondition == "isIncreaseOf10percent" {
                self.isIncreaseOf10percent = true
            } else if optionToRenewCondition == "isIncreaseOf15percent" {
                self.isIncreaseOf15percent = true
            } else if optionToRenewCondition == "isOtherPercent" {
                self.isOtherPercent = true
            } else if optionToRenewCondition == "isMarketPrevaillingRate" {
                self.isMarketPrevaillingRate = true
            }
        }
        if let otherTermsofRenewal = jsonTA["otherTermsofRenewal"] as? String {
            self.otherTermsofRenewal = otherTermsofRenewal
        }
        if let rental = jsonTA["rental"] as? Double {
            self.rental = rental
        }
        if let advanceRental = jsonTA["advanceRental"] as? Double {
            self.advanceRental = advanceRental
        }
        if let securityDepositRent = jsonTA["securityDepositRent"] as? Double {
            self.securityDepositRent = securityDepositRent
        }
        if let securityDepositUtilities = jsonTA["securityDepositUtilities"] as? Double {
            self.securityDepositUtilities = securityDepositUtilities
        }
        if let jsonOtherSecurityDepositsString = jsonTA["otherSecurityDeposit"] as? String {
                if let jsonOtherSecurityDepositsData = jsonOtherSecurityDepositsString.data(using: String.Encoding.utf8) {
                    if let jsonOtherSecurityDepositsDecode = try? JSONSerialization.jsonObject(with: jsonOtherSecurityDepositsData, options: []){
                        if let jsonOtherSecurityDeposits = jsonOtherSecurityDepositsDecode as? [[String: Any]] {
                            var tmpOtherSecurityDeposit: [SecurityDeposit] = []
                            for jsonOtherSecurityDeposit in jsonOtherSecurityDeposits {
                                tmpOtherSecurityDeposit.append(SecurityDeposit(jsonResult: jsonOtherSecurityDeposit)!)
                            }
                            self.otherSecurityDeposit = tmpOtherSecurityDeposit
                        }
                    }
                }
        }

        
        if let rentalFreePeriodWeeks = jsonTA["rentalFreePeriodWeeks"] as? Int {
            self.rentalFreePeriodWeeks = rentalFreePeriodWeeks
        }
        if let rentalFreePeriodMonths = jsonTA["rentalFreePeriodMonths"] as? Int {
            self.rentalFreePeriodMonths = rentalFreePeriodMonths
        }
        if let rentalFreePeriodYears = jsonTA["rentalFreePeriodYears"] as? Int {
            self.rentalFreePeriodYears = rentalFreePeriodYears
        }
        if let commencementDateString = jsonTA["commencementDate"] as? String {
            self.commencementDate = convertStringToDate(dateString: commencementDateString)
        }
        if let rentalFreePeriodStartString = jsonTA["rentalFreePeriodStart"] as? String {
            self.rentalFreePeriodStartDate = convertStringToDate(dateString: rentalFreePeriodStartString)
        }
        if let rentalFreePeriodEndString = jsonTA["rentalFreePeriodEnd"] as? String {
            self.rentalFreePeriodEndDate = convertStringToDate(dateString: rentalFreePeriodEndString)
        }
        if let carParkLotsString = jsonTA["carParkLots"] as? String {
            if let carParkLotsData = carParkLotsString.data(using: String.Encoding.utf8) {
                if let jsonCarParkLots = try? JSONSerialization.jsonObject(with: carParkLotsData, options: []){
                    if let carParkLotsStringOldArray = jsonCarParkLots as? [String] {
                        self.carParkLots = carParkLotsStringOldArray
                    }
                }
            }
        }
        if let paymentMethod = jsonTA["paymentMethod"] as? String {
            if paymentMethod == "isPostDatedCheque" {
                self.paymentMethod = 1
            } else if paymentMethod == "isCollectionByLandlord" {
                self.paymentMethod = 2
            } else {
                self.paymentMethod = 0
            }
        }
        if let paymentAccountName = jsonTA["paymentAccountName"] as? String {
            self.paymentAccountName = paymentAccountName
        }
        if let bank = jsonTA["bank"] as? String {
            self.bank = bank
        }
        if let bankAccountNum = jsonTA["bankAccountNum"] as? String {
            self.bankAccountNum = bankAccountNum
        }
        
        if let notificationMethod = jsonTA["notificationMethod"] as? String {
            if notificationMethod == "isSMS" {
                self.notificationMethod = 1
            } else if notificationMethod == "isWhatsapp" {
                self.notificationMethod = 2
            } else {
                self.notificationMethod = 0
            }
        }
        
        if let postDatedMethod = jsonTA["postDatedMethod"] as? String {
            if postDatedMethod == "is12Months" {
                self.postDatedMethod = 1
            } else if postDatedMethod == "is24Months" {
                self.postDatedMethod = 2
            } else {
                self.postDatedMethod = 0
            }
        }
        if let fixturesFittingString = jsonTA["fixturesFitting"] as? String {
            if let fixturesFittingData = fixturesFittingString.data(using: String.Encoding.utf8) {
                if let jsonFixturesFitting = try? JSONSerialization.jsonObject(with: fixturesFittingData, options: []){
                    if let fixturesFittingStringOldArray = jsonFixturesFitting as? [String] {
                        self.fixturesFitting = fixturesFittingStringOldArray
                    }
                }
            }
        }
        if let specialRequestString = jsonTA["specialRequest"] as? String {
            if let specialRequestData = specialRequestString.data(using: String.Encoding.utf8) {
                if let jsonSpecialRequest = try? JSONSerialization.jsonObject(with: specialRequestData, options: []){
                    if let specialRequestStringOldArray = jsonSpecialRequest as? [String] {
                        self.specialRequest = specialRequestStringOldArray
                    }
                }
            }
        }
        if let specialRequestString = jsonTA["specialRequest"] as? String {
            if let specialRequestData = specialRequestString.data(using: String.Encoding.utf8) {
                if let jsonSpecialRequest = try? JSONSerialization.jsonObject(with: specialRequestData, options: []){
                    if let specialRequestStringOldArray = jsonSpecialRequest as? [String] {
                        self.specialRequest = specialRequestStringOldArray
                    } else if let specialRequestStringNewArray = jsonSpecialRequest as? [[String: String]] {
                        var tempSpecialRequests: [String] = []
                        for newSpecialRequest in specialRequestStringNewArray {
                            if let originalSpecialRequest = newSpecialRequest["original"] {
                                tempSpecialRequests.append(originalSpecialRequest)
                            }
                        }
                        self.specialRequest = tempSpecialRequests
                    }
                }
            }
        }
        self.currentStep = 8
        
    }
    
    func convertStringToDate(dateString: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let formattedDate = formatter.date(from: dateString) {
            return formattedDate
        }
        return Date()
    }
}
