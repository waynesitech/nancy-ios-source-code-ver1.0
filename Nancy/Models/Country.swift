//
//  Country.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 27/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

class Country : NSObject, NSCoding {

    var code: String = ""
    var name: String = ""
    var phoneCode: String = ""
    
    func encode(with aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encode(self.code, forKey: "CountryCode")
        keyedArchiver.encode(self.name, forKey: "CountryName")
        keyedArchiver.encode(self.phoneCode, forKey: "CountryPhoneCode")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let CountryCode = keyedUnarchiver.decodeObject(forKey: "CountryCode") as! String
        let CountryName = keyedUnarchiver.decodeObject(forKey: "CountryName") as! String
        let CountryPhoneCode = keyedUnarchiver.decodeObject(forKey: "CountryPhoneCode") as! String
        self.init(code: CountryCode, name: CountryName, phoneCode: CountryPhoneCode)
    }
    
    init(code: String, name: String, phoneCode: String) {
        self.code = code
        self.name = name
        self.phoneCode = phoneCode
    }
    
    init(code: String, name: String) {
        self.code = code
        self.name = name
    }
    
    init(phoneCode: String) {
        self.phoneCode = phoneCode
        for code in NSLocale.isoCountryCodes {
            
            let phoneNumberUtil = NBPhoneNumberUtil.sharedInstance()!
            let phoneCodeVal: String? = "+\(phoneNumberUtil.getCountryCode(forRegion: code)!)"
            
            if phoneCodeVal == self.phoneCode {
                self.code = code
                self.name = NSLocale.current.localizedString(forRegionCode: code)!
            }
        }
    }
    
    init(code: String) {
        self.code = code
        self.name = NSLocale.current.localizedString(forRegionCode: code)!
        
        let phoneNumberUtil = NBPhoneNumberUtil.sharedInstance()!
        self.phoneCode = "+\(phoneNumberUtil.getCountryCode(forRegion: code)!)"
    }
}
