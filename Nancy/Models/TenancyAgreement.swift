//
//  TenancyAgreement.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 19/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class TenancyAgreement {
    var order_id: String = ""
    var referenceNumber: String = ""
    var tenancyAgreementStatus: String = ""
    var eSignStatus: String = ""
    var eStampStatus: String = ""
    var propertyName: String = ""
    var propertyImageURL: String = ""
    var propertyStreet: String = ""
    var propertyTown: String = ""
    var propertyPostcode: String = ""
    var propertyState: String = ""
    var previewDocumentPath: String = ""
    var actualDocumentPath: String = ""
    var ipayBackendURL: String = ""
    var ipayRemark: String = ""
    var tenantNames: [String] = []
    var landlordNames: [String] = []
    var tenantStatus: [String] = []
    var landlordStatus: [String] = []
    var specialRequests: [[String:String]] = []
    
    init (referenceNumber: String, tenancyAgreementStatus: String, eSignStatus: String, eStampStatus: String, propertyName: String, propertyImageURL: String) {
        self.referenceNumber = referenceNumber
        self.tenancyAgreementStatus = tenancyAgreementStatus
        self.eSignStatus = eSignStatus
        self.eStampStatus = eStampStatus
        self.propertyName = propertyName
        self.propertyImageURL = propertyImageURL
    }
    
    init() {}
    
    convenience init?(jsonTA: [String : Any]) {
        self.init()
        if let order_id = jsonTA["id"] as? Int {
            self.order_id = String(order_id)
        }
        if let order_num = jsonTA["order_num"] as? String {
            self.referenceNumber = order_num
        }
        if let propertyStreet = jsonTA["propertyStreet"] as? String {
            self.propertyStreet = propertyStreet
        }
        if let propertyTown = jsonTA["propertyTown"] as? String {
            self.propertyTown = propertyTown
        }
        if let propertyPostcode = jsonTA["propertyPostcode"] as? String {
            self.propertyPostcode = propertyPostcode
        }
        if let propertyState = jsonTA["propertyState"] as? String {
            self.propertyState = propertyState
        }
        if let propertyPicture = jsonTA["propertyPicture"] as? String {
            self.propertyImageURL = propertyPicture
        }
        if let previewDocumentPath = jsonTA["previewDocumentPath"] as? String {
            self.previewDocumentPath = previewDocumentPath
        }
        if let actualDocumentPath = jsonTA["actualDocumentPath"] as? String {
            self.actualDocumentPath = actualDocumentPath
        }
        if let tenancyAgreementStatus = jsonTA["tenancyAgreementStatus"] as? String {
            self.tenancyAgreementStatus = tenancyAgreementStatus
        }
        if let eSignStatus = jsonTA["eSignStatus"] as? String {
            self.eSignStatus = eSignStatus
        }
        if let eStampStatus = jsonTA["eStampStatus"] as? String {
            self.eStampStatus = eStampStatus
        }
        if let specialRequestString = jsonTA["specialRequest"] as? String {
            if let specialRequestData = specialRequestString.data(using: String.Encoding.utf8) {
                if let jsonSpecialRequest = try? JSONSerialization.jsonObject(with: specialRequestData, options: []){
                    if let specialRequestStringOldArray = jsonSpecialRequest as? [String] {
                        var tempSpecialRequests: [[String: String]] = []
                        for oldSpecialRequest in specialRequestStringOldArray {
                            tempSpecialRequests.append(["original": oldSpecialRequest,
                                                        "edited": ""])
                        }
                        self.specialRequests = tempSpecialRequests
                    } else if let specialRequestStringNewArray = jsonSpecialRequest as? [[String: String]] {
                        self.specialRequests = specialRequestStringNewArray
                    }
                }
            }
        }
        if let jsonTenants = jsonTA["tenants"] as? [[String: String]] {
            for jsonTenant in jsonTenants {
                if let tenantName = jsonTenant["name"] {
                    tenantNames.append(tenantName)
                    if let tenantStatus = jsonTenant["status"] {
                        self.tenantStatus.append(tenantStatus)
                    }
                }
            }
        }
        if let jsonLandlords = jsonTA["landlords"] as? [[String: String]] {
            for jsonLandlord in jsonLandlords {
                if let landlordName = jsonLandlord["name"] {
                    landlordNames.append(landlordName)
                    if let landlordStatus = jsonLandlord["status"] {
                        self.landlordStatus.append(landlordStatus)
                    }
                }
            }
        }
        if let ipayBackendURL = jsonTA["ipayBackendURL"] as? String {
            self.ipayBackendURL = ipayBackendURL
        }
        if let ipayRemark = jsonTA["ipayRemark"] as? String {
            self.ipayRemark = ipayRemark
        }
    }
    
    func getLandlordNames() -> String {
        if self.landlordNames.count > 0 {
            var landlordText = "\(self.landlordNames[0])"
            for i in 1..<self.landlordNames.count {
                landlordText = landlordText + "\n\(self.landlordNames[i])"
            }
            return landlordText
        }
        return ""
    }
    
    func getTenantNames() -> String {
        if self.tenantNames.count > 0 {
            var tenantText = "\(self.tenantNames[0])"
            for i in 1..<self.tenantNames.count {
                tenantText = tenantText + "\n\(self.tenantNames[i])"
            }
            return tenantText
        }
        return ""
    }
    
    func getPropertyName() -> String {
        return "\(self.propertyStreet)\n\(self.propertyTown)\n\(self.propertyPostcode) \(self.propertyState)"
    }
}
