//
//  NetworkLoader.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 18/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import Alamofire

class NetworkLoader {
    //Singleton Pattern
    static let sharedLoader: NetworkLoader = NetworkLoader()
    
    // Server specific values
    let baseURL: URL = URL(string: "https://nancy.com.my/api/")!
    let currentVersionCode: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    let platform: String = "ios"
    
    // Error Messages
    let noInternetTitle = "Error"
    let noInternetBody = "There seems to be an issue with your Internet connection. Please try again later."
    
    let generalErrorTitle = "Error"
    
    let errorGettingJSONTitle = "Error"
    let errorGettingJSONBody = "There seems to be an issue parsing the response from server."
    
    let emailExistsTitle = "Email Exists"
    let emailExistsBody = "The email exists, please try again with another email or sign in."
    
    func resetPassword(_ email: String, completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "users/reset", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "email": email
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .validate()
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    if response.response?.statusCode == 404 {
                        completion(NetworkLoaderError(networkLoaderErrorTitle: "Email Not Found.", networkLoaderErrorBody: "Please try again with another email."), nil)
                        return
                    }
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                    }
                    return
                }
                
                guard let JSON = response.result.value as? [String:Any] else {
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let meta = JSON["meta"] as? [String:Any] else {
                    print("could not get the meta")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = meta["stat"] as? Int else {
                    print("could not get the status")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                if status == 404 {
                    completion(NetworkLoaderError(networkLoaderErrorTitle: "Email Not Found.", networkLoaderErrorBody: "Please try again with another email."), nil)
                    return
                } else if status == 200 {
                    completion(nil, true)
                    return
                }
        }
    }
    
    
    func login(_ email: String, password: String, completion: @escaping (NetworkLoaderError?, [String:Any]?) -> Void) {
        let url: URL = URL(string: "users/login", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "email"              : email,
            "password"           : password,
            "currentVersionCode" : currentVersionCode,
            "platform"           : platform
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .validate()
            .responseJSON { response in
                
                guard response.result.isSuccess else {
                    if response.response?.statusCode == 401 || response.response?.statusCode == 404 {
                        completion(NetworkLoaderError(networkLoaderErrorTitle: "Incorrect Email or Password", networkLoaderErrorBody: "Please try again."), nil)
                        return
                    }
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                    }
                    return
                }
                
                guard let JSON = response.result.value as? [String:Any] else {
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let meta = JSON["meta"] as? [String:Any] else {
                    print("could not get the meta")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = meta["stat"] as? Int else {
                    print("could not get the status")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                if status == 401 || status == 404 {
                    completion(NetworkLoaderError(networkLoaderErrorTitle: "Incorrect Email or Password", networkLoaderErrorBody: "Please try again."), nil)
                    return
                } else if status == 200 {
                    guard let jsonResults = JSON["result"] as? [String:Any] else {
                        print("Error getting result from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    UserData.sharedUserData.ParseUserFromJsonResponse(jsonData: jsonResults)
                    completion(nil, jsonResults)
                    return
                }
        }
    }
    
    func register(_ name: String, ic: String, email: String, mobile: String, password: String, completion: @escaping (NetworkLoaderError?, [String:Any]?) -> Void) {
        let url: URL = URL(string: "users/register", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "name"               : name,
            "ic_number"           : ic,
            "email"              : email,
            "mobile"             : mobile,
            "password"           : password,
            "currentVersionCode" : currentVersionCode,
            "platform"           : platform
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let meta = JSON["meta"] as? [String:Any] else {
                    print("could not get the meta")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = meta["stat"] as? Int else {
                    print("could not get the status")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                if status != 200 {
                    if let errorMessage = meta["msg"] as? [String:Any] {
                        if let errorTitle = errorMessage["subj"] as? String {
                            if let errorBody = errorMessage["body"] as? String {
                                completion(NetworkLoaderError(networkLoaderErrorTitle: errorTitle, networkLoaderErrorBody: errorBody), nil)
                                return
                            }
                        }
                    }
                    completion(NetworkLoaderError(networkLoaderErrorTitle: "Unable to register account", networkLoaderErrorBody: "Please try again."), nil)
                    return
                }
                
                if status == 200 {
                    guard let jsonResults = JSON["result"] as? [String:Any] else {
                        print("Error getting result from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    UserData.sharedUserData.ParseUserFromJsonResponse(jsonData: jsonResults)
                    completion(nil, jsonResults)
                    return
                }
        }
    }
    
    func requestPhoneVerification(_ completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "users/requestPhoneVerificationById", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "userID" : UserData.sharedUserData.userID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let meta = JSON["meta"] as? [String:Any] else {
                    print("could not get the meta")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = meta["stat"] as? Int else {
                    print("could not get the status")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                if status != 200 {
                    if let errorMessage = meta["msg"] as? [String:Any] {
                        if let errorTitle = errorMessage["subj"] as? String {
                            if let errorBody = errorMessage["body"] as? String {
                                completion(NetworkLoaderError(networkLoaderErrorTitle: errorTitle, networkLoaderErrorBody: errorBody), nil)
                                return
                            }
                        }
                    }
                    completion(NetworkLoaderError(networkLoaderErrorTitle: "Unable to register account", networkLoaderErrorBody: "Please try again."), nil)
                    return
                }
                
                if status == 200 {
                    completion(nil, true)
                    return
                }
        }
    }
    
    
    func submitPhoneVerification(_ verificationCode: String, completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "users/submitPhoneVerificationById", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "userID"           : UserData.sharedUserData.userID,
            "verificationCode" : verificationCode
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                
                guard let JSON = response.result.value as? [String:Any] else {
                    if response.response?.statusCode == 400 {
                        completion(nil, true)
                        return
                    }
                    
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                if let isPhoneVerify: Bool = jsonResults["success"] as? Bool {
                    completion(nil, isPhoneVerify)
                }
        }
    }
    
    func createNewOrderWithPDF(_ completion: @escaping (NetworkLoaderError?, TenancyAgreement?) -> Void) {
        let url: URL = URL(string: "order/newWithPDF", relativeTo: self.baseURL)!
        
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        var parameters: [String:String] = [
            "uploadType" : newTenancyAgreement.uploadType,
            "propertyStreet"   : newTenancyAgreement.streetName,
            "propertyTown"   : newTenancyAgreement.town,
            "propertyPostcode"   : newTenancyAgreement.postCode,
            "propertyState"   : newTenancyAgreement.states,
            "isLandlord"    : newTenancyAgreement.isLandlord.description,
            "isResidential"   : newTenancyAgreement.isResidential.description,
            "commencementDate"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.commencementDate),
            "termsOfTenancyMonths"   : newTenancyAgreement.termsOfTenancyMonths.description,
            "termsOfTenancyYears"   : newTenancyAgreement.termsOfTenancyYears.description,
            "isOptionToRenew"   : newTenancyAgreement.isOptionToRenew.description,
            "rental"   : newTenancyAgreement.rental.description,
            "advanceRental"   : newTenancyAgreement.advanceRental.description,
            "securityDepositRent"   : newTenancyAgreement.securityDepositRent.description,
            "securityDepositUtilities"   : newTenancyAgreement.securityDepositUtilities.description,
            "carParkLots"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertStringArrayToJSON(stringArray: newTenancyAgreement.carParkLots)
            ]
        
        if newTenancyAgreement.isOptionToRenew {
            parameters["optionToRenewMonths"] = newTenancyAgreement.optionToRenewMonths.description
            parameters["optionToRenewYears"] = newTenancyAgreement.optionToRenewYears.description
            parameters["optionToRenewCondition"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertOptionToRenew()
            if newTenancyAgreement.isOtherPercent {
                parameters["otherTermsofRenewal"] = newTenancyAgreement.otherTermsofRenewal
            }
        }
        
        if newTenancyAgreement.isRentalFreePeriodDuration {
            parameters["rentalFreePeriodWeeks"] = newTenancyAgreement.rentalFreePeriodWeeks.description
            parameters["rentalFreePeriodMonths"] = newTenancyAgreement.rentalFreePeriodMonths.description
            parameters["rentalFreePeriodYears"] = newTenancyAgreement.rentalFreePeriodYears.description
        } else {
            parameters["rentalFreePeriodStart"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.rentalFreePeriodStartDate)
            parameters["rentalFreePeriodEnd"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.rentalFreePeriodEndDate)
        }
        
        print(parameters)
        Alamofire.upload( multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(NewTenancyAgreementData.sharedNewTenancyAgreementData.propertyImage, 0.5) {
                multipartFormData.append(imageData, withName: "propertyPicture", fileName: "propertyPicture.jpg", mimeType: "image/jpeg")
            }
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF!, withName: "taPDF", fileName: "taPDF.pdf", mimeType: "application/pdf")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.ascii)!, withName: key)
            }
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertOtherSecuritiesToJSON(), withName: "otherSecurityDeposit")
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertLandlordTenantArrayToJSON(landlordTenants: newTenancyAgreement.tenants), withName: "tenants")
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertLandlordTenantArrayToJSON(landlordTenants: newTenancyAgreement.landlords), withName: "landlords")
        }, to: url) {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    guard let JSON = response.result.value as? [String:Any] else {
                        if let error = response.result.error {
                            completion(NetworkLoaderError(
                                networkLoaderErrorTitle: self.generalErrorTitle,
                                networkLoaderErrorBody: error.localizedDescription), nil)
                            return
                        }
                        
                        print("Error getting JSON from response")
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.errorGettingJSONTitle,
                            networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonResults = JSON["result"] as? [String:Any] else {
                        print("Error getting result from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonOrder = jsonResults["order"] as? [String:Any] else {
                        print("Error getting orders from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    if let tenancyAgreement:TenancyAgreement = TenancyAgreement(jsonTA: jsonOrder) {
                        completion(nil, tenancyAgreement)
                    }
                }
            case .failure(let encodingError):
                completion(NetworkLoaderError(networkLoaderErrorTitle: "Encoding Error", networkLoaderErrorBody: "Failed to encode image, please try again"), nil)
            }
        }
    }

    func createNewOrder(_ completion: @escaping (NetworkLoaderError?, TenancyAgreement?) -> Void) {
        let url: URL = URL(string: "order/new", relativeTo: self.baseURL)!
        
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        var parameters: [String:String] = [
            "propertyStreet"   : newTenancyAgreement.streetName,
            "propertyTown"   : newTenancyAgreement.town,
            "propertyPostcode"   : newTenancyAgreement.postCode,
            "propertyState"   : newTenancyAgreement.states,
            "isLandlord"    : newTenancyAgreement.isLandlord.description,
            "isResidential"   : newTenancyAgreement.isResidential.description,
            "commencementDate"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.commencementDate),
            "termsOfTenancyMonths"   : newTenancyAgreement.termsOfTenancyMonths.description,
            "termsOfTenancyYears"   : newTenancyAgreement.termsOfTenancyYears.description,
            "isOptionToRenew"   : newTenancyAgreement.isOptionToRenew.description,
            "rental"   : newTenancyAgreement.rental.description,
            "advanceRental"   : newTenancyAgreement.advanceRental.description,
            "securityDepositRent"   : newTenancyAgreement.securityDepositRent.description,
            "securityDepositUtilities"   : newTenancyAgreement.securityDepositUtilities.description,
            "carParkLots"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertStringArrayToJSON(stringArray: newTenancyAgreement.carParkLots),
            "paymentMethod"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertPaymentMethod(),
            "specialRequest"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertSpecialRequestToJSON(specialRequestStringArray: newTenancyAgreement.specialRequest),
            "fixturesFitting"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertStringArrayToJSON(stringArray: newTenancyAgreement.fixturesFitting),
        ]
        
        if newTenancyAgreement.isOptionToRenew {
            parameters["optionToRenewMonths"] = newTenancyAgreement.optionToRenewMonths.description
            parameters["optionToRenewYears"] = newTenancyAgreement.optionToRenewYears.description
            parameters["optionToRenewCondition"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertOptionToRenew()
            if newTenancyAgreement.isOtherPercent {
                parameters["otherTermsofRenewal"] = newTenancyAgreement.otherTermsofRenewal
            }
        }
        
        if newTenancyAgreement.paymentMethod == 0 {
            parameters["paymentAccountName"] = newTenancyAgreement.paymentAccountName
            parameters["bank"] = newTenancyAgreement.bank
            parameters["bankAccountNum"] = newTenancyAgreement.bankAccountNum
            parameters["notificationMethod"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertNotificationMethod()
        } else if newTenancyAgreement.paymentMethod == 1 {
            parameters["postDatedMethod"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertPostDatedMethod()
        }
        
        if newTenancyAgreement.isRentalFreePeriodDuration {
            parameters["rentalFreePeriodWeeks"] = newTenancyAgreement.rentalFreePeriodWeeks.description
            parameters["rentalFreePeriodMonths"] = newTenancyAgreement.rentalFreePeriodMonths.description
            parameters["rentalFreePeriodYears"] = newTenancyAgreement.rentalFreePeriodYears.description
        } else {
            parameters["rentalFreePeriodStart"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.rentalFreePeriodStartDate)
            parameters["rentalFreePeriodEnd"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.rentalFreePeriodEndDate)
        }
        
        print(parameters)
        Alamofire.upload( multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(NewTenancyAgreementData.sharedNewTenancyAgreementData.propertyImage, 0.5) {
                multipartFormData.append(imageData, withName: "propertyPicture", fileName: "propertyPicture.jpg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.ascii)!, withName: key)
            }
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertOtherSecuritiesToJSON(), withName: "otherSecurityDeposit")
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertLandlordTenantArrayToJSON(landlordTenants: newTenancyAgreement.tenants), withName: "tenants")
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertLandlordTenantArrayToJSON(landlordTenants: newTenancyAgreement.landlords), withName: "landlords")
        }, to: url) {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    guard let JSON = response.result.value as? [String:Any] else {
                        if let error = response.result.error {
                            completion(NetworkLoaderError(
                                networkLoaderErrorTitle: self.generalErrorTitle,
                                networkLoaderErrorBody: error.localizedDescription), nil)
                            return
                        }
                        
                        print("Error getting JSON from response")
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.errorGettingJSONTitle,
                            networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonResults = JSON["result"] as? [String:Any] else {
                        print("Error getting result from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonOrder = jsonResults["order"] as? [String:Any] else {
                        print("Error getting orders from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    if let tenancyAgreement:TenancyAgreement = TenancyAgreement(jsonTA: jsonOrder) {
                        completion(nil, tenancyAgreement)
                    }
                }
            case .failure(let encodingError):
                completion(NetworkLoaderError(networkLoaderErrorTitle: "Encoding Error", networkLoaderErrorBody: "Failed to encode image, please try again"), nil)
            }
        }
    }
    
    func editOrder(_ completion: @escaping (NetworkLoaderError?, TenancyAgreement?) -> Void) {
        let url: URL = URL(string: "order/edit", relativeTo: self.baseURL)!
        
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        var parameters: [String:String] = [
            "orderID"   : newTenancyAgreement.orderID,
            "propertyStreet"   : newTenancyAgreement.streetName,
            "propertyTown"   : newTenancyAgreement.town,
            "propertyPostcode"   : newTenancyAgreement.postCode,
            "propertyState"   : newTenancyAgreement.states,
            "isLandlord"    : newTenancyAgreement.isLandlord.description,
            "isResidential"   : newTenancyAgreement.isResidential.description,
            "commencementDate"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.commencementDate),
            "termsOfTenancyMonths"   : newTenancyAgreement.termsOfTenancyMonths.description,
            "termsOfTenancyYears"   : newTenancyAgreement.termsOfTenancyYears.description,
            "isOptionToRenew"   : newTenancyAgreement.isOptionToRenew.description,
            "rental"   : newTenancyAgreement.rental.description,
            "advanceRental"   : newTenancyAgreement.advanceRental.description,
            "securityDepositRent"   : newTenancyAgreement.securityDepositRent.description,
            "securityDepositUtilities"   : newTenancyAgreement.securityDepositUtilities.description,
            "carParkLots"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertStringArrayToJSON(stringArray: newTenancyAgreement.carParkLots),
            "paymentMethod"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertPaymentMethod(),
            "specialRequest"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertSpecialRequestToJSON(specialRequestStringArray: newTenancyAgreement.specialRequest),
            "fixturesFitting"   : NewTenancyAgreementData.sharedNewTenancyAgreementData.convertStringArrayToJSON(stringArray: newTenancyAgreement.fixturesFitting),
            ]
        if newTenancyAgreement.isOptionToRenew {
            parameters["optionToRenewMonths"] = newTenancyAgreement.optionToRenewMonths.description
            parameters["optionToRenewYears"] = newTenancyAgreement.optionToRenewYears.description
            parameters["optionToRenewCondition"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertOptionToRenew()
            if newTenancyAgreement.isOtherPercent {
                parameters["otherTermsofRenewal"] = newTenancyAgreement.otherTermsofRenewal
            }
        }
        
        if newTenancyAgreement.paymentMethod == 0 {
            parameters["paymentAccountName"] = newTenancyAgreement.paymentAccountName
            parameters["bank"] = newTenancyAgreement.bank
            parameters["bankAccountNum"] = newTenancyAgreement.bankAccountNum
            parameters["notificationMethod"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertNotificationMethod()
        } else if newTenancyAgreement.paymentMethod == 1 {
            parameters["postDatedMethod"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertPostDatedMethod()
        }
        
        if newTenancyAgreement.isRentalFreePeriodDuration {
            parameters["rentalFreePeriodWeeks"] = newTenancyAgreement.rentalFreePeriodWeeks.description
            parameters["rentalFreePeriodMonths"] = newTenancyAgreement.rentalFreePeriodMonths.description
            parameters["rentalFreePeriodYears"] = newTenancyAgreement.rentalFreePeriodYears.description
        } else {
            parameters["rentalFreePeriodStart"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.rentalFreePeriodStartDate)
            parameters["rentalFreePeriodEnd"] = NewTenancyAgreementData.sharedNewTenancyAgreementData.convertDateToString(date: newTenancyAgreement.rentalFreePeriodEndDate)
        }
        Alamofire.upload( multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(NewTenancyAgreementData.sharedNewTenancyAgreementData.propertyImage, 0.5) {
                multipartFormData.append(imageData, withName: "propertyPicture", fileName: "propertyPicture.jpg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.ascii)!, withName: key)
            }
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertOtherSecuritiesToJSON(), withName: "otherSecurityDeposit")
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertLandlordTenantArrayToJSON(landlordTenants: newTenancyAgreement.tenants), withName: "tenants")
            multipartFormData.append(NewTenancyAgreementData.sharedNewTenancyAgreementData.convertLandlordTenantArrayToJSON(landlordTenants: newTenancyAgreement.landlords), withName: "landlords")
        }, to: url) {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    guard let JSON = response.result.value as? [String:Any] else {
                        if let error = response.result.error {
                            completion(NetworkLoaderError(
                                networkLoaderErrorTitle: self.generalErrorTitle,
                                networkLoaderErrorBody: error.localizedDescription), nil)
                            return
                        }
                        
                        print("Error getting JSON from response")
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.errorGettingJSONTitle,
                            networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonResults = JSON["result"] as? [String:Any] else {
                        print("Error getting result from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonOrder = jsonResults["order"] as? [String:Any] else {
                        print("Error getting orders from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    if let tenancyAgreement:TenancyAgreement = TenancyAgreement(jsonTA: jsonOrder) {
                        completion(nil, tenancyAgreement)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completion(NetworkLoaderError(networkLoaderErrorTitle: "Encoding Error", networkLoaderErrorBody: "Failed to encode image, please try again"), nil)
            }
        }
    }
    
    func listTenancyAgreement(_ completion: @escaping (NetworkLoaderError?, [TenancyAgreement]?) -> Void) {
        let url: URL = URL(string: "order/list", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [:]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                print(JSON)
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonOrders = jsonResults["orders"] as? [[String:Any]] else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                var tenancyAgreements: [TenancyAgreement] = []
                for jsonOrder in jsonOrders {
                    if let tenancyAgreement:TenancyAgreement = TenancyAgreement(jsonTA: jsonOrder) {
                        tenancyAgreements.append(tenancyAgreement)
                    }
                }
                completion(nil, tenancyAgreements)
        }
    }
    
    func getTenancyAgreementDetails(_ orderID: String, completion: @escaping (NetworkLoaderError?, TenancyAgreement?) -> Void) {
        let url: URL = URL(string: "order/detail", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                print(JSON)
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonOrder = jsonResults["order"] as? [String:Any] else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                if let tenancyAgreement:TenancyAgreement = TenancyAgreement(jsonTA: jsonOrder) {
                    completion(nil, tenancyAgreement)
                }
                
        }
    }
    
    func getTenancyAgreementFullDetail(_ orderID: String, completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "order/fullDetail", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonOrder = jsonResults["order"] as? [String:Any] else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                NewTenancyAgreementData.sharedNewTenancyAgreementData.repopulateTenancyAgreement(jsonTA: jsonOrder)
                completion(nil, true)
        }
    }
    
    func sendTAviaEmail(_ orderID: String, completion: @escaping (NetworkLoaderError?, String?) -> Void) {
        let url: URL = URL(string: "order/resendAgreement", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonMeta = JSON["meta"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonMsg = jsonMeta["msg"] as? [String:Any] else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let messageString = jsonMsg["body"] as? String else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                completion(nil, messageString)
                
        }
    }
    
    func sendOTP(_ orderID: String, completion: @escaping (NetworkLoaderError?, String?) -> Void) {
        let url: URL = URL(string: "order/sendSignInvitation", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonSmsStatus = jsonResults["smsStatus"] as? [[String:Any]] else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                
                var messageString = "OTP has been sent to"
                for smsStatus in jsonSmsStatus {
                    if let status = smsStatus["status"] as? Int, status == 1 {
                        messageString.append("\n")
                        messageString.append(smsStatus["userRole"] as! String)
                        messageString.append(" (")
                        messageString.append(smsStatus["userName"] as! String)
                        messageString.append(") at ")
                        messageString.append(smsStatus["smsMobile"] as! String)
                    }
                }
                completion(nil, messageString)
                
        }
    }
    
    func getEstampPrice(_ orderID: String, completion: @escaping (NetworkLoaderError?, [String:Any]?) -> Void) {
        let url: URL = URL(string: "order/getEstampingPrice", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                completion(nil, jsonResults)
                
        }
    }
    
    func loginWithPasscode(_ passcode: String, icNumber: String, completion: @escaping (NetworkLoaderError?, TenancyAgreement?, Int?, String?) -> Void) {
        let url: URL = URL(string: "order/loginViaPasscode", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "passcode": passcode,
            "ic_num": icNumber
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil, nil, nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil, nil, nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil, nil, nil)
                    return
                }
                
                guard let jsonOrder = jsonResults["order"] as? [String:Any] else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil, nil, nil)
                    return
                }
                
                guard let orderUserID = jsonResults["orderUserID"] as? Int else {
                    print("Error getting order user ID from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil, nil, nil)
                    return
                }
                
                guard let orderUserName = jsonResults["orderUserName"] as? String else {
                    print("Error getting order user Name from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil, nil, nil)
                    return
                }
                
                if let tenancyAgreement:TenancyAgreement = TenancyAgreement(jsonTA: jsonOrder) {
                    completion(nil, tenancyAgreement, orderUserID, orderUserName)
                }
                
        }
    }
    
    func submitESign(_ orderUserID: String, signatureUIImage: UIImage, completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "order/apiSubmitSignature", relativeTo: self.baseURL)!
        
        let parameters: [String:String] = [
            "orderUserID"   : orderUserID
        ]
        Alamofire.upload( multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(signatureUIImage, 0.5) {
                multipartFormData.append(imageData, withName: "signatureImage", fileName: "signatureImage.jpg", mimeType: "image/jpeg")
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.ascii)!, withName: key)
            }
        }, to: url) {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    guard let JSON = response.result.value as? [String:Any] else {
                        if let error = response.result.error {
                            completion(NetworkLoaderError(
                                networkLoaderErrorTitle: self.generalErrorTitle,
                                networkLoaderErrorBody: error.localizedDescription), nil)
                            return
                        }
                        
                        print("Error getting JSON from response")
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.errorGettingJSONTitle,
                            networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let jsonResults = JSON["result"] as? [String:Any] else {
                        print("Error getting result from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    guard let successInt = jsonResults["success"] as? Int else {
                        print("Error getting orders from JSON")
                        completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                        return
                    }
                    
                    completion(nil, true)
                    return
                }
            case .failure(let encodingError):
                print(encodingError)
                completion(NetworkLoaderError(networkLoaderErrorTitle: "Encoding Error", networkLoaderErrorBody: "Failed to encode image, please try again"), nil)
            }
        }
    }
    
    
    func getExplanations(_ order_id: String, language: String, completion: @escaping (NetworkLoaderError?, [ExplanationAudio]?) -> Void) {
        let url: URL = URL(string: "order/getExplanationAudio", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": order_id,
            "language": language
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                print(JSON);
                
                guard let jsonResults = JSON["result"] as? [[String:Any]] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                var explanationAudios: [ExplanationAudio] = []
                for jsonResult in jsonResults {
                    if let explanationAudio:ExplanationAudio = ExplanationAudio(jsonResult: jsonResult) {
                        explanationAudios.append(explanationAudio)
                    }
                }
                completion(nil, explanationAudios)
        }
    }
    
    func approveOrder(_ orderID: String, completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "order/approve", relativeTo: self.baseURL)!
        
        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = jsonResults["status"] as? Bool else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                completion(nil, status)
        }
    }
    
    func rejectOrder(_ orderID: String, completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "order/reject", relativeTo: self.baseURL)!

        let parameters: Parameters = [
            "orderID": orderID
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = jsonResults["status"] as? Bool else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                completion(nil, status)
        }
    }
    
    func updateSpecialRequest(_ orderID: String, specialRequest: [[String: String]], completion: @escaping (NetworkLoaderError?, Bool?) -> Void) {
        let url: URL = URL(string: "order/updateSpecialRequest", relativeTo: self.baseURL)!
        
        var specialRequestString = "[]"
        if let theJSONData = try? JSONSerialization.data( withJSONObject: specialRequest, options: []) {
            if let theJSONText = String(data: theJSONData, encoding: .ascii) {
                specialRequestString = theJSONText
            }
        }
        
        let parameters: Parameters = [
            "orderID": orderID,
            "specialRequest": specialRequestString
        ]
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters)
            .responseJSON { response in
                guard let JSON = response.result.value as? [String:Any] else {
                    if let error = response.result.error {
                        completion(NetworkLoaderError(
                            networkLoaderErrorTitle: self.generalErrorTitle,
                            networkLoaderErrorBody: error.localizedDescription), nil)
                        return
                    }
                    print("Error getting JSON from response")
                    completion(NetworkLoaderError(
                        networkLoaderErrorTitle: self.errorGettingJSONTitle,
                        networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let jsonResults = JSON["result"] as? [String:Any] else {
                    print("Error getting result from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }
                
                guard let status = jsonResults["status"] as? Bool else {
                    print("Error getting orders from JSON")
                    completion(NetworkLoaderError(networkLoaderErrorTitle: self.errorGettingJSONTitle, networkLoaderErrorBody: self.errorGettingJSONBody), nil)
                    return
                }

                completion(nil, status)
        }
    }
}

