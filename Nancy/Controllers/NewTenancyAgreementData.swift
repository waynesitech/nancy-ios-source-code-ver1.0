//
//  NewTenancyAgreementData.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 10/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import AlamofireImage

class NewTenancyAgreementData {
    static let sharedNewTenancyAgreementData: NewTenancyAgreementData  = NewTenancyAgreementData()
    
    var currentLandlordIteration: Int = 0
    var currentTenantIteration: Int = 0
    var currentlyShowingScreen: Int = 0
    
    func discardTenancyAgreement() {
        currentLandlordIteration = 0
        currentTenantIteration = 0
        currentlyShowingScreen = 0
        newTenancyAgreement = NewTenancyAgreement()
        privPropertyImage = UIImage(named: "previewCamera")!
    }
    
    func repopulateTenancyAgreement(jsonTA: [String : Any]) {
        currentLandlordIteration = 0
        currentTenantIteration = 0
        currentlyShowingScreen = 0
        newTenancyAgreement = NewTenancyAgreement(jsonTA: jsonTA)!
        privPropertyImage = UIImage(named: "previewCamera")!
        if let imageURL = jsonTA["propertyPicture"] as? String {
            let downloader = ImageDownloader()
            let urlRequest = URLRequest(url: URL(string: imageURL)!)
            downloader.download(urlRequest) { response in
                if let image = response.result.value {
                    self.privPropertyImage = image
                }
            }
        }
    }
    
    fileprivate var privNewTenancyAgreement: NewTenancyAgreement?
    var newTenancyAgreement: NewTenancyAgreement {
        get {
            if let newTenancyAgreement = self.privNewTenancyAgreement {
                return newTenancyAgreement
            }
            self.privNewTenancyAgreement = readNewTenancyAgreementFromFile()
            if let newTenancyAgreement = self.privNewTenancyAgreement {
                return newTenancyAgreement
            }
            return NewTenancyAgreement()
        }
        set (newTenancyAgreement) {
            self.privNewTenancyAgreement = newTenancyAgreement
            saveNewTenancyAgreementToFile(newTenancyAgreement)
        }
    }
    
    fileprivate var privPropertyImage: UIImage?
    var propertyImage: UIImage {
        get {
            if let propertyImage = self.privPropertyImage {
                return propertyImage
            }
            
            do {
                let imageData = try Data(contentsOf: self.propertyImageFileURL())
                self.privPropertyImage = UIImage(data: imageData)!
                return self.privPropertyImage!
            } catch {
                return UIImage(named: "previewCamera")!
            }
        }
        set (newPropertyImage) {
            if let imageData = UIImageJPEGRepresentation(newPropertyImage, 1.0) {
                try? imageData.write(to: self.propertyImageFileURL(), options: .atomic)
            }
            self.privPropertyImage = newPropertyImage
        }
    }
    
    func convertLandlordTenantArrayToJSON(landlordTenants: [LandlordTenant]) -> Data {
        var landlordTenantArray: [[String: String]] = []
        for i in 0..<landlordTenants.count {
            landlordTenantArray.append(landlordTenants[i].convertToJSON());
        }
        if let theJSONData = try? JSONSerialization.data( withJSONObject: landlordTenantArray, options: []) {
            if let theJSONText = String(data: theJSONData, encoding: .ascii) {
                return theJSONText.data(using: String.Encoding.ascii)!
            }
        }
        
        return "[]".data(using: .utf8)!
    }
    
    func convertOtherSecuritiesToJSON() -> Data {
        let otherSecurities = newTenancyAgreement.otherSecurityDeposit
        var otherSecuritiesArray: [[String: Any]] = []
        for i in 0..<otherSecurities.count {
            otherSecuritiesArray.append(otherSecurities[i].convertToJSON());
        }
        if let theJSONData = try? JSONSerialization.data( withJSONObject: otherSecuritiesArray, options: []) {
            if let theJSONText = String(data: theJSONData, encoding: .ascii) {
                return theJSONText.data(using: String.Encoding.ascii)!
            }
        }
        return "[]".data(using: .utf8)!
    }
    
    func convertDateToString(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    func convertOptionToRenew() -> String {
        if newTenancyAgreement.isMutuallyAgreedRate {
            return "isMutuallyAgreedRate"
        } else if newTenancyAgreement.isIncreaseOf10percent {
            return "isIncreaseOf10percent"
        } else if newTenancyAgreement.isIncreaseOf15percent {
            return "isIncreaseOf15percent"
        } else if newTenancyAgreement.isOtherPercent {
            return "isOtherPercent"
        } else {
            return "isMarketPrevaillingRate"
        }
    }
    
    func convertPaymentMethod() -> String {
        switch newTenancyAgreement.paymentMethod {
        case 1:
            return "isPostDatedCheque"
        case 2:
            return "isCollectionByLandlord"
        default:
            return "isDirectBankIn"
        }
    }
    
    func convertNotificationMethod() -> String {
        switch newTenancyAgreement.notificationMethod {
        case 1:
            return "isSMS"
        case 2:
            return "isWhatsapp"
        default:
            return "isEmail"
        }
    }
    
    func convertPostDatedMethod() -> String {
        switch newTenancyAgreement.postDatedMethod {
        case 1:
            return "is12Months"
        case 2:
            return "is24Months"
        default:
            return "is6Months"
        }
    }
    
    func convertStringArrayToJSON(stringArray: [String]) -> String {
        if let theJSONData = try? JSONSerialization.data( withJSONObject: stringArray, options: []) {
            if let theJSONText = String(data: theJSONData, encoding: .ascii) {
                return theJSONText
            }
        }
        return "[]"
    }
    
    func convertSpecialRequestToJSON(specialRequestStringArray: [String]) -> String {
        var tempSpecialRequests: [[String: String]] = []
        for oldSpecialRequest in specialRequestStringArray {
            tempSpecialRequests.append(["original": oldSpecialRequest,
                                        "edited": ""])
        }
        if let theJSONData = try? JSONSerialization.data( withJSONObject: tempSpecialRequests, options: []) {
            if let theJSONText = String(data: theJSONData, encoding: .ascii) {
                return theJSONText
            }
        }
        return "[]"
    }
}

extension NewTenancyAgreementData {
    fileprivate func dataFileURL() -> URL {
        let documentsDirectory: URL = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath: URL = documentsDirectory.appendingPathComponent("NewTenancyAgreementData.plist")
        
        return filePath
    }
    
    fileprivate func readNewTenancyAgreementFromFile() -> NewTenancyAgreement {
        if let newTenancyAgreement: NewTenancyAgreement = NSKeyedUnarchiver.unarchiveObject(withFile: self.dataFileURL().path) as? NewTenancyAgreement {
            return newTenancyAgreement
        }
        return NewTenancyAgreement()
    }
    
    fileprivate func saveNewTenancyAgreementToFile(_ newTenancyAgreement: NewTenancyAgreement){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(newTenancyAgreement, toFile: self.dataFileURL().path)
    }
    
    fileprivate func propertyImageFileURL() -> URL {
        let documentsDirectory: URL = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePath: URL = documentsDirectory.appendingPathComponent("PropertyImageData.png")
        
        return filePath
    }
}
