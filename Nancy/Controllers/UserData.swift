//
//  UserData.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 23/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class UserData {
    static let sharedUserData: UserData  = UserData()
    
    fileprivate let preferences = UserDefaults.standard
    
    var userID: Int {
        get {
            return self.preferences.integer(forKey: "USER_ID")
        }
        set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_ID")
            self.preferences.synchronize()
        }
    }
    
    var isPhoneVerify: Bool {
        get {
            return self.preferences.bool(forKey: "USER_IS_PHONE_VERIFIED")
        } set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_IS_PHONE_VERIFIED")
            self.preferences.synchronize()
        }
    }
    
    var name: String {
        get {
            if let name = self.preferences.string(forKey: "USER_NAME") {
                return name
            }
            return ""
        }
        set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_NAME")
            self.preferences.synchronize()
        }
    }
    
    var email: String {
        get {
            if let email = self.preferences.string(forKey: "USER_EMAIL") {
                return email
            }
            return ""
        }
        set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_EMAIL")
            self.preferences.synchronize()
        }
    }
    
    var mobile: String {
        get {
            if let mobile = self.preferences.string(forKey: "USER_MOBILE") {
                return mobile
            }
            return ""
        }
        set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_MOBILE")
            self.preferences.synchronize()
        }
    }
    
    var status: Int {
        get {
            return self.preferences.integer(forKey: "USER_STATUS")
        }
        set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_STATUS")
            self.preferences.synchronize()
        }
    }
    
    var role: Int {
        get {
            return self.preferences.integer(forKey: "USER_ROLE")
        }
        set (newValue) {
            self.preferences.setValue(newValue, forKey: "USER_ROLE")
            self.preferences.synchronize()
        }
    }
    
    func ParseUserFromJsonResponse(jsonData: [String : Any]) {

        if let userID: Int = jsonData["id"] as? Int {
            self.userID = userID
        }
        
        if let name: String = jsonData["name"] as? String {
            self.name = name
        }
        
        if let email: String = jsonData["email"] as? String {
            self.email = email
        }
        
        if let mobile: String = jsonData["mobile"] as? String {
            self.mobile = mobile
        }
        
        if let status: Int = jsonData["status"] as? Int {
            self.status = status
        }
        
        if let roleJson: [String : Any] = jsonData["role"] as? [String : Any] {
            if let role: Int = roleJson["id"] as? Int {
                self.role = role
            }
        }
        
        if let isPhoneVerifyStr: String = jsonData["isPhoneVerify"] as? String {
            if (isPhoneVerifyStr == "true") {
                self.isPhoneVerify = true
            } else {
                self.isPhoneVerify = false
            }
        }
    }
    
    func logoutUser() {
        self.userID = 0
        self.name = ""
        self.email = ""
        self.mobile = ""
        self.status = 0
        self.role = 0
        self.isPhoneVerify = false
    }
}
