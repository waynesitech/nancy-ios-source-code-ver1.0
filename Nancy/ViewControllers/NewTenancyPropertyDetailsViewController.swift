//
//  NewTenancyPropertyDetailsViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 06/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD
import DLRadioButton

class NewTenancyPropertyDetailsViewController: UITableViewController {
    
    @IBOutlet weak var isResidentialRadio: DLRadioButton!
    @IBOutlet weak var isCommercialRadio: DLRadioButton!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var postcodeTextField: UITextField!
    @IBOutlet weak var townTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    let picker = UIImagePickerController()
    var currentlySelectedPropertyType = 0
    // 0 - isResidential
    // 1 - isCommercial
    
    @IBAction func propertyTypeRadioChanged(_ sender: DLRadioButton) {
        if sender == isResidentialRadio {
            currentlySelectedPropertyType = 0
        } else {
            currentlySelectedPropertyType = 1
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Would you like to save your changes?", preferredStyle: .alert)
        let dismissAction: UIAlertAction = UIAlertAction(title: "No", style: .default, handler: {_ in
            self.performSegue(withIdentifier: "backToMenu", sender: nil)
            NewTenancyAgreementData.sharedNewTenancyAgreementData.discardTenancyAgreement()
        })
        alert.addAction(dismissAction)
        let alertAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: {_ in
            self.performSegue(withIdentifier: "backToMenu", sender: nil)
        })
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        roundImageView()
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 1
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(NewTenancyPropertyDetailsViewController.previewImagePressed(gesture:)))
        previewImageView.addGestureRecognizer(tapGesture)
        previewImageView.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        streetTextField.text = newTenancyAgreement.streetName
        townTextField.text = newTenancyAgreement.town
        postcodeTextField.text = newTenancyAgreement.postCode
        stateTextField.text = newTenancyAgreement.states
        previewImageView.image = NewTenancyAgreementData.sharedNewTenancyAgreementData.propertyImage
        if newTenancyAgreement.isResidential {
            isResidentialRadio.deselectOtherButtons()
            isResidentialRadio.isSelected = true
            currentlySelectedPropertyType = 0
        } else {
            isCommercialRadio.deselectOtherButtons()
            isCommercialRadio.isSelected = true
            currentlySelectedPropertyType = 1
        }
        if newTenancyAgreement.currentStep == -1 {
            newTenancyAgreement.currentStep = 0
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        newTenancyAgreement.streetName = streetTextField.text!
        newTenancyAgreement.town = townTextField.text!
        newTenancyAgreement.postCode = postcodeTextField.text!
        newTenancyAgreement.states = stateTextField.text!
        if currentlySelectedPropertyType == 0 {
            newTenancyAgreement.isResidential = true
        } else {
            newTenancyAgreement.isResidential = false
        }
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectStateSegue" {
            let selectStateVC: SelectStateViewController = segue.destination as! SelectStateViewController
            selectStateVC.state = self.stateTextField.text
            selectStateVC.didSelectStateProtocolDelegate = self
        } else if segue.identifier == "proceedFromPropertyDetails" {
            let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
            newTenancyAgreement.streetName = streetTextField.text!
            newTenancyAgreement.town = townTextField.text!
            newTenancyAgreement.postCode = postcodeTextField.text!
            newTenancyAgreement.states = stateTextField.text!
            newTenancyAgreement.currentStep = 1
            if currentlySelectedPropertyType == 0 {
                newTenancyAgreement.isResidential = true
            } else {
                newTenancyAgreement.isResidential = false
            }
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        if newTenancyAgreement.currentStep > 2 {
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 2
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = 0
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = 0
            performSegue(withIdentifier: "proceedFromPropertyDetailsSkippingPartySelect", sender: nil)
        } else {
            performSegue(withIdentifier: "proceedFromPropertyDetails", sender: nil)
        }
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "proceedFromPropertyDetails" || identifier == "proceedFromPropertyDetailsSkippingPartySelect" {
            return everythingValid()
        }
        return true
    }
    
    func everythingValid() -> Bool {
        if streetTextField.text!.characters.count < 1 {
            showAlertMessage(errorMessage: "Street can not be empty")
            return false
        }
        if townTextField.text!.characters.count < 1 {
            showAlertMessage(errorMessage: "Town can not be empty")
            return false
        }
        if postcodeTextField.text!.characters.count < 4 || postcodeTextField.text!.characters.count > 7 {
            showAlertMessage(errorMessage: "Postcode is not valid")
            return false
        }
        if stateTextField.text!.characters.count < 1 {
            showAlertMessage(errorMessage: "State is not valid")
            return false
        }
        return true
    }
    
    func showAlertMessage(errorMessage: String) {
        let alert = UIAlertController(title: "", message: errorMessage, preferredStyle: .alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension NewTenancyPropertyDetailsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn (_ textField: UITextField) -> Bool{
        if (textField == streetTextField){
            townTextField.becomeFirstResponder()
        } else if (textField == townTextField){
            postcodeTextField.becomeFirstResponder()
        } else if (textField == postcodeTextField){
            stateTextField.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == stateTextField {
            self.view.endEditing(true)
            self.performSegue(withIdentifier: "selectStateSegue", sender: nil)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == stateTextField {
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension NewTenancyPropertyDetailsViewController : didSelectStateProtocol {
    func didSelectState(selectedState: String) {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        newTenancyAgreement.states = selectedState
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        self.stateTextField.text = selectedState
    }
}



extension NewTenancyPropertyDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func previewImagePressed(gesture: UIGestureRecognizer) {
        if let _ = gesture.view as? UIImageView {
            let alertVC = UIAlertController(
                title: "Property Image",
                message: "Select an image from library or capture with camera.",
                preferredStyle: .alert)
            let cameraAction = UIAlertAction(
                title: "Camera",
                style:.default,
                handler: {(alert: UIAlertAction!) in self.shootPhoto()})
            alertVC.addAction(cameraAction)
            let photoLibraryAction = UIAlertAction(
                title: "Library",
                style:.default,
                handler: {(alert: UIAlertAction!) in self.photoFromLibrary()})
            alertVC.addAction(photoLibraryAction)
            present(alertVC, animated: true, completion: nil)
        }
    }
    
    func roundImageView() {
        previewImageView.layer.cornerRadius = previewImageView.frame.size.width / 2
        previewImageView.layer.masksToBounds = true
        previewImageView.clipsToBounds = true
        previewImageView.contentMode = .scaleAspectFill
    }
    
    func photoFromLibrary() {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
    }
    
    func shootPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        var chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        previewImageView.image = chosenImage
        NewTenancyAgreementData.sharedNewTenancyAgreementData.propertyImage = chosenImage
        
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
