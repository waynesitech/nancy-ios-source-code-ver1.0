//
//  SignInViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 17/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class SignInViewController: UIViewController {
    
    @IBOutlet weak var contentScroll : UIScrollView!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var passwordErrorIcon: UIImageView!
    @IBOutlet weak var emailErrorIcon: UIImageView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    
    @IBAction func signInPressed(_ sender: AnyObject) {
        let email = emailTextField.text!
        let password = passwordTextField.text!
        
        emailErrorIcon.isHidden = true
        passwordErrorIcon.isHidden = true
        errorMessageLabel.text = ""
        
        if email.characters.count == 0 {
            errorMessageLabel.text = "Email can not be empty."
            emailErrorIcon.isHidden = false
            emailTextField.becomeFirstResponder()
            return
        } else if password.characters.count == 0 {
            errorMessageLabel.text = "Password can not be empty."
            passwordErrorIcon.isHidden = false
            passwordTextField.becomeFirstResponder()
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.login(email, password: password) {
            networkLoaderError, jsonResult in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                // Handle Error Message
                self.errorMessageLabel.text = "\(networkLoaderError.networkLoaderErrorTitle)\n\(networkLoaderError.networkLoaderErrorBody)"
            } else if (!UserData.sharedUserData.isPhoneVerify) {
                self.performSegue(withIdentifier: "showPhoneVerificationSegue", sender: nil)
            } else {
                self.performSegue(withIdentifier: "showWelcomeSegue", sender: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(SignInViewController.willShowKeyBoard(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignInViewController.willHideKeyBoard(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let bold = [
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 13.0),
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let black = [
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12.0),
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        let signUpLabel = NSMutableAttributedString(string:"Don't have an account?", attributes:black)
        signUpLabel.append(NSMutableAttributedString(string:" Sign Up ", attributes:bold))
        signUpLabel.append(NSMutableAttributedString(string:"now.", attributes:black))
        signUpButton.setAttributedTitle(signUpLabel, for: .normal)
        
        let forgotPasswordLabel = NSMutableAttributedString(string:"Forgot Password?", attributes:bold)
        forgotPasswordButton.setAttributedTitle(forgotPasswordLabel, for: .normal)
        
        emailErrorIcon.isHidden = true
        passwordErrorIcon.isHidden = true
        errorMessageLabel.text = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showWelcomeSegue") {
            let destinationVC: SignInOutSuccessfulViewController = segue.destination as! SignInOutSuccessfulViewController
            destinationVC.fromLoginScreen = true
        }
    }
    
}

extension SignInViewController: UITextFieldDelegate {
    
    @objc func willShowKeyBoard(_ notification : Notification){
        self.contentScroll.contentInset.bottom = 184
    }
    
    @objc func willHideKeyBoard(_ notification : Notification){
        self.contentScroll.contentInset.bottom = 0
    }
    
    func textFieldShouldReturn (_ textField: UITextField) -> Bool{
        if ((textField == emailTextField)){
            passwordTextField.becomeFirstResponder()
        } else if (textField == passwordTextField){
            passwordTextField.resignFirstResponder()
            self.contentScroll.contentInset.bottom = 0
        }
        return true
    }
    
}
