//
//  PhoneVerificationViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 22/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class PhoneVerificationViewController: UIViewController {
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var verificationCodeTextField: UITextField!
    @IBOutlet weak var errorIcon: UIImageView!
    
    let phoneNumber = UserData.sharedUserData.mobile
    
    let tinyBlack = [
        NSAttributedStringKey.font : UIFont.systemFont(ofSize: 13.0),
        NSAttributedStringKey.foregroundColor : UIColor.black
    ]
    
    let tinyItalic = [
        NSAttributedStringKey.font : UIFont.italicSystemFont(ofSize: 13.0),
        NSAttributedStringKey.foregroundColor : UIColor.black,
        NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue
        ] as [NSAttributedStringKey : Any]
    
    @IBAction func resendButtonPressed(_ sender: Any) {
        resetErrorMessage()
        NetworkLoader.sharedLoader.requestPhoneVerification {
            (networkLoaderError, requestSuccessful) in
            if let networkLoaderError = networkLoaderError {
                // Handle the Error
                self.showErrorMessage(networkLoaderError.networkLoaderErrorTitle,
                                      message: networkLoaderError.networkLoaderErrorBody)
            }
            if requestSuccessful != nil {
                // Let user know that it is successful
                self.showErrorMessage("", message: "NanCy have resent a Verification Code to \(UserData.sharedUserData.mobile).")
            }
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        NetworkLoader.sharedLoader.submitPhoneVerification(verificationCodeTextField.text!) {
            (networkLoaderError, requestSuccessful) in
            if let requestSuccessful = requestSuccessful {
                if !requestSuccessful {
                    self.errorIcon.isHidden = false
                    let errorMessageText = NSMutableAttributedString(string:"Invalid Verification Code. Please try again.\nClick resend to request a new one.", attributes:self.tinyBlack)
                    self.errorMessageLabel.attributedText = errorMessageText
                } else {
                    UserData.sharedUserData.isPhoneVerify = true
                    self.performSegue(withIdentifier: "showWelcomeSegue", sender: nil)
                }
            } else if let networkLoaderError = networkLoaderError {
                // Handle the Error
                self.showErrorMessage(networkLoaderError.networkLoaderErrorTitle,
                                      message: networkLoaderError.networkLoaderErrorBody)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resetErrorMessage()
        
        informationLabel.text = "NanCy have sent a Verification Code\nto \(phoneNumber)"
    }
    
    func resetErrorMessage() {
        verificationCodeTextField.text = ""
        errorIcon.isHidden = true
        
        let errorMessageText = NSMutableAttributedString(string:"If you do not receive your Verification Code in ", attributes:tinyBlack)
        errorMessageText.append(NSMutableAttributedString(string:"60", attributes:tinyItalic))
        errorMessageText.append(NSMutableAttributedString(string:" seconds, please click resend to request a new one.", attributes:tinyBlack))
        errorMessageLabel.attributedText = errorMessageText
    }
    
    func showErrorMessage(_ title: String, message: String) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alertController.addAction( UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil)
        )
        self.present(alertController, animated: true, completion: nil)
    }
    
}
