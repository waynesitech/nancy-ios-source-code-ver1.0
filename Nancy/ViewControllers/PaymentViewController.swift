//
//  PaymentViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 28/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//
import UIKit

class PaymentViewController: UIViewController, PaymentResultDelegate {
    func requeryFailed(_ refNo: String!, withMerchantCode merchantCode: String!, withAmount amount: String!, withErrDesc errDesc: String!) {
        
    }

    func requerySuccess(_ refNo: String!, withMerchantCode merchantCode: String!, withAmount amount: String!, withResult result: String!) {
        
    }
    
    @IBOutlet var paymentView: UIView!
    
    var batchId: Int = 0
    
    var packageId: String = ""
    var strWebUrl: String = ""
    var strRemark: String = ""
    var strIpayPrice: String = ""
    var strDescription: String = ""
    
    var paymentSdk: Ipay = Ipay()
    var iPay88PaymentView: UIView?
    var callbackId: String = ""
    var isPaymentInProgress: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Payment"
        edgesForExtendedLayout = []
        paymentSdk.delegate = self
        let payment = IpayPayment()
        payment.refNo = packageId
        payment.amount = strIpayPrice
        payment.backendPostURL = strWebUrl
        payment.prodDesc = strDescription
        payment.remark = strRemark
        payment.userName = UserData.sharedUserData.name
        payment.userEmail = UserData.sharedUserData.email
        payment.userContact = UserData.sharedUserData.mobile
        payment.paymentId = "16"
        payment.merchantKey = "Mp7FIn6fFM"
        payment.merchantCode = "M08747"
        payment.currency = "MYR"
        payment.lang = "ISO-8859-1"
        payment.country = "MY"
        iPay88PaymentView = paymentSdk.checkout(payment)
        iPay88PaymentView?.frame = paymentView.frame
        paymentView.addSubview(iPay88PaymentView!)
    }
    
    func paymentSuccess(_ refNo: String, withTransId transId: String, withAmount amount: String, withRemark remark: String, withAuthCode authCode: String) {
        let alert = UIAlertController(title: "", message: "You have successfully completed your transaction", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in self.navigationController?.popViewController(animated: true) }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func requery(_ payment: IpayPayment) {
    }
    
    func paymentFailed(_ refNo: String, withTransId transId: String, withAmount amount: String, withRemark remark: String, withErrDesc errDesc: String) {
        let alert = UIAlertController(title: "", message: errDesc, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in self.navigationController?.popViewController(animated: true) }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func paymentCancelled(_ refNo: String, withTransId transId: String, withAmount amount: String, withRemark remark: String, withErrDesc errDesc: String) {
        self.navigationController?.popViewController(animated: true)
    }
}
