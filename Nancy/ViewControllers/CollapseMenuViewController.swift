//
//  CollapseMenuViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 23/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import SWRevealViewController

class CollapseMenuViewController: UIViewController {
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameLabel.text = UserData.sharedUserData.name.uppercased()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logout" {
            UserData.sharedUserData.logoutUser()
        }
    }
}
