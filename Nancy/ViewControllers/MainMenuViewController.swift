//
//  MainMenuViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 23/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import SWRevealViewController

class MainMenuViewController: UIViewController {
    
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var navDrawerButton: UIBarButtonItem!
    @IBOutlet weak var tenancyListButton: UIButton!
    @IBOutlet weak var generateNewTAButton: UIButton!
    @IBOutlet weak var ESignButton: UIButton!
    @IBOutlet weak var EStampButton: UIButton!
    @IBOutlet weak var autoReminderButton: UIButton!
    @IBOutlet weak var explanationOfClausesButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navDrawerButton.target = self.revealViewController()
        navDrawerButton.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
        greetingsLabel.text = "Hi \(UserData.sharedUserData.name),\nWhat can I help you today?"
        
        tenancyListButton.imageView?.contentMode = .scaleAspectFit
        generateNewTAButton.imageView?.contentMode = .scaleAspectFit
        ESignButton.imageView?.contentMode = .scaleAspectFit
        EStampButton.imageView?.contentMode = .scaleAspectFit
        autoReminderButton.imageView?.contentMode = .scaleAspectFit
        explanationOfClausesButton.imageView?.contentMode = .scaleAspectFit
    }
    @IBAction func newTenancyAgreementButtonPressed(_ sender: Any) {
        if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.currentStep > -1 {
            let alert = UIAlertController(title: "", message: "You have an unsubmitted Tenancy Agreement. Would you like to continue editing?", preferredStyle: .alert)
            let dismissAction: UIAlertAction = UIAlertAction(title: "Discard", style: .default, handler: {_ in
                NewTenancyAgreementData.sharedNewTenancyAgreementData.discardTenancyAgreement()
                self.performSegue(withIdentifier: "goToNewTenancyAgreement", sender: nil)
            })
            alert.addAction(dismissAction)
            let alertAction: UIAlertAction = UIAlertAction(title: "Continue Editing", style: .default, handler: {_ in
                self.performSegue(withIdentifier: "goToEditPropertyDetail", sender: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            NewTenancyAgreementData.sharedNewTenancyAgreementData.discardTenancyAgreement()
            self.performSegue(withIdentifier: "goToNewTenancyAgreement", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToEsignMenu" {
            let destinationNavigationController = segue.destination as! UINavigationController
            let ESignEstampAlarmMenuVC = destinationNavigationController.topViewController as! ESignEstampAlarmMenu
            ESignEstampAlarmMenuVC.menuType = "E-Sign"
        } else if segue.identifier == "goToEstampMenu" {
            let destinationNavigationController = segue.destination as! UINavigationController
            let ESignEstampAlarmMenuVC = destinationNavigationController.topViewController as! ESignEstampAlarmMenu
            ESignEstampAlarmMenuVC.menuType = "E-Stamp"
        } else if segue.identifier == "goToAutoReminderMenu" {
            let destinationNavigationController = segue.destination as! UINavigationController
            let ESignEstampAlarmMenuVC = destinationNavigationController.topViewController as! ESignEstampAlarmMenu
            ESignEstampAlarmMenuVC.menuType = "Auto Reminder"
        }
    }
}
