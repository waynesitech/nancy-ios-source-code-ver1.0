//
//  NewTenancyPreviewViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class NewTenancyPreviewViewController: UITableViewController {
    
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF != nil {
            let alert = UIAlertController(title: "Reminder", message: "Please take note that once confirm the details here, no changes will be accepted.", preferredStyle: .alert)
            let noAction: UIAlertAction = UIAlertAction(title: "Go Back", style: .default, handler: nil)
            alert.addAction(noAction)
            let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {(_) in self.proceedToUploadPDF()})
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isEditTA {
            let alert = UIAlertController(title: "", message: "Update Your Tenancy Agreement?", preferredStyle: .alert)
            let noAction: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: nil)
            alert.addAction(noAction)
            let alertAction: UIAlertAction = UIAlertAction(title: "YES", style: .default, handler: {(_) in self.proceedToEdit()})
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "", message: "Render Your Tenancy Agreement?", preferredStyle: .alert)
            let noAction: UIAlertAction = UIAlertAction(title: "NO", style: .default, handler: nil)
            alert.addAction(noAction)
            let alertAction: UIAlertAction = UIAlertAction(title: "YES", style: .default, handler: {(_) in self.proceedToRender()})
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // 0 - Property Details
        // 1 - Landlords Details
        // 2 - Tenants Details
        // 3 - Rent Details
        // 4 - Security Deposit
        // 5 - Rental Free Period
        // 6 - Car Parks
        // 7 - Payment Details
        // 8 - Special Requests
        // 9 - Fixtures
        return 10
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        } else if section == 1 {
            return NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.landlords.count + 1
        } else if section == 2 {
            return NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenants.count + 1
        } else if section == 3 {
            return 7
        } else if section == 4 {
            return 3 + NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.otherSecurityDeposit.count
        } else if section == 5 {
            return 1
        } else if section == 6 {
            return 1 + NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.carParkLots.count
        } else if section == 7 {
            if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF != nil {
                return 0
            }
            switch NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.paymentMethod {
            case 0:
                return 6
            case 1:
                return 3
            case 2:
                return 2
            default:
                return 1
            }
        } else if section == 8 {
            if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF != nil {
                return 0
            }
            return NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.specialRequest.count + 1
        } else if section == 9 {
            if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF != nil {
                return 0
            }
            return NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.fixturesFitting.count + 1
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Property Details")
                return previewMainTitleTableViewCell
            } else if indexPath.row == 1 {
                let previewPropertyDetailsTableViewCell: PreviewPropertyDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPropertyDetailsTableViewCell", for: indexPath) as! PreviewPropertyDetailsTableViewCell
                var propertyType = "Residential"
                if !NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isResidential {
                    propertyType = "Commercial"
                }
                previewPropertyDetailsTableViewCell.setupContent(propertyPreviewImage: NewTenancyAgreementData.sharedNewTenancyAgreementData.propertyImage, propertyType: propertyType, address: "\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.streetName),\n\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.town),\n\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.postCode) \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.states), Malaysia.")
                return previewPropertyDetailsTableViewCell
                
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Landlord Details")
                return previewMainTitleTableViewCell
            }
            if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.landlords[indexPath.row - 1].isCompany {
                let previewLandlordTenantCompanyDetailsTableViewCell: PreviewLandlordTenantCompanyDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewLandlordTenantCompanyDetailsTableViewCell", for: indexPath) as! PreviewLandlordTenantCompanyDetailsTableViewCell
                previewLandlordTenantCompanyDetailsTableViewCell.title = "Landlord \(indexPath.row) Details (Company)"
                previewLandlordTenantCompanyDetailsTableViewCell.landlordTenant = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.landlords[indexPath.row - 1]
                previewLandlordTenantCompanyDetailsTableViewCell.updateUI()
                return previewLandlordTenantCompanyDetailsTableViewCell
            } else {
                let previewLandlordTenantIndividualDetailsTableViewCell: PreviewLandlordTenantIndividualDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewLandlordTenantIndividualDetailsTableViewCell", for: indexPath) as! PreviewLandlordTenantIndividualDetailsTableViewCell
                previewLandlordTenantIndividualDetailsTableViewCell.title = "Landlord \(indexPath.row) Details (Individual)"
                previewLandlordTenantIndividualDetailsTableViewCell.landlordTenant = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.landlords[indexPath.row - 1]
                previewLandlordTenantIndividualDetailsTableViewCell.updateUI()
                return previewLandlordTenantIndividualDetailsTableViewCell
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Tenant Details")
                return previewMainTitleTableViewCell
            }
            if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenants[indexPath.row - 1].isCompany {
                let previewLandlordTenantCompanyDetailsTableViewCell: PreviewLandlordTenantCompanyDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewLandlordTenantCompanyDetailsTableViewCell", for: indexPath) as! PreviewLandlordTenantCompanyDetailsTableViewCell
                previewLandlordTenantCompanyDetailsTableViewCell.title = "Tenant \(indexPath.row) Details (Company)"
                previewLandlordTenantCompanyDetailsTableViewCell.landlordTenant = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenants[indexPath.row - 1]
                previewLandlordTenantCompanyDetailsTableViewCell.updateUI()
                return previewLandlordTenantCompanyDetailsTableViewCell
            } else {
                let previewLandlordTenantIndividualDetailsTableViewCell: PreviewLandlordTenantIndividualDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewLandlordTenantIndividualDetailsTableViewCell", for: indexPath) as! PreviewLandlordTenantIndividualDetailsTableViewCell
                previewLandlordTenantIndividualDetailsTableViewCell.title = "Tenant \(indexPath.row) Details (Individual)"
                previewLandlordTenantIndividualDetailsTableViewCell.landlordTenant = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenants[indexPath.row - 1]
                previewLandlordTenantIndividualDetailsTableViewCell.updateUI()
                return previewLandlordTenantIndividualDetailsTableViewCell
            }
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Rent Details")
                return previewMainTitleTableViewCell
            } else if indexPath.row == 1 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM yyyy"
                let commencementDateInString = formatter.string(from: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.commencementDate)
                previewTitleContentTableViewCell.setupContent(title: "Commencement Date", content: commencementDateInString)
                return previewTitleContentTableViewCell
            } else if indexPath.row == 2 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                previewTitleContentTableViewCell.setupContent(title: "Term of the Tenancy", content: "\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.termsOfTenancyYears) Years \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.termsOfTenancyMonths) Months")
                return previewTitleContentTableViewCell
            } else if indexPath.row == 3 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isOptionToRenew {
                    previewTitleContentTableViewCell.setupContent(title: "Option to renew", content: "\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.optionToRenewYears) Years \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.optionToRenewMonths) Months")
                } else {
                    previewTitleContentTableViewCell.setupContent(title: "Option to renew", content: "No")
                }
                return previewTitleContentTableViewCell
            }  else if indexPath.row == 4 && NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isOptionToRenew{
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                var termsOfRenewal = ""
                if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isMarketPrevaillingRate {
                    termsOfRenewal = "market prevailing rate"
                } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isIncreaseOf10percent {
                    termsOfRenewal = "icrease of 10% from the last rental rate"
                } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isIncreaseOf15percent {
                    termsOfRenewal = "icrease of 15% from the last rental rate"
                } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isMutuallyAgreedRate {
                    termsOfRenewal = "at a mutually agreed rate"
                } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isOtherPercent {
                    termsOfRenewal = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.otherTermsofRenewal
                }
                previewPaddedTitleContentTableViewCell.setupContent(title: "Term of Renewal", content: termsOfRenewal)
                return previewPaddedTitleContentTableViewCell
            } else if indexPath.row == 5 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                previewTitleContentTableViewCell.setupContent(title: "Rental", content: "RM \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.rental)")
                return previewTitleContentTableViewCell
            } else if indexPath.row == 6 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                previewTitleContentTableViewCell.setupContent(title: "Advanced Rental", content: "RM \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.advanceRental)")
                return previewTitleContentTableViewCell
            }
        } else if indexPath.section == 4 {
            if indexPath.row == 0 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                previewTitleContentTableViewCell.setupContent(title: "Security Deposit", content: "")
                return previewTitleContentTableViewCell
            } else if indexPath.row == 1 {
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                previewPaddedTitleContentTableViewCell.setupContent(title: "Rent", content: "RM \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.securityDepositRent)")
                return previewPaddedTitleContentTableViewCell
            } else if indexPath.row == 2 {
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                previewPaddedTitleContentTableViewCell.setupContent(title: "Utilities", content: "RM \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.securityDepositUtilities)")
                return previewPaddedTitleContentTableViewCell
            }
            let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
            let otherSecurityDeposit = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.otherSecurityDeposit[indexPath.row - 3]
            previewPaddedTitleContentTableViewCell.setupContent(title: otherSecurityDeposit.name, content: "RM \(otherSecurityDeposit.amount)")
            return previewPaddedTitleContentTableViewCell
        } else if indexPath.section == 5 && indexPath.row == 0 {
            let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
            if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.isRentalFreePeriodDuration {
                previewTitleContentTableViewCell.setupContent(title: "Rental Free Period", content: "\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.rentalFreePeriodYears) Years \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.rentalFreePeriodMonths) Months \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.rentalFreePeriodWeeks) Weeks")
            } else {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM yyyy"
                let rentalFreePeriodStartDateInString = formatter.string(from: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.rentalFreePeriodStartDate)
                let rentalFreePeriodEndDateInString = formatter.string(from: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.rentalFreePeriodEndDate)
                previewTitleContentTableViewCell.setupContent(title: "Rental Free Period", content: "\(rentalFreePeriodStartDateInString) - \(rentalFreePeriodEndDateInString)")
            }
            return previewTitleContentTableViewCell
        } else if indexPath.section == 6 {
            if indexPath.row == 0 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                previewTitleContentTableViewCell.setupContent(title: "Car Park (\(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.carParkLots.count) lots)", content: "")
                return previewTitleContentTableViewCell
            }
            let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
            previewPaddedTitleContentTableViewCell.setupContent(title: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.carParkLots[indexPath.row - 1], content: "")
            return previewPaddedTitleContentTableViewCell
        } else if indexPath.section == 7 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Payment Details")
                return previewMainTitleTableViewCell
            } else if indexPath.row == 1 {
                let previewTitleContentTableViewCell: PreviewTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewTitleContentTableViewCell", for: indexPath) as! PreviewTitleContentTableViewCell
                switch NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.paymentMethod {
                case 0:
                    previewTitleContentTableViewCell.setupContent(title: "Direct Bank in", content: "")
                    break
                case 1:
                    previewTitleContentTableViewCell.setupContent(title: "Post-Dated Cheque", content: "")
                    break
                case 2:
                    previewTitleContentTableViewCell.setupContent(title: "Collection by Landlord", content: "")
                    break
                default:
                    break
                }
                return previewTitleContentTableViewCell
            } else if indexPath.row == 2 {
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.paymentMethod == 0 {
                    previewPaddedTitleContentTableViewCell.setupContent(title: "Name of Account Holder", content: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.paymentAccountName)
                } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.paymentMethod == 1 {
                    switch NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.postDatedMethod {
                    case 0:
                        previewPaddedTitleContentTableViewCell.setupContent(title: "6 Months", content: "")
                    case 1:
                        previewPaddedTitleContentTableViewCell.setupContent(title: "12 Months", content: "")
                    case 2:
                        previewPaddedTitleContentTableViewCell.setupContent(title: "24 Months", content: "")
                    default:
                        break
                    }
                    
                }
                return previewPaddedTitleContentTableViewCell
            } else if indexPath.row == 3 {
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                previewPaddedTitleContentTableViewCell.setupContent(title: "Bank", content: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.bank)
                return previewPaddedTitleContentTableViewCell
            } else if indexPath.row == 4 {
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                previewPaddedTitleContentTableViewCell.setupContent(title: "Bank Account Number", content: NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.bankAccountNum)
                return previewPaddedTitleContentTableViewCell
            } else if indexPath.row == 5 {
                let previewPaddedTitleContentTableViewCell: PreviewPaddedTitleContentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewPaddedTitleContentTableViewCell", for: indexPath) as! PreviewPaddedTitleContentTableViewCell
                switch NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.notificationMethod {
                case 0:
                    previewPaddedTitleContentTableViewCell.setupContent(title: "Method of notification upon payment", content: "Email")
                case 1:
                    previewPaddedTitleContentTableViewCell.setupContent(title: "Method of notification upon payment", content: "SMS")
                case 2:
                    previewPaddedTitleContentTableViewCell.setupContent(title: "Method of notification upon payment", content: "Whatsapp")
                default:
                    break
                }
                return previewPaddedTitleContentTableViewCell
            }
        } else if indexPath.section == 8 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Special Conditions")
                return previewMainTitleTableViewCell
            }
            let previewContentOnlyTableViewCell: PreviewContentOnlyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewContentOnlyTableViewCell", for: indexPath) as! PreviewContentOnlyTableViewCell
            previewContentOnlyTableViewCell.setupContent(content: "\(indexPath.row). \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.specialRequest[indexPath.row - 1])")
            return previewContentOnlyTableViewCell
        } else if indexPath.section == 9 {
            if indexPath.row == 0 {
                let previewMainTitleTableViewCell: PreviewMainTitleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewMainTitleTableViewCell", for: indexPath) as! PreviewMainTitleTableViewCell
                previewMainTitleTableViewCell.setupTitle(title: "Fixtures & Fittings")
                return previewMainTitleTableViewCell
            }
            let previewContentOnlyTableViewCell: PreviewContentOnlyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PreviewContentOnlyTableViewCell", for: indexPath) as! PreviewContentOnlyTableViewCell
            previewContentOnlyTableViewCell.setupContent(content: "\(indexPath.row). \(NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.fixturesFitting[indexPath.row - 1])")
            return previewContentOnlyTableViewCell
        }
        
        return tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "renderTenancyAgreement" {
            let tenancyAgreementDetailsVC: TenancyAgreementDetailsViewController = (segue.destination as? TenancyAgreementDetailsViewController)!
            tenancyAgreementDetailsVC.orderID = self.tenancyAgreement.order_id
            tenancyAgreementDetailsVC.tenancyAgreement = self.tenancyAgreement
        }
    }
    
    func proceedToRender() {
        if let view: UIView = self.tableView.superview {
            MBProgressHUD.showAdded(to: view, animated: true)
            NetworkLoader.sharedLoader.createNewOrder() {
                networkLoaderError, tenancyAgreement in
                MBProgressHUD.hide(for: view, animated: true)
                if let networkLoaderError = networkLoaderError {
                    // Handle the Error
                    self.showErrorMessage(networkLoaderError.networkLoaderErrorTitle,
                                          message: networkLoaderError.networkLoaderErrorBody)
                } else if let tenancyAgreement = tenancyAgreement {
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = NewTenancyAgreement()
                    self.tenancyAgreement = tenancyAgreement
                    self.performSegue(withIdentifier: "renderTenancyAgreement", sender: nil)
                }
            }
        }
    }
    
    func proceedToEdit() {
        if let view: UIView = self.tableView.superview {
            MBProgressHUD.showAdded(to: view, animated: true)
            NetworkLoader.sharedLoader.editOrder() {
                networkLoaderError, tenancyAgreement in
                MBProgressHUD.hide(for: view, animated: true)
                if let networkLoaderError = networkLoaderError {
                    // Handle the Error
                    self.showErrorMessage(networkLoaderError.networkLoaderErrorTitle,
                                          message: networkLoaderError.networkLoaderErrorBody)
                } else if let tenancyAgreement = tenancyAgreement {
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = NewTenancyAgreement()
                    self.tenancyAgreement = tenancyAgreement
                    self.performSegue(withIdentifier: "renderTenancyAgreement", sender: nil)
                }
            }
        }
    }
    
    func proceedToUploadPDF() {
        if let view: UIView = self.tableView.superview {
            MBProgressHUD.showAdded(to: view, animated: true)
            NetworkLoader.sharedLoader.createNewOrderWithPDF() {
                networkLoaderError, tenancyAgreement in
                MBProgressHUD.hide(for: view, animated: true)
                if let networkLoaderError = networkLoaderError {
                    // Handle the Error
                    self.showErrorMessage(networkLoaderError.networkLoaderErrorTitle,
                                          message: networkLoaderError.networkLoaderErrorBody)
                } else if let tenancyAgreement = tenancyAgreement {
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = NewTenancyAgreement()
                    self.tenancyAgreement = tenancyAgreement
                    self.performSegue(withIdentifier: "renderTenancyAgreement", sender: nil)
                }
            }
        }
    }
    
    func showErrorMessage(_ title: String, message: String) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alertController.addAction( UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil)
        )
        self.present(alertController, animated: true, completion: nil)
    }
    
}
