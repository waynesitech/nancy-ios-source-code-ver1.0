//
//  TenancyAgreementsViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 19/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class TenancyAgreementsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tenancyAgreements: [TenancyAgreement] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().tintColor = UIColor.white
        NetworkLoader.sharedLoader.listTenancyAgreement {
            (networkLoaderError, tenancyAgreements) in
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            
            if let tenancyAgreements = tenancyAgreements {
                self.tenancyAgreements = tenancyAgreements
                self.tableView.reloadData()
            }
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 162.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTenancyAgreementDetailSegue" {
            let tenancyAgreementDetailsVC: TenancyAgreementDetailsViewController = (segue.destination as? TenancyAgreementDetailsViewController)!
            tenancyAgreementDetailsVC.orderID = tenancyAgreements[(tableView.indexPathForSelectedRow?.row)!].order_id
            tenancyAgreementDetailsVC.tenancyAgreement = tenancyAgreements[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
}

extension TenancyAgreementsViewController : NavigateToRenewTAProtocol {
    func didSelectRenewButton(_ orderID: String) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.getTenancyAgreementFullDetail(orderID) {
            (networkLoaderError, tenancyAgreement) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            } else {
                self.performSegue(withIdentifier: "goToRenewTA", sender: nil)
            }
        }
    }
}

extension TenancyAgreementsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tenancyAgreements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tenancyAgreementTableViewCell: TenancyAgreementTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TenancyAgreementTableViewCell", for: indexPath) as! TenancyAgreementTableViewCell
        tenancyAgreementTableViewCell.tenancyAgreement = self.tenancyAgreements[(indexPath as NSIndexPath).row]
        tenancyAgreementTableViewCell.delegate = self
        return tenancyAgreementTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showTenancyAgreementDetailSegue", sender: indexPath)
    }
}
