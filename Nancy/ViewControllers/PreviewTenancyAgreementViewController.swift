//
//  PreviewTenancyAgreementViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 27/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class PreviewTenancyAgreementViewController: UIViewController {
    @IBOutlet weak var previewContainerWebView: UIWebView!
    @IBOutlet weak var changeButton: RoundOrangeColoredButton!
    @IBOutlet weak var confirmButton: RoundOrangeColoredButton!
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var isTAPreview: Bool = true
    
    @IBOutlet weak var previewActionButtonsHeight: NSLayoutConstraint!
    @IBOutlet weak var previewActionButtons: UIView!
    
    @IBAction func changeButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.getTenancyAgreementFullDetail(tenancyAgreement.order_id) {
            (networkLoaderError, tenancyAgreement) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            } else {
                let tempTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
                tempTenancyAgreement.isEditTA = true
                NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = tempTenancyAgreement
                self.performSegue(withIdentifier: "goToEditTA", sender: nil)
            }
        }
    }
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var reqURL =  URL(string: tenancyAgreement.previewDocumentPath)
        if isTAPreview {
            previewActionButtons.isHidden = false
            previewActionButtonsHeight.constant = 55
        } else {
            previewActionButtons.isHidden = true
            previewActionButtonsHeight.constant = 0
            reqURL =  URL(string: tenancyAgreement.actualDocumentPath)
        }
        self.view.layoutIfNeeded()
        
        let request = URLRequest(url: reqURL!)
        
        self.previewContainerWebView.loadRequest(request)
        self.previewContainerWebView.scalesPageToFit = true
        self.previewContainerWebView.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.previewContainerWebView.autoresizingMask = UIViewAutoresizing.flexibleHeight
        self.previewContainerWebView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToPaymentViewController" {
            let PaymentVC: PaymentViewController = segue.destination as! PaymentViewController
            PaymentVC.packageId = self.tenancyAgreement.referenceNumber
            PaymentVC.strWebUrl = self.tenancyAgreement.ipayBackendURL
            PaymentVC.strRemark = self.tenancyAgreement.ipayRemark
            PaymentVC.strIpayPrice = "1.00"
            PaymentVC.strDescription = "Generate Tenancy Agreement"
        }
    }
}

extension PreviewTenancyAgreementViewController: UIWebViewDelegate {
    
}
