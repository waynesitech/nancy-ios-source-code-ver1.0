//
//  EsignTenancyAgreementDetailViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 02/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import AlamofireImage

class EsignTenancyAgreementDetailViewController: UITableViewController {
    
    @IBOutlet weak var propertyImageView: UIImageView!
    
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var landlordLabel: UILabel!
    @IBOutlet weak var tenantLabel: UILabel!
    
    @IBOutlet weak var signLabel: UILabel!
    
    @IBOutlet weak var landlord1NameLabel: UILabel!
    @IBOutlet weak var landlord1StatusLabel: UILabel!
    @IBOutlet weak var landlord2NameLabel: UILabel!
    @IBOutlet weak var landlord2StatusLabel: UILabel!
    @IBOutlet weak var landlord3NameLabel: UILabel!
    @IBOutlet weak var landlord3StatusLabel: UILabel!
    @IBOutlet weak var landlord4NameLabel: UILabel!
    @IBOutlet weak var landlord4StatusLabel: UILabel!
    @IBOutlet weak var landlord5NameLabel: UILabel!
    @IBOutlet weak var landlord5StatusLabel: UILabel!
    @IBOutlet weak var landlord6NameLabel: UILabel!
    @IBOutlet weak var landlord6StatusLabel: UILabel!
    @IBOutlet weak var landlord7NameLabel: UILabel!
    @IBOutlet weak var landlord7StatusLabel: UILabel!
    @IBOutlet weak var landlord8NameLabel: UILabel!
    @IBOutlet weak var landlord8StatusLabel: UILabel!
    @IBOutlet weak var landlord9NameLabel: UILabel!
    @IBOutlet weak var landlord9StatusLabel: UILabel!
    @IBOutlet weak var landlord10NameLabel: UILabel!
    @IBOutlet weak var landlord10StatusLabel: UILabel!
    
    @IBOutlet weak var tenant1NameLabel: UILabel!
    @IBOutlet weak var tenant1StatusLabel: UILabel!
    @IBOutlet weak var tenant2NameLabel: UILabel!
    @IBOutlet weak var tenant2StatusLabel: UILabel!
    @IBOutlet weak var tenant3NameLabel: UILabel!
    @IBOutlet weak var tenant3StatusLabel: UILabel!
    @IBOutlet weak var tenant4NameLabel: UILabel!
    @IBOutlet weak var tenant4StatusLabel: UILabel!
    @IBOutlet weak var tenant5NameLabel: UILabel!
    @IBOutlet weak var tenant5StatusLabel: UILabel!
    @IBOutlet weak var tenant6NameLabel: UILabel!
    @IBOutlet weak var tenant6StatusLabel: UILabel!
    @IBOutlet weak var tenant7NameLabel: UILabel!
    @IBOutlet weak var tenant7StatusLabel: UILabel!
    @IBOutlet weak var tenant8NameLabel: UILabel!
    @IBOutlet weak var tenant8StatusLabel: UILabel!
    @IBOutlet weak var tenant9NameLabel: UILabel!
    @IBOutlet weak var tenant9StatusLabel: UILabel!
    @IBOutlet weak var tenant10NameLabel: UILabel!
    @IBOutlet weak var tenant10StatusLabel: UILabel!
    
    var orderUserID: Int = 0
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var orderUserName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateAllLabels()
    }
    
    func updateAllLabels() {
        let placeholderImage = UIImage(named: "noPropertyImagePlaceholder")
        
        let filter = AspectScaledToFillSizeCircleFilter (
            size: self.propertyImageView.frame.size
        )
        
        let url = URL(string: tenancyAgreement.propertyImageURL.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)
        self.propertyImageView.af_setImage(
            withURL: url!,
            placeholderImage: placeholderImage,
            filter: filter
        )
        
        referenceLabel.text = tenancyAgreement.referenceNumber
        propertyLabel.text = "\(tenancyAgreement.propertyStreet),\n\(tenancyAgreement.propertyTown),\n\(tenancyAgreement.propertyPostcode) \(tenancyAgreement.propertyState),\nMalaysia."
        landlordLabel.text = tenancyAgreement.getLandlordNames()
        tenantLabel.text = tenancyAgreement.getTenantNames()
        
        if tenancyAgreement.landlordNames.count > 0 {
            landlord1NameLabel.text = tenancyAgreement.landlordNames[0]
            landlord1StatusLabel.text = tenancyAgreement.landlordStatus[0]
        }
        if tenancyAgreement.landlordNames.count > 1 {
            landlord2NameLabel.text = tenancyAgreement.landlordNames[1]
            landlord2StatusLabel.text = tenancyAgreement.landlordStatus[1]
        }
        if tenancyAgreement.landlordNames.count > 2 {
            landlord3NameLabel.text = tenancyAgreement.landlordNames[2]
            landlord3StatusLabel.text = tenancyAgreement.landlordStatus[2]
        }
        if tenancyAgreement.landlordNames.count > 3 {
            landlord4NameLabel.text = tenancyAgreement.landlordNames[3]
            landlord4StatusLabel.text = tenancyAgreement.landlordStatus[3]
        }
        if tenancyAgreement.landlordNames.count > 4 {
            landlord5NameLabel.text = tenancyAgreement.landlordNames[4]
            landlord5StatusLabel.text = tenancyAgreement.landlordStatus[4]
        }
        if tenancyAgreement.landlordNames.count > 5 {
            landlord6NameLabel.text = tenancyAgreement.landlordNames[5]
            landlord6StatusLabel.text = tenancyAgreement.landlordStatus[5]
        }
        if tenancyAgreement.landlordNames.count > 6 {
            landlord7NameLabel.text = tenancyAgreement.landlordNames[6]
            landlord7StatusLabel.text = tenancyAgreement.landlordStatus[6]
        }
        if tenancyAgreement.landlordNames.count > 7 {
            landlord8NameLabel.text = tenancyAgreement.landlordNames[7]
            landlord8StatusLabel.text = tenancyAgreement.landlordStatus[7]
        }
        if tenancyAgreement.landlordNames.count > 8 {
            landlord9NameLabel.text = tenancyAgreement.landlordNames[8]
            landlord9StatusLabel.text = tenancyAgreement.landlordStatus[8]
        }
        if tenancyAgreement.landlordNames.count > 9 {
            landlord10NameLabel.text = tenancyAgreement.landlordNames[9]
            landlord10StatusLabel.text = tenancyAgreement.landlordStatus[9]
        }
        if tenancyAgreement.tenantNames.count > 0 {
            tenant1NameLabel.text = tenancyAgreement.tenantNames[0]
            tenant1StatusLabel.text = tenancyAgreement.tenantStatus[0]
        }
        if tenancyAgreement.tenantNames.count > 1 {
            tenant2NameLabel.text = tenancyAgreement.tenantNames[1]
            tenant2StatusLabel.text = tenancyAgreement.tenantStatus[1]
        }
        if tenancyAgreement.tenantNames.count > 2 {
            tenant3NameLabel.text = tenancyAgreement.tenantNames[2]
            tenant3StatusLabel.text = tenancyAgreement.tenantStatus[2]
        }
        if tenancyAgreement.tenantNames.count > 3 {
            tenant4NameLabel.text = tenancyAgreement.tenantNames[3]
            tenant4StatusLabel.text = tenancyAgreement.tenantStatus[3]
        }
        if tenancyAgreement.tenantNames.count > 4 {
            tenant5NameLabel.text = tenancyAgreement.tenantNames[4]
            tenant5StatusLabel.text = tenancyAgreement.tenantStatus[4]
        }
        if tenancyAgreement.tenantNames.count > 5 {
            tenant6NameLabel.text = tenancyAgreement.tenantNames[5]
            tenant6StatusLabel.text = tenancyAgreement.tenantStatus[5]
        }
        if tenancyAgreement.tenantNames.count > 6 {
            tenant7NameLabel.text = tenancyAgreement.tenantNames[6]
            tenant7StatusLabel.text = tenancyAgreement.tenantStatus[6]
        }
        if tenancyAgreement.tenantNames.count > 7 {
            tenant8NameLabel.text = tenancyAgreement.tenantNames[7]
            tenant8StatusLabel.text = tenancyAgreement.tenantStatus[7]
        }
        if tenancyAgreement.tenantNames.count > 8 {
            tenant9NameLabel.text = tenancyAgreement.tenantNames[8]
            tenant9StatusLabel.text = tenancyAgreement.tenantStatus[8]
        }
        if tenancyAgreement.tenantNames.count > 9 {
            tenant10NameLabel.text = tenancyAgreement.tenantNames[9]
            tenant10StatusLabel.text = tenancyAgreement.tenantStatus[9]
        }
        
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return 168
        case 1:
            return 50
        case 2:
            return CGFloat(140 + ((tenancyAgreement.landlordNames.count + tenancyAgreement.tenantNames.count) * 19 ))
        case 3:
            return 3
        case 4:
            return 44
        default:
            if indexPath.row < 15 {
                let currentRow = indexPath.row - 4
                if currentRow > tenancyAgreement.landlordNames.count {
                    return 0
                }
            } else if indexPath.row < 25 {
                let currentRow = indexPath.row - 14
                if currentRow > tenancyAgreement.tenantNames.count {
                    return 0
                }
            }
            return 25
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewPDFSegue" {
            let EsignViewTenancyAgreementVC: EsignViewTenancyAgreementViewController = segue.destination as! EsignViewTenancyAgreementViewController
            EsignViewTenancyAgreementVC.tenancyAgreement = self.tenancyAgreement
            EsignViewTenancyAgreementVC.orderUserID = self.orderUserID
            EsignViewTenancyAgreementVC.orderUserName = self.orderUserName
        }
    }
}
