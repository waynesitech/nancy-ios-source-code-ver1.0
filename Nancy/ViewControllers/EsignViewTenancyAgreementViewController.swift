//
//  EsignViewTenancyAgreementViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 02/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class EsignViewTenancyAgreementViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var previewContainerWebView: UIWebView!
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var orderUserID: Int = 0
    var orderUserName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let reqURL =  URL(string: tenancyAgreement.actualDocumentPath)
        let request = URLRequest(url: reqURL!)
        
        self.previewContainerWebView.loadRequest(request)
        self.previewContainerWebView.scalesPageToFit = true
        self.previewContainerWebView.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.previewContainerWebView.autoresizingMask = UIViewAutoresizing.flexibleHeight
        self.previewContainerWebView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToEsignViewController" {
            let EsignVC: EsignViewController = segue.destination as! EsignViewController
            EsignVC.orderUserID = self.orderUserID
            EsignVC.orderUserName = self.orderUserName
        }
    }
}
