//
//  NewTenancySpecialConditionViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 09/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class NewTenancySpecialConditionViewController: UITableViewController {
    
    var specialRequest: [String] = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        self.specialRequest = newTenancyAgreement.specialRequest
        if specialRequest.count == 0 {
            specialRequest.append("")
        }
        if specialRequest[specialRequest.count - 1] != "" {
            specialRequest.append("")
        }
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        self.specialRequest = self.specialRequest.filter{$0 != ""}
        newTenancyAgreement.specialRequest = self.specialRequest
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specialRequest.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let specialConditionTableViewCell: SpecialConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SpecialConditionTableViewCell", for: indexPath) as! SpecialConditionTableViewCell
        specialConditionTableViewCell.delegate = self
        specialConditionTableViewCell.thisIndex = indexPath.row + 1
        
        if indexPath.row < specialRequest.count - 1 {
            specialConditionTableViewCell.isEditable = false
        } else {
            specialConditionTableViewCell.isEditable = true
        }
        specialConditionTableViewCell.thisText = specialRequest[indexPath.row]
        specialConditionTableViewCell.updateUI()
        
        return specialConditionTableViewCell
    }
    
}

extension NewTenancySpecialConditionViewController: AddNewTextProtocol {
    func addNewText(_ newText: String) {
        self.specialRequest.append("")
        self.tableView.reloadData()
    }
    
    func deleteThisText(_ thisIndex: Int) {
        self.specialRequest.remove(at: thisIndex - 1)
        self.tableView.reloadData()
    }
    
    func textChanged(_ newText: String, thisIndex: Int) {
        self.specialRequest[thisIndex - 1] = newText
    }
}
