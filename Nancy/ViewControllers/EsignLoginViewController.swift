//
//  EsignLoginViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 02/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class EsignLoginViewController: UITableViewController {
    
    @IBOutlet weak var box1: UIView!
    @IBOutlet weak var box2: UIView!
    @IBOutlet weak var box3: UIView!
    @IBOutlet weak var box4: UIView!
    @IBOutlet weak var box5: UIView!
    @IBOutlet weak var box6: UIView!
    @IBOutlet weak var box7: UIView!
    @IBOutlet weak var box8: UIView!
    @IBOutlet weak var passcode1: UITextField!
    @IBOutlet weak var passcode2: UITextField!
    @IBOutlet weak var passcode3: UITextField!
    @IBOutlet weak var passcode4: UITextField!
    @IBOutlet weak var ic1: UITextField!
    @IBOutlet weak var ic2: UITextField!
    @IBOutlet weak var ic3: UITextField!
    @IBOutlet weak var ic4: UITextField!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var orderUserID: Int = 0
    var orderUserName: String = ""
    
    @IBAction func verifyButtonPressed(_ sender: Any) {
        errorMessageLabel.text = ""
        let passcode = "\(passcode1.text!)\(passcode2.text!)\(passcode3.text!)\(passcode4.text!)"
        let ic = "\(ic1.text!)\(ic2.text!)\(ic3.text!)\(ic4.text!)"
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.loginWithPasscode(passcode, icNumber: ic) {
            (networkLoaderError, tenancyAgreement, orderUserID, orderUserName) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                self.errorMessageLabel.text="\(networkLoaderError.networkLoaderErrorTitle)\n\(networkLoaderError.networkLoaderErrorBody)"
            }
            if let tenancyAgreement = tenancyAgreement {
                if let orderUserID = orderUserID {
                    if let orderUserName = orderUserName {
                        self.tenancyAgreement = tenancyAgreement
                        self.orderUserID = orderUserID
                        self.orderUserName = orderUserName
                        self.performSegue(withIdentifier: "goToTenancyAgreement", sender: nil)
                    }
                }
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorMessageLabel.text = ""
        box1.layer.cornerRadius = 5
        box1.layer.borderWidth = 1.0
        box1.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box2.layer.cornerRadius = 5
        box2.layer.borderWidth = 1.0
        box2.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box3.layer.cornerRadius = 5
        box3.layer.borderWidth = 1.0
        box3.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box4.layer.cornerRadius = 5
        box4.layer.borderWidth = 1.0
        box4.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box5.layer.cornerRadius = 5
        box5.layer.borderWidth = 1.0
        box5.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box6.layer.cornerRadius = 5
        box6.layer.borderWidth = 1.0
        box6.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box7.layer.cornerRadius = 5
        box7.layer.borderWidth = 1.0
        box7.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        box8.layer.cornerRadius = 5
        box8.layer.borderWidth = 1.0
        box8.layer.borderColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        passcode1.delegate = self
        passcode2.delegate = self
        passcode3.delegate = self
        passcode4.delegate = self
        ic1.delegate = self
        ic2.delegate = self
        ic3.delegate = self
        ic4.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToTenancyAgreement" {
            let EsignTenancyAgreementDetailVC: EsignTenancyAgreementDetailViewController = segue.destination as! EsignTenancyAgreementDetailViewController
            EsignTenancyAgreementDetailVC.tenancyAgreement = self.tenancyAgreement
            EsignTenancyAgreementDetailVC.orderUserID = self.orderUserID
            EsignTenancyAgreementDetailVC.orderUserName = self.orderUserName
        }
    }
}

extension EsignLoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString text: String) -> Bool {
        let str = (textField.text! + text)
        if str.characters.count == 0 {
            return true
        } else if str.characters.count == 1 {
            return true
        } else if str.characters.count == 2 {
            if textField == passcode1 {
                passcode2.text = ""
                passcode2.becomeFirstResponder()
            } else if textField == passcode2 {
                passcode3.text = ""
                passcode3.becomeFirstResponder()
            } else if textField == passcode3 {
                passcode4.text = ""
                passcode4.becomeFirstResponder()
            } else if textField == passcode4 {
                ic1.text = ""
                ic1.becomeFirstResponder()
            } else if textField == ic1 {
                ic2.text = ""
                ic2.becomeFirstResponder()
            } else if textField == ic2 {
                ic3.text = ""
                ic3.becomeFirstResponder()
            } else if textField == ic3 {
                ic4.text = ""
                ic4.becomeFirstResponder()
            } else if textField == ic4 {
                ic4.resignFirstResponder()
                return false
            }
            return true
        }
        return false
    }
}
