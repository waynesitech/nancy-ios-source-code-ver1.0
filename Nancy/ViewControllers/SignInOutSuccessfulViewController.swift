//
//  SignInOutSuccessfulViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 23/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class SignInOutSuccessfulViewController: UIViewController {
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    var fromLoginScreen: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (fromLoginScreen) {
            mainTitleLabel.text = "Welcome Back,"
            subTitleLabel.text = "\(UserData.sharedUserData.name)!"
        } else {
            mainTitleLabel.text = "Congratulations!"
            subTitleLabel.text = "You are now a NanCy's user."
        }
    }
}
