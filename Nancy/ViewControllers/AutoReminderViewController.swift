//
//  AutoReminderViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 14/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class AutoReminderViewController: UIViewController {
    
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EStampPricingViewControllerAgreeAutoReminder" {
            let EStampPricingVC: EStampPricingViewController = segue.destination as! EStampPricingViewController
            EStampPricingVC.tenancyAgreement = self.tenancyAgreement
            EStampPricingVC.acceptAutoReminder = true
        } else if segue.identifier == "EStampPricingViewControllerDeniedAutoReminder" {
            let EStampPricingVC: EStampPricingViewController = segue.destination as! EStampPricingViewController
            EStampPricingVC.tenancyAgreement = self.tenancyAgreement
            EStampPricingVC.acceptAutoReminder = false
        }
    }

}
