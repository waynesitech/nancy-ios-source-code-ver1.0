//
//  ExplanationViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 14/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MBProgressHUD

class ExplanationViewController: UITableViewController {
    
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var language: String = "en"
    var explanationAudios: [ExplanationAudio] = []
    var player: AVAudioPlayer?
    var currentSelectedRow: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkLoader.sharedLoader.getExplanations(self.tenancyAgreement.order_id, language: self.language) {
            (networkLoaderError, explanationAudios) in
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if let explanationAudios = explanationAudios {
                self.explanationAudios = explanationAudios
                self.tableView.reloadData()
            }
        }
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
    }
    
    func downloadFileFromURL(url:URL){
        
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URL, response, error) -> Void in
            self?.play(url: URL!)
        })
        
        downloadTask.resume()
        
    }
    
    func play(url:URL) {        
        do {
            self.player = try AVAudioPlayer(contentsOf: url)
            self.player?.delegate = self
            player?.prepareToPlay()
            player?.volume = 1.0
            player?.play()
            self.hideTheMBProgressHUD()
        } catch let error as NSError {
            print(error.localizedDescription)
            currentSelectedRow = -1
            self.tableView.reloadData()
            self.hideTheMBProgressHUD()
        } catch {
            self.hideTheMBProgressHUD()
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.explanationAudios.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let explanationTableViewCell: ExplanationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ExplanationTableViewCell", for: indexPath) as! ExplanationTableViewCell
        if indexPath.row == currentSelectedRow {
            explanationTableViewCell.isPlaying = true
        } else {
            explanationTableViewCell.isPlaying = false
        }
        explanationTableViewCell.rowNumber = indexPath.row
        explanationTableViewCell.setupContent(explanation: self.explanationAudios[(indexPath as NSIndexPath).row])
        explanationTableViewCell.delegate = self
        return explanationTableViewCell
    }
    
    func hideTheMBProgressHUD() {
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: (self.navigationController?.view)!, animated: true)
        }
    }
}

extension ExplanationViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        currentSelectedRow = -1
        self.tableView.reloadData()
    }
}

extension ExplanationViewController: PlayExplanationProtocol {
    func playAudio(_ audioURL: String, rowNumber: Int) {
        MBProgressHUD.showAdded(to: (self.navigationController?.view)!, animated: true)
        let url = URL(string: audioURL)
        downloadFileFromURL(url: url!)
        currentSelectedRow = rowNumber
        self.tableView.reloadData()
    }
    
    func stopPlaying() {
        self.player?.stop()
        currentSelectedRow = -1
        self.tableView.reloadData()
    }
}
