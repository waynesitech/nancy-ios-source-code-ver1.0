//
//  EStampPricingViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 13/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class EStampPricingViewController: UITableViewController {
    
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var duplicatedCopies: Int = 0
    var stampingFee: Double = 0.00
    var penaltyFee: Double = 0.00
    var stampingDuplicateFee: Double = 0.00
    var serviceFee: Double = 0.00
    var totalFee: Double = 0.00
    var ipayEstampingBackendURL: String = ""
    var ipayRemark: String = ""
    var acceptAutoReminder: Bool = false
    
    @IBOutlet weak var stampingFeeLabel: UILabel!
    @IBOutlet weak var penaltyFeeLabel: UILabel!
    @IBOutlet weak var duplicatesLabel: UILabel!
    @IBOutlet weak var duplicatesFeeLabel: UILabel!
    @IBOutlet weak var preServiceFeeTotalLabel: UILabel!
    @IBOutlet weak var serviceChargeFeeLabel: UILabel!
    @IBOutlet weak var totalFeeLabel: UILabel!
    
    @IBOutlet weak var stepper: UIStepper!
    @IBAction func stepperValueChanged(_ sender: Any) {
        if stepper.value == 0 {
            duplicatesLabel.text = "Duplicates: 0 copy"
        } else if stepper.value == 1 {
            duplicatesLabel.text = "Duplicates: 1 copy"
        } else {
            duplicatesLabel.text = "Duplicates: \(String(Int(stepper.value))) copies"
        }
        self.updateAllValues()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkLoader.sharedLoader.getEstampPrice(self.tenancyAgreement.order_id) {
            (networkLoaderError, jsonResult) in
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if let jsonResult = jsonResult {
                if let ipayEstampingBackendURL = jsonResult["ipayEstampingBackendURL"] as? String {
                    self.ipayEstampingBackendURL = ipayEstampingBackendURL
                }
                if let ipayRemark = jsonResult["ipayRemark"] as? String {
                    self.ipayRemark = ipayRemark
                }
                if let jsonPricing = jsonResult["pricing"] as? [String:Any] {
                    if let stampingFee = jsonPricing["stampingFee"] as? Double {
                        self.stampingFee = stampingFee
                    }
                    if let penaltyFee = jsonPricing["penaltyFee"] as? Double {
                        self.penaltyFee = penaltyFee
                    }
                    if let stampingDuplicateFee = jsonPricing["stampingDuplicateFee"] as? Double {
                        self.stampingDuplicateFee = stampingDuplicateFee
                    }
                    if let serviceFee = jsonPricing["serviceFee"] as? Double {
                        self.serviceFee = serviceFee
                    }
                }
            }
            self.updateAllValues()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToEstampPaymentViewController" {
            let PaymentVC: PaymentViewController = segue.destination as! PaymentViewController
            PaymentVC.packageId = self.tenancyAgreement.referenceNumber
            PaymentVC.strWebUrl = self.ipayEstampingBackendURL + String(Int(stepper.value))
            PaymentVC.strRemark = self.ipayRemark
            PaymentVC.strIpayPrice = roundToTwoDecimal(x: self.totalFee)
            PaymentVC.strDescription = "Estamping Fee"
        }
    }
    
    func updateAllValues() {
        self.stampingFeeLabel.text = "RM \(roundToTwoDecimal(x: self.stampingFee))"
        self.penaltyFeeLabel.text = "RM \(roundToTwoDecimal(x: self.penaltyFee))"
        self.duplicatesFeeLabel.text = "RM \(roundToTwoDecimal(x: (self.stampingDuplicateFee * self.stepper.value)))"
        self.preServiceFeeTotalLabel.text = "RM \(roundToTwoDecimal(x: self.stampingFee + self.penaltyFee + (self.stampingDuplicateFee * self.stepper.value)))"
        self.serviceChargeFeeLabel.text = "RM \(roundToTwoDecimal(x: self.serviceFee))"
        self.totalFee = self.stampingFee + self.penaltyFee + (self.stampingDuplicateFee * self.stepper.value) + self.serviceFee
        self.totalFeeLabel.text = "RM \(roundToTwoDecimal(x: self.totalFee))"
    }
    
    func roundToTwoDecimal(x: Double) -> String {
        return String(format: "%.2f", x)
    }
}
