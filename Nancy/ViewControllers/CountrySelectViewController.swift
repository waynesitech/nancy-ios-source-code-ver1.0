//
//  CountrySelectViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 27/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

protocol didSelectCountryProtocol : NSObjectProtocol {
    func didSelectCountry(selectedCountry: Country) -> Void
}

class CountrySelectViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var countries: [Country]!
    var country: Country?
    var countryIndex: Int?
    var navTitle: String?
    var didSelectCountryProtocolDelegate: didSelectCountryProtocol?
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        countries = countryNamesByCode()
        if let navigationTitle = navTitle {
            self.title = navigationTitle
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.scrollToRow(at: IndexPath(item: countryIndex!, section: 0), at: .top, animated: true)
    }
    
    func countryNamesByCode() -> [Country] {
        var countries = [Country]()
        
        for code in NSLocale.isoCountryCodes {
            let countryName = NSLocale.current.localizedString(forRegionCode: code)!
            
            let phoneNumberUtil = NBPhoneNumberUtil.sharedInstance()!
            if let countryCode = phoneNumberUtil.getCountryCode(forRegion: code) {
                let phoneCode: String = "+\(countryCode.intValue)"
                if phoneCode != "+0" {
                    let country = Country(code: code, name: countryName, phoneCode: phoneCode)
                    countries.append(country)
                }
            }
            
        }
        
        countries = countries.sorted(by: { $0.name < $1.name })
        
        for index in 0...countries.count - 1 {
            if countries[index].name == country!.name {
                countryIndex = index
            }
        }
        
        return countries
    }
    
}

extension CountrySelectViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let countryTableViewCell : CountryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell", for: indexPath) as! CountryTableViewCell
        countryTableViewCell.setupPhoneCode(country: countries[indexPath.row])
        
        return countryTableViewCell
    }
}

extension CountrySelectViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Call delegate to pass value back
        if didSelectCountryProtocolDelegate?.didSelectCountry != nil {
            didSelectCountryProtocolDelegate?.didSelectCountry(selectedCountry: countries[indexPath.row])
        }
        self.navigationController?.popViewController(animated: true)
    }
}
