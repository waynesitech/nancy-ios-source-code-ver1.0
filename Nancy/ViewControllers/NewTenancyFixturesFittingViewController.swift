//
//  NewTenancyFixturesFittingViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 10/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class NewTenancyFixturesFittingViewController: UITableViewController {
    
    var fixturesFitting: [String] = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        self.fixturesFitting = newTenancyAgreement.fixturesFitting
        if fixturesFitting.count == 0 {
            fixturesFitting.append("")
        }
        if fixturesFitting[fixturesFitting.count - 1] != "" {
            fixturesFitting.append("")
        }
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        self.fixturesFitting = self.fixturesFitting.filter{$0 != ""}
        newTenancyAgreement.fixturesFitting = self.fixturesFitting
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fixturesFitting.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let specialConditionTableViewCell: SpecialConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SpecialConditionTableViewCell", for: indexPath) as! SpecialConditionTableViewCell
        specialConditionTableViewCell.delegate = self
        specialConditionTableViewCell.thisIndex = indexPath.row + 1
        
        if indexPath.row < fixturesFitting.count - 1 {
            specialConditionTableViewCell.isEditable = false
        } else {
            specialConditionTableViewCell.isEditable = true
        }
        specialConditionTableViewCell.thisText = fixturesFitting[indexPath.row]
        specialConditionTableViewCell.updateUI()
        
        return specialConditionTableViewCell
    }
    
}

extension NewTenancyFixturesFittingViewController: AddNewTextProtocol {
    func addNewText(_ newText: String) {
        self.fixturesFitting.append("")
        self.tableView.reloadData()
    }
    
    func deleteThisText(_ thisIndex: Int) {
        self.fixturesFitting.remove(at: thisIndex - 1)
        self.tableView.reloadData()
    }
    
    func textChanged(_ newText: String, thisIndex: Int) {
        self.fixturesFitting[thisIndex - 1] = newText
    }
}
