//
//  NewTenancySelectPartyViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class NewTenancySelectPartyViewController: UIViewController {
    @IBAction func isLandlordButtonPressed(_ sender: Any) {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        newTenancyAgreement.isLandlord = true
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 2
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = 0
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = 0
        newTenancyAgreement.currentStep = 2
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        self.performSegue(withIdentifier: "proceedFromSelectParty", sender: nil)
    }
    @IBAction func isTenantButtonPressed(_ sender: Any) {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        newTenancyAgreement.isLandlord = false
        newTenancyAgreement.currentStep = 2
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 2
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = 0
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = 0
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        self.performSegue(withIdentifier: "proceedFromSelectParty", sender: nil)
    }
    
}
