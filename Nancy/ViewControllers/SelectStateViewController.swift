//
//  SelectStateViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 07/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

protocol didSelectStateProtocol : NSObjectProtocol {
    func didSelectState(selectedState: String) -> Void
}

class SelectStateViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var states: [String] = ["Kedah", "Kelantan", "Kuala Lumpur", "Labuan", "Malacca", "Negeri Sembilan", "Pahang", "Penang", "Perak", "Perlis", "Putrajaya", "Sabah", "Sarawak", "Selangor", "Terengganu", "Johor"]
    var state: String?
    var stateIndex: Int = 0
    var didSelectStateProtocolDelegate: didSelectStateProtocol?

    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for i in 0..<states.count {
            if self.state == states[i] {
                self.stateIndex = i
            }
        }
        tableView.scrollToRow(at: IndexPath(item: stateIndex, section: 0), at: .top, animated: true)
    }

}

extension SelectStateViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return states.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        tableViewCell.textLabel?.text = states[indexPath.row]
        
        return tableViewCell
    }
}

extension SelectStateViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Call delegate to pass value back
        if didSelectStateProtocolDelegate?.didSelectState != nil {
            didSelectStateProtocolDelegate?.didSelectState(selectedState: states[indexPath.row])
        }
        self.navigationController?.popViewController(animated: true)
    }
}
