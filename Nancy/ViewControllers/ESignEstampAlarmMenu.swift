//
//  ESignEstampAlarmMenu.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 27/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MobileCoreServices

class ESignEstampAlarmMenu: UIViewController {
    @IBOutlet weak var chooseOneLabel: UILabel!
    @IBOutlet weak var existingTenancyAgreementOptionButton: RoundOrangeBorderedButton!
    
    var menuType: String = ""
    
    @IBAction func uploadTenancyAgreementButtonPressed(_ sender: Any) {
        if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.currentStep > -1 {
            let alert = UIAlertController(title: "", message: "You have an unsubmitted Tenancy Agreement. Would you like to continue editing?", preferredStyle: .alert)
            let dismissAction: UIAlertAction = UIAlertAction(title: "Discard", style: .default, handler: {_ in
                self.selectTenancyAgreement()
                NewTenancyAgreementData.sharedNewTenancyAgreementData.discardTenancyAgreement()
            })
            alert.addAction(dismissAction)
            let alertAction: UIAlertAction = UIAlertAction(title: "Continue Editing", style: .default, handler: {_ in
                self.performSegue(withIdentifier: "goToPropertyDetail", sender: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.selectTenancyAgreement()
        }
    }
    
    func selectTenancyAgreement() {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = menuType
        if menuType == "Auto Reminder" {
            chooseOneLabel.isHidden = true
            existingTenancyAgreementOptionButton.isHidden = true
        }
    }
    
}

extension ESignEstampAlarmMenu: UIDocumentMenuDelegate, UIDocumentPickerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let cico = url as URL
        do {
           try NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF = Data(contentsOf: cico)
        } catch {
        
        }
        
        if menuType == "E-Sign" {
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.uploadType = "E_SIGN"
        } else if menuType == "E-Stamp" {
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.uploadType = "E_STAMP"
        } else {
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.uploadType = "AUTO_REMINDER"
        }
        self.performSegue(withIdentifier: "goToPropertyDetail", sender: nil)
    }
    
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
}
