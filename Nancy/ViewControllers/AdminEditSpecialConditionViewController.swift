//
//  AdminEditSpecialConditionViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 21/08/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class AdminEditSpecialConditionViewController: UITableViewController {
    
    var specialRequest: [[String: String]] = []
    var orderID: String = ""
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.updateSpecialRequest(self.orderID, specialRequest: self.specialRequest) {
            (networkLoaderError, status) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                let alertController = UIAlertController(
                    title: "Failed",
                    message: networkLoaderError.networkLoaderErrorBody,
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alertController.addAction( UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.default,
                    handler: nil)
                )
                self.present(alertController, animated: true, completion: nil)
            }
            if status != nil {
                let alertController = UIAlertController(
                    title: "Success",
                    message: "Special Request Updated.",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alertController.addAction( UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.default,
                    handler: nil)
                )
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 88.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return specialRequest.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let adminSpecialConditionTableViewCell: AdminSpecialConditionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AdminSpecialConditionTableViewCell", for: indexPath) as! AdminSpecialConditionTableViewCell
        adminSpecialConditionTableViewCell.delegate = self
        adminSpecialConditionTableViewCell.thisIndex = indexPath.row + 1
        
        adminSpecialConditionTableViewCell.thisText = specialRequest[indexPath.row]["original"]!
        adminSpecialConditionTableViewCell.thisEditText = specialRequest[indexPath.row]["edited"]!
        adminSpecialConditionTableViewCell.updateUI()
        
        return adminSpecialConditionTableViewCell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension AdminEditSpecialConditionViewController: AdminSpecialConditionTextViewProtocol {
    func textChanged(_ newText: String, thisIndex: Int, needsReloadCell: Bool) {
        self.specialRequest[thisIndex - 1]["edited"] = newText
        if needsReloadCell {
            UIView.setAnimationsEnabled(false)
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            
//            let scrollTo = self.tableView.cellForRow(at: IndexPath(row: thisIndex, section: 0))?.frame.origin.y
//            self.tableView.setContentOffset(CGPoint(0, scrollTo!), animated: false)
            
            UIView.setAnimationsEnabled(true)
        }
    }
}

