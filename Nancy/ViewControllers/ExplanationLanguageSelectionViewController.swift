//
//  ExplanationLanguageSelectionViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 14/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class ExplanationLanguageSelectionViewController: UIViewController {
    
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "explainInEnglish" {
            let ExplanationVC: ExplanationViewController = segue.destination as! ExplanationViewController
            ExplanationVC.tenancyAgreement = self.tenancyAgreement
            ExplanationVC.language = "en"
        } else if segue.identifier == "explainInMandarin" {
            let ExplanationVC: ExplanationViewController = segue.destination as! ExplanationViewController
            ExplanationVC.tenancyAgreement = self.tenancyAgreement
            ExplanationVC.language = "cn"
        }
    }
    
}
