//
//  ForgotPasswordViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 11/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var contentScroll : UIScrollView!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var emailErrorIcon: UIImageView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    
    @IBAction func forgotPasswordPressed(_ sender: AnyObject) {
        let email = emailTextField.text!
        
        emailErrorIcon.isHidden = true
        errorMessageLabel.text = ""
        
        if !isValidEmail(testStr: email) {
            errorMessageLabel.text = "Email is not valid."
            emailErrorIcon.isHidden = false
            emailTextField.becomeFirstResponder()
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.resetPassword(email) {
            networkLoaderError, resetSuccessfully in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                // Handle Error Message
                self.errorMessageLabel.text = "\(networkLoaderError.networkLoaderErrorTitle)\n\(networkLoaderError.networkLoaderErrorBody)"
            } else if let _ = resetSuccessfully {
                let alertVC = UIAlertController(
                    title: "Check Your Email",
                    message: "NanCy has sent a reset password link to your email address, \(email).",
                    preferredStyle: .alert)
                let dismissAction = UIAlertAction(
                    title: "Ok, Thanks",
                    style: .default,
                    handler: nil)
                alertVC.addAction(dismissAction)
                self.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(SignInViewController.willShowKeyBoard(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignInViewController.willHideKeyBoard(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        emailErrorIcon.isHidden = true
        errorMessageLabel.text = ""
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func willShowKeyBoard(_ notification : Notification){
        self.contentScroll.contentInset.bottom = 184
    }
    
    func willHideKeyBoard(_ notification : Notification){
        self.contentScroll.contentInset.bottom = 0
    }
    
    func textFieldShouldReturn (_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.contentScroll.contentInset.bottom = 0
        return true
    }
    
}
