//
//  NewTenancyPaymentDetailsViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 09/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import DLRadioButton

class NewTenancyPaymentDetailsViewController: UITableViewController {
    
    var currentlySelectedPaymentMethod: Int = -1
    var currentlySelectedNotificationMethod: Int = -1
    var currentlySelectedPostDatedMethod: Int = -1
    @IBOutlet weak var accountHolderNameTextField: UITextField!
    @IBOutlet weak var bankNameTextField: UITextField!
    @IBOutlet weak var bankAccountNameTextField: UITextField!
    
    @IBOutlet weak var directBankInRadioButton: DLRadioButton!
    @IBOutlet weak var postDatedChequeRadioButton: DLRadioButton!
    @IBOutlet weak var collectionByLandlordRadioButton: DLRadioButton!
    
    @IBOutlet weak var emailRadioButton: DLRadioButton!
    @IBOutlet weak var smsRadioButton: DLRadioButton!
    @IBOutlet weak var whatsappRadioButton: DLRadioButton!
    
    @IBOutlet weak var sixMonthRadioButton: DLRadioButton!
    @IBOutlet weak var twelveRadioButton: DLRadioButton!
    @IBOutlet weak var twentyFourRadioButton: DLRadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        bankNameTextField.text = newTenancyAgreement.bank
        bankAccountNameTextField.text = newTenancyAgreement.bankAccountNum
        accountHolderNameTextField.text = newTenancyAgreement.paymentAccountName
        
        currentlySelectedPaymentMethod = newTenancyAgreement.paymentMethod
        currentlySelectedPostDatedMethod = newTenancyAgreement.postDatedMethod
        currentlySelectedNotificationMethod = newTenancyAgreement.notificationMethod
        
        directBankInRadioButton.isSelected = false
        postDatedChequeRadioButton.isSelected = false
        collectionByLandlordRadioButton.isSelected = false
        emailRadioButton.isSelected = false
        smsRadioButton.isSelected = false
        whatsappRadioButton.isSelected = false
        sixMonthRadioButton.isSelected = false
        twelveRadioButton.isSelected = false
        twentyFourRadioButton.isSelected = false
        
        switch currentlySelectedPaymentMethod {
        case 1:
            postDatedChequeRadioButton.isSelected = true
        case 2:
            collectionByLandlordRadioButton.isSelected = true
        default:
            directBankInRadioButton.isSelected = true
        }
        
        switch currentlySelectedNotificationMethod {
        case 1:
            smsRadioButton.isSelected = true
        case 2:
            whatsappRadioButton.isSelected = true
        default:
            emailRadioButton.isSelected = true
        }
        
        switch currentlySelectedPostDatedMethod {
        case 1:
            twelveRadioButton.isSelected = true
        case 2:
            twentyFourRadioButton.isSelected = true
        default:
            sixMonthRadioButton.isSelected = true
        }
        
        directBankInRadioButton.setNeedsDisplay()
        postDatedChequeRadioButton.setNeedsDisplay()
        collectionByLandlordRadioButton.setNeedsDisplay()
        emailRadioButton.setNeedsDisplay()
        smsRadioButton.setNeedsDisplay()
        whatsappRadioButton.setNeedsDisplay()
        sixMonthRadioButton.setNeedsDisplay()
        twelveRadioButton.setNeedsDisplay()
        twentyFourRadioButton.setNeedsDisplay()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
         newTenancyAgreement.bank = bankNameTextField.text!
         newTenancyAgreement.bankAccountNum = bankAccountNameTextField.text!
         newTenancyAgreement.paymentAccountName = accountHolderNameTextField.text!
        
         newTenancyAgreement.paymentMethod = currentlySelectedPaymentMethod
         newTenancyAgreement.postDatedMethod = currentlySelectedPostDatedMethod
         newTenancyAgreement.notificationMethod = currentlySelectedNotificationMethod
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 109.0
        } else if indexPath.row == 2 && currentlySelectedPaymentMethod == 0 {
            return 280.0
        } else if indexPath.row == 4  && currentlySelectedPaymentMethod == 1 {
            return 80.0
        } else if indexPath.row == 2  || indexPath.row == 4 {
            return 0.0
        }
        
        return UITableViewAutomaticDimension
    }
    
    @IBAction func selectedButtonChanged(_ sender: DLRadioButton) {
        switch sender {
        case directBankInRadioButton:
            currentlySelectedPaymentMethod = 0
            tableView.reloadData()
        case postDatedChequeRadioButton:
            currentlySelectedPaymentMethod = 1
            tableView.reloadData()
        case collectionByLandlordRadioButton:
            currentlySelectedPaymentMethod = 2
            tableView.reloadData()
        case emailRadioButton:
            currentlySelectedNotificationMethod = 0
        case smsRadioButton:
            currentlySelectedNotificationMethod = 1
        case whatsappRadioButton:
            currentlySelectedNotificationMethod = 2
        case sixMonthRadioButton:
            currentlySelectedPostDatedMethod = 0
        case twelveRadioButton:
            currentlySelectedPostDatedMethod = 1
        case twentyFourRadioButton:
            currentlySelectedPostDatedMethod = 2
        default:
            break
        }
    }
    
}
