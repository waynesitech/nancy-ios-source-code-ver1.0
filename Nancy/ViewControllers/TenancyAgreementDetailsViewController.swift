//
//  TenancyAgreementDetailsViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 16/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import AlamofireImage
import MBProgressHUD
import Alamofire

class TenancyAgreementDetailsViewController: UITableViewController {
    
    @IBOutlet weak var propertyImageView: UIImageView!
    @IBOutlet weak var viewTAButton: RoundOrangeBorderedButton!
    @IBOutlet weak var downloadTAButton: RoundOrangeBorderedButton!
    @IBOutlet weak var explainTAButton: RoundOrangeBorderedButton!
    @IBOutlet weak var resendTAButton: RoundOrangeBorderedButton!
    @IBOutlet weak var previewTAButton: RoundOrangeBorderedButton!
    @IBOutlet weak var previewExplanationTAButton: RoundOrangeBorderedButton!
    
    @IBOutlet weak var specialConditionButton: RoundOrangeBorderedButton!
    @IBOutlet weak var approveButton: RoundOrangeBorderedButton!
    @IBOutlet weak var rejectButton: RoundOrangeBorderedButton!
    
    @IBOutlet weak var referenceLabel: UILabel!
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var landlordLabel: UILabel!
    @IBOutlet weak var tenantLabel: UILabel!
    @IBOutlet weak var taStatusLabelConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var taStatusLabel: UILabel!
    @IBOutlet weak var signStatusLabel: UILabel!
    @IBOutlet weak var stampStatusLabel: UILabel!
    @IBOutlet weak var signLabel: UILabel!
    
    @IBOutlet weak var renewButton: RoundEdgeOrangeColoredButton!
    @IBOutlet weak var sendOTPButton: RoundEdgeOrangeColoredButton!
    @IBOutlet weak var submitEStampButton: RoundEdgeOrangeColoredButton!
    
    @IBOutlet weak var landlord1NameLabel: UILabel!
    @IBOutlet weak var landlord1StatusLabel: UILabel!
    @IBOutlet weak var landlord2NameLabel: UILabel!
    @IBOutlet weak var landlord2StatusLabel: UILabel!
    @IBOutlet weak var landlord3NameLabel: UILabel!
    @IBOutlet weak var landlord3StatusLabel: UILabel!
    @IBOutlet weak var landlord4NameLabel: UILabel!
    @IBOutlet weak var landlord4StatusLabel: UILabel!
    @IBOutlet weak var landlord5NameLabel: UILabel!
    @IBOutlet weak var landlord5StatusLabel: UILabel!
    @IBOutlet weak var landlord6NameLabel: UILabel!
    @IBOutlet weak var landlord6StatusLabel: UILabel!
    @IBOutlet weak var landlord7NameLabel: UILabel!
    @IBOutlet weak var landlord7StatusLabel: UILabel!
    @IBOutlet weak var landlord8NameLabel: UILabel!
    @IBOutlet weak var landlord8StatusLabel: UILabel!
    @IBOutlet weak var landlord9NameLabel: UILabel!
    @IBOutlet weak var landlord9StatusLabel: UILabel!
    @IBOutlet weak var landlord10NameLabel: UILabel!
    @IBOutlet weak var landlord10StatusLabel: UILabel!
    
    @IBOutlet weak var tenant1NameLabel: UILabel!
    @IBOutlet weak var tenant1StatusLabel: UILabel!
    @IBOutlet weak var tenant2NameLabel: UILabel!
    @IBOutlet weak var tenant2StatusLabel: UILabel!
    @IBOutlet weak var tenant3NameLabel: UILabel!
    @IBOutlet weak var tenant3StatusLabel: UILabel!
    @IBOutlet weak var tenant4NameLabel: UILabel!
    @IBOutlet weak var tenant4StatusLabel: UILabel!
    @IBOutlet weak var tenant5NameLabel: UILabel!
    @IBOutlet weak var tenant5StatusLabel: UILabel!
    @IBOutlet weak var tenant6NameLabel: UILabel!
    @IBOutlet weak var tenant6StatusLabel: UILabel!
    @IBOutlet weak var tenant7NameLabel: UILabel!
    @IBOutlet weak var tenant7StatusLabel: UILabel!
    @IBOutlet weak var tenant8NameLabel: UILabel!
    @IBOutlet weak var tenant8StatusLabel: UILabel!
    @IBOutlet weak var tenant9NameLabel: UILabel!
    @IBOutlet weak var tenant9StatusLabel: UILabel!
    @IBOutlet weak var tenant10NameLabel: UILabel!
    @IBOutlet weak var tenant10StatusLabel: UILabel!
    
    var orderID: String = ""
    var shouldHideEsign: Bool = false
    var tenancyAgreement: TenancyAgreement = TenancyAgreement()
    var docController: UIDocumentInteractionController?
    
    @IBAction func previewExplanationButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "toExplanationLanguageSelectVC", sender: nil)
    }
    
    @IBAction func approveButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.approveOrder(self.orderID) {
            (networkLoaderError, status) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if status != nil {
                let alertController = UIAlertController(
                    title: "Success",
                    message: "Tenancy Agreement Approve.",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alertController.addAction( UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.default,
                    handler: nil)
                )
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }

    @IBAction func rejectButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.rejectOrder(self.orderID) {
            (networkLoaderError, status) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if status != nil {
                let alertController = UIAlertController(
                    title: "Success",
                    message: "Tenancy Agreement Rejected.",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alertController.addAction( UIAlertAction(
                    title: "OK",
                    style: UIAlertActionStyle.default,
                    handler: nil)
                )
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func sendTAviaEmailButtonPressed(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.sendTAviaEmail(self.orderID) {
            (networkLoaderError, resultString) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if let resultString = resultString {
                let alert = UIAlertController(title: "", message: resultString, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok, Thanks!", style: .default, handler: {_ in self.viewDidLoad()}))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func sendOTPButtonPressed(_ sender: Any) {
        if tenancyAgreement.eStampStatus == "-" && tenancyAgreement.eSignStatus != "Done" {
            let alert = UIAlertController(title: "", message: "Proceed to E Sign?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "NO", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: {_ in self.eSign()}))
            self.present(alert, animated: true, completion: nil)
        } else {
            eSign()
        }
    }
    
    @IBAction func downloadTenancyAgreementPressed(_ sender: Any) {
        
        let fileURL = URL(string: tenancyAgreement.actualDocumentPath)
        var localPath: URL?
        
        Alamofire.download(fileURL!, method: .get, parameters: nil, headers: nil) {
            (tempUrl, response)  in
            
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let pathComponent = "\(self.tenancyAgreement.referenceNumber).pdf"
            
            localPath = directoryURL.appendingPathComponent(pathComponent) as URL?
            return (destinationURL: localPath!, options: .removePreviousFile)
            
            
            }.response { response in
                if localPath != nil{
                    self.docController = UIDocumentInteractionController(url: localPath! as URL)
                    self.docController?.presentOptionsMenu(from: CGRect.zero, in: self.view, animated: true)
                }
        }
    }
    
    func eSign() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.sendOTP(self.orderID) {
            (networkLoaderError, resultString) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if let resultString = resultString {
                let alert = UIAlertController(title: "", message: resultString, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok, Thanks!", style: .default, handler: {_ in self.viewDidLoad()}))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadTAButton.isHidden = true
        explainTAButton.isHidden = true
        resendTAButton.isHidden = true
        viewTAButton.isHidden = true
        renewButton.isHidden = true
        previewTAButton.isHidden = true
        previewExplanationTAButton.isHidden = true
        specialConditionButton.isHidden = true
        approveButton.isHidden = true
        rejectButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.getTenancyAgreementDetails(self.orderID) {
            (networkLoaderError, tenancyAgreement) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                print(networkLoaderError.networkLoaderErrorTitle)
                print(networkLoaderError.networkLoaderErrorBody)
            }
            if let tenancyAgreement = tenancyAgreement {
                self.tenancyAgreement = tenancyAgreement
                self.updateAllLabels()
            }
        }
    }
    
    func updateAllLabels() {
        let placeholderImage = UIImage(named: "noPropertyImagePlaceholder")
        
        let filter = AspectScaledToFillSizeCircleFilter (
            size: self.propertyImageView.frame.size
        )
        
        let url = URL(string: tenancyAgreement.propertyImageURL)
        self.propertyImageView.af_setImage(
            withURL: url!,
            placeholderImage: placeholderImage,
            filter: filter
        )
        
        referenceLabel.text = tenancyAgreement.referenceNumber
        propertyLabel.text = "\(tenancyAgreement.propertyStreet),\n\(tenancyAgreement.propertyTown),\n\(tenancyAgreement.propertyPostcode) \(tenancyAgreement.propertyState),\nMalaysia."
        landlordLabel.text = tenancyAgreement.getLandlordNames()
        tenantLabel.text = tenancyAgreement.getTenantNames()
        
        taStatusLabelConstraint.constant = 35
        if tenancyAgreement.tenancyAgreementStatus == "Received" && UserData.sharedUserData.role == 2 {
            if tenancyAgreement.specialRequests.count == 0 {
                specialConditionButton.isHidden = true
            } else {
                specialConditionButton.isHidden = false
            }
            approveButton.isHidden = false
            rejectButton.isHidden = false
            downloadTAButton.isHidden = true
            explainTAButton.isHidden = true
            resendTAButton.isHidden = true
            viewTAButton.isHidden = true
            renewButton.isHidden = true
            previewTAButton.isHidden = true
            previewExplanationTAButton.isHidden = true
            taStatusLabel.text = "RENDERING"
            taStatusLabelConstraint.constant = 85
        } else if tenancyAgreement.tenancyAgreementStatus == "Received" {
            specialConditionButton.isHidden = true
            approveButton.isHidden = true
            rejectButton.isHidden = true
            downloadTAButton.isHidden = true
            explainTAButton.isHidden = true
            resendTAButton.isHidden = true
            viewTAButton.isHidden = true
            renewButton.isHidden = true
            previewTAButton.isHidden = true
            previewExplanationTAButton.isHidden = true
            taStatusLabel.text = "RENDERING"
            taStatusLabelConstraint.constant = 85
        } else if tenancyAgreement.tenancyAgreementStatus == "Ready" {
            specialConditionButton.isHidden = true
            approveButton.isHidden = true
            rejectButton.isHidden = true
            downloadTAButton.isHidden = true
            explainTAButton.isHidden = true
            resendTAButton.isHidden = true
            viewTAButton.isHidden = true
            renewButton.isHidden = true
            previewTAButton.isHidden = false
            previewExplanationTAButton.isHidden = false
            taStatusLabel.text = "READY"
        } else {
            if tenancyAgreement.tenancyAgreementStatus == "Expired"  {
                taStatusLabel.text = "EXPIRED"
            } else {
                taStatusLabel.text = "DONE"
            }
            specialConditionButton.isHidden = true
            approveButton.isHidden = true
            rejectButton.isHidden = true
            viewTAButton.isHidden = false
            downloadTAButton.isHidden = false
            explainTAButton.isHidden = false
            resendTAButton.isHidden = false
            renewButton.isHidden = false
            previewTAButton.isHidden = true
            previewExplanationTAButton.isHidden = true
        }
        
        shouldHideEsign = false
        if tenancyAgreement.eSignStatus == "MANUALLY_SIGNED" {
            signLabel.text = "Manual Sign"
            signStatusLabel.text = "Done"
            signStatusLabel.isHidden = false
            sendOTPButton.isHidden = true
        } else if tenancyAgreement.eSignStatus == "-" &&
            tenancyAgreement.tenancyAgreementStatus == "Done" {
            signLabel.text = "E Sign"
            signStatusLabel.isHidden = true
            sendOTPButton.isHidden = false
            sendOTPButton.setTitle("SEND OTP", for: .normal)
        } else if tenancyAgreement.eSignStatus == "PARTIAL_SIGNED" {
            signLabel.text = "E Sign"
            signStatusLabel.isHidden = true
            sendOTPButton.isHidden = false
            sendOTPButton.setTitle("RESEND OTP", for: .normal)
        } else if tenancyAgreement.eSignStatus == "ALL_SIGNED" {
            signLabel.text = "E Sign"
            signStatusLabel.isHidden = false
            signStatusLabel.text = "Done"
            sendOTPButton.isHidden = true
        } else {
            shouldHideEsign = true
        }
        
        if tenancyAgreement.eStampStatus == "-" {
            submitEStampButton.isHidden = false
            stampStatusLabel.isHidden = true
            stampStatusLabel.text = ""
        } else {
            submitEStampButton.isHidden = true
            stampStatusLabel.isHidden = false
            stampStatusLabel.text = tenancyAgreement.eStampStatus
        }
        
        if tenancyAgreement.landlordNames.count > 0 {
            landlord1NameLabel.text = tenancyAgreement.landlordNames[0]
            landlord1StatusLabel.text = tenancyAgreement.landlordStatus[0]
        }
        if tenancyAgreement.landlordNames.count > 1 {
            landlord2NameLabel.text = tenancyAgreement.landlordNames[1]
            landlord2StatusLabel.text = tenancyAgreement.landlordStatus[1]
        }
        if tenancyAgreement.landlordNames.count > 2 {
            landlord3NameLabel.text = tenancyAgreement.landlordNames[2]
            landlord3StatusLabel.text = tenancyAgreement.landlordStatus[2]
        }
        if tenancyAgreement.landlordNames.count > 3 {
            landlord4NameLabel.text = tenancyAgreement.landlordNames[3]
            landlord4StatusLabel.text = tenancyAgreement.landlordStatus[3]
        }
        if tenancyAgreement.landlordNames.count > 4 {
            landlord5NameLabel.text = tenancyAgreement.landlordNames[4]
            landlord5StatusLabel.text = tenancyAgreement.landlordStatus[4]
        }
        if tenancyAgreement.landlordNames.count > 5 {
            landlord6NameLabel.text = tenancyAgreement.landlordNames[5]
            landlord6StatusLabel.text = tenancyAgreement.landlordStatus[5]
        }
        if tenancyAgreement.landlordNames.count > 6 {
            landlord7NameLabel.text = tenancyAgreement.landlordNames[6]
            landlord7StatusLabel.text = tenancyAgreement.landlordStatus[6]
        }
        if tenancyAgreement.landlordNames.count > 7 {
            landlord8NameLabel.text = tenancyAgreement.landlordNames[7]
            landlord8StatusLabel.text = tenancyAgreement.landlordStatus[7]
        }
        if tenancyAgreement.landlordNames.count > 8 {
            landlord9NameLabel.text = tenancyAgreement.landlordNames[8]
            landlord9StatusLabel.text = tenancyAgreement.landlordStatus[8]
        }
        if tenancyAgreement.landlordNames.count > 9 {
            landlord10NameLabel.text = tenancyAgreement.landlordNames[9]
            landlord10StatusLabel.text = tenancyAgreement.landlordStatus[9]
        }
        if tenancyAgreement.tenantNames.count > 0 {
            tenant1NameLabel.text = tenancyAgreement.tenantNames[0]
            tenant1StatusLabel.text = tenancyAgreement.tenantStatus[0]
        }
        if tenancyAgreement.tenantNames.count > 1 {
            tenant2NameLabel.text = tenancyAgreement.tenantNames[1]
            tenant2StatusLabel.text = tenancyAgreement.tenantStatus[1]
        }
        if tenancyAgreement.tenantNames.count > 2 {
            tenant3NameLabel.text = tenancyAgreement.tenantNames[2]
            tenant3StatusLabel.text = tenancyAgreement.tenantStatus[2]
        }
        if tenancyAgreement.tenantNames.count > 3 {
            tenant4NameLabel.text = tenancyAgreement.tenantNames[3]
            tenant4StatusLabel.text = tenancyAgreement.tenantStatus[3]
        }
        if tenancyAgreement.tenantNames.count > 4 {
            tenant5NameLabel.text = tenancyAgreement.tenantNames[4]
            tenant5StatusLabel.text = tenancyAgreement.tenantStatus[4]
        }
        if tenancyAgreement.tenantNames.count > 5 {
            tenant6NameLabel.text = tenancyAgreement.tenantNames[5]
            tenant6StatusLabel.text = tenancyAgreement.tenantStatus[5]
        }
        if tenancyAgreement.tenantNames.count > 6 {
            tenant7NameLabel.text = tenancyAgreement.tenantNames[6]
            tenant7StatusLabel.text = tenancyAgreement.tenantStatus[6]
        }
        if tenancyAgreement.tenantNames.count > 7 {
            tenant8NameLabel.text = tenancyAgreement.tenantNames[7]
            tenant8StatusLabel.text = tenancyAgreement.tenantStatus[7]
        }
        if tenancyAgreement.tenantNames.count > 8 {
            tenant9NameLabel.text = tenancyAgreement.tenantNames[8]
            tenant9StatusLabel.text = tenancyAgreement.tenantStatus[8]
        }
        if tenancyAgreement.tenantNames.count > 9 {
            tenant10NameLabel.text = tenancyAgreement.tenantNames[9]
            tenant10StatusLabel.text = tenancyAgreement.tenantStatus[9]
        }
        
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return 168
        case 1:
            if tenancyAgreement.tenancyAgreementStatus == "Received" && UserData.sharedUserData.role == 2 {
                return 85
            } else if tenancyAgreement.tenancyAgreementStatus == "Received" {
                return 0
            } else if tenancyAgreement.tenancyAgreementStatus == "Ready" {
                return 50
            }
            return 85
        case 2:
            return CGFloat(140 + ((tenancyAgreement.landlordNames.count + tenancyAgreement.tenantNames.count) * 19 ))
        case 3:
            return 3
        case 4:
            return 44
        case 5: // Esign
            if shouldHideEsign && tenancyAgreement.tenancyAgreementStatus != "Done" {
                return 0
            }
            return 44
        case 26:
            if tenancyAgreement.eStampStatus == "-" && tenancyAgreement.eSignStatus != "ALL_SIGNED" {
                return 0
            }
            return 44
        default:
            if shouldHideEsign {
                return 0
            }
            if indexPath.row < 16 {
                let currentRow = indexPath.row - 5
                if currentRow > tenancyAgreement.landlordNames.count {
                    return 0
                }
            } else if indexPath.row < 26 {
                let currentRow = indexPath.row - 15
                if currentRow > tenancyAgreement.tenantNames.count {
                    return 0
                }
            }
            return 25
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "previewPDFSegue" {
            let PreviewTenancyAgreementVC: PreviewTenancyAgreementViewController = segue.destination as! PreviewTenancyAgreementViewController
            PreviewTenancyAgreementVC.tenancyAgreement = self.tenancyAgreement
            PreviewTenancyAgreementVC.isTAPreview = true
        } else if segue.identifier == "viewPDFSegue" {
            let PreviewTenancyAgreementVC: PreviewTenancyAgreementViewController = segue.destination as! PreviewTenancyAgreementViewController
            PreviewTenancyAgreementVC.tenancyAgreement = self.tenancyAgreement
            PreviewTenancyAgreementVC.isTAPreview = false
        } else if segue.identifier == "toAutoReminderViewController" {
            let AutoReminderVC: AutoReminderViewController = segue.destination as! AutoReminderViewController
            AutoReminderVC.tenancyAgreement = self.tenancyAgreement
        } else if segue.identifier == "toExplanationLanguageSelectVC" {
            let ExplanationLanguageSelectionVC: ExplanationLanguageSelectionViewController = segue.destination as! ExplanationLanguageSelectionViewController
            ExplanationLanguageSelectionVC.tenancyAgreement = self.tenancyAgreement
        } else if segue.identifier == "goToAdminEditSpecialCondition" {
            let AdminEditSpecialConditionVC: AdminEditSpecialConditionViewController = segue.destination as! AdminEditSpecialConditionViewController
            AdminEditSpecialConditionVC.orderID = self.tenancyAgreement.order_id
            AdminEditSpecialConditionVC.specialRequest = self.tenancyAgreement.specialRequests
        }
    }
}
