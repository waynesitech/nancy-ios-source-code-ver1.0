//
//  SignUpViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 18/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD
import libPhoneNumber_iOS

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var contentScroll : UIScrollView!
    
    @IBOutlet weak var nameTextField : UITextField!
    @IBOutlet weak var icTextField : UITextField!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var mobileTextField : UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var termsAndServiceButton: UIButton!
    @IBOutlet weak var termsAndServiceCheckboxButton: UIButton!
    
    @IBOutlet weak var nameErrorIcon: UIImageView!
    @IBOutlet weak var icErrorIcon: UIImageView!
    @IBOutlet weak var emailErrorIcon: UIImageView!
    @IBOutlet weak var mobileErrorIcon: UIImageView!
    @IBOutlet weak var passwordErrorIcon: UIImageView!
    @IBOutlet weak var repeatPasswordErrorIcon: UIImageView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    var termsAgreed: Bool = false
    var country = Country(code: "MY", name: "Malaysia", phoneCode: "+60")
    
    @IBAction func termsAndServiceCheckboxPressed(_ sender: AnyObject) {
        if (termsAgreed) {
            termsAndServiceCheckboxButton.setImage(UIImage(named: "checkboxUnpressed.png"), for: .normal)
            termsAgreed = false
        } else {
            termsAndServiceCheckboxButton.setImage(UIImage(named: "checkboxPressed.png"), for: .normal)
            termsAgreed = true
        }
    }
    
    @IBAction func signUpPressed(_ sender: AnyObject) {
        
        resetErrorIcon()
        
        let name = nameTextField.text!
        let ic = icTextField.text!
        let email = emailTextField.text!
        var mobile = mobileTextField.text!
        let password = passwordTextField.text!
        let repeatPassword = repeatPasswordTextField.text!
        
        if name.characters.count == 0 {
            errorMessageLabel.text = "Name can not be empty."
            nameErrorIcon.isHidden = false
            nameTextField.becomeFirstResponder()
            return
        } else if ic.characters.count < 12 {
            errorMessageLabel.text = "IC Number is invalid"
            icErrorIcon.isHidden = false
            icTextField.becomeFirstResponder()
            return
        } else if !isValidEmail(testStr: email) {
            errorMessageLabel.text = "Email is invalid"
            emailErrorIcon.isHidden = false
            emailTextField.becomeFirstResponder()
            return
        } else if !isValidPhoneNumber(testNumber: mobile) {
            errorMessageLabel.text = "Mobile Number is invalid"
            mobileErrorIcon.isHidden = false
            mobileTextField.becomeFirstResponder()
            return
        } else if password != repeatPassword {
            errorMessageLabel.text = "Password does not match"
            passwordErrorIcon.isHidden = false
            repeatPasswordErrorIcon.isHidden = false
            passwordTextField.becomeFirstResponder()
            return
        } else if !termsAgreed {
            errorMessageLabel.text = "Please agree on the terms and service of NanCy."
            return
        }
        
        mobile = getValidPhoneNumber(validNumber: mobile)
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkLoader.sharedLoader.register(name, ic: ic, email: email, mobile: mobile, password: password) {
            networkLoaderError, jsonResult in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let networkLoaderError = networkLoaderError {
                // Handle Error Message
                self.errorMessageLabel.text = "\(networkLoaderError.networkLoaderErrorTitle)\n\(networkLoaderError.networkLoaderErrorBody)"
            } else if let jsonResult = jsonResult {
                self.performSegue(withIdentifier: "showPhoneVerificationSegue", sender: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.willShowKeyBoard(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.willHideKeyBoard(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let bold = [
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 13.0),
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let black = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 12.0),
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let tinyBlack = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 10.0),
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let tinyItalic = [
            NSAttributedStringKey.font : UIFont.italicSystemFont(ofSize: 10.0),
            NSAttributedStringKey.foregroundColor : UIColor.black,
            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue
            ] as [NSAttributedStringKey : Any]
        
        let signInLabel = NSMutableAttributedString(string:"Have an Account?", attributes:black)
        signInLabel.append(NSMutableAttributedString(string:" Sign In ", attributes:bold))
        signInLabel.append(NSMutableAttributedString(string:"now!", attributes:black))
        signInButton.setAttributedTitle(signInLabel, for: .normal)
        
        let termsAndServiceLabel = NSMutableAttributedString(string:"I have read and agree to the NanCy ", attributes:tinyBlack)
        termsAndServiceLabel.append(NSMutableAttributedString(string:"Terms and Service.", attributes:tinyItalic))
        termsAndServiceButton.setAttributedTitle(termsAndServiceLabel, for: .normal)
        
        resetErrorIcon()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.countryCodeTextField.text = country.phoneCode
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectCountryCodeSegue" {
            let countrySelectVC: CountrySelectViewController = segue.destination as! CountrySelectViewController
            countrySelectVC.country = self.country
            countrySelectVC.didSelectCountryProtocolDelegate = self
        }
    }
    
    func resetErrorIcon() {
        nameErrorIcon.isHidden = true
        icErrorIcon.isHidden = true
        emailErrorIcon.isHidden = true
        mobileErrorIcon.isHidden = true
        passwordErrorIcon.isHidden = true
        repeatPasswordErrorIcon.isHidden = true
        errorMessageLabel.text = ""
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhoneNumber(testNumber:String) -> Bool {
        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil!.parse(testNumber, defaultRegion: country.code)
            return phoneUtil!.isValidNumber(phoneNumber)
        }
        catch let error as NSError {
            return false
        }
    }
    
    func getValidPhoneNumber(validNumber: String) -> String {
        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil!.parse(validNumber, defaultRegion: country.code)
            let formattedString: String = try phoneUtil!.format(phoneNumber, numberFormat: .E164)
            return formattedString
        }
        catch {
            return ""
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    
    @objc func willShowKeyBoard(_ notification : Notification){
        self.contentScroll.contentInset.bottom = 184
    }
    
    @objc func willHideKeyBoard(_ notification : Notification){
        self.contentScroll.contentInset.bottom = 0
    }
    
    func textFieldShouldReturn (_ textField: UITextField) -> Bool{
        if (textField == nameTextField){
            icTextField.becomeFirstResponder()
        } else if (textField == icTextField){
            emailTextField.becomeFirstResponder()
        } else if (textField == emailTextField){
            mobileTextField.becomeFirstResponder()
        } else if (textField == mobileTextField){
            passwordTextField.becomeFirstResponder()
        } else if (textField == passwordTextField){
            repeatPasswordTextField.becomeFirstResponder()
        } else if (textField == repeatPasswordTextField){
            repeatPasswordTextField.resignFirstResponder()
            self.contentScroll.contentInset.bottom = 0
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryCodeTextField {
            self.view.endEditing(true)
            self.performSegue(withIdentifier: "selectCountryCodeSegue", sender: nil)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryCodeTextField {
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension SignUpViewController : didSelectCountryProtocol {
    func didSelectCountry(selectedCountry: Country) {
        self.country = selectedCountry
    }
}
