//
//  NewTenancyLandlordTenantDetailsViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import MBProgressHUD
import libPhoneNumber_iOS
import DLRadioButton

class NewTenancyLandlordTenantDetailsViewController: UITableViewController {
    
    @IBOutlet weak var viewControllerTitle: UILabel!
    
    @IBOutlet weak var isIndividualRadio: DLRadioButton!
    @IBOutlet weak var isCompanyRadio: DLRadioButton!
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var companyRegistrationNumberTextField: UITextField!
    @IBOutlet weak var nameTextField : UITextField!
    @IBOutlet weak var icTextField : UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var mobileTextField : UITextField!
    @IBOutlet weak var countryCodeTextField: UITextField!
    @IBOutlet weak var addAnotherButton: RoundOrangeColoredButton!
    
    var landlordTenant: LandlordTenant = LandlordTenant()
    var isEditingLandlordTenant: Bool = false
    var isCase1: Bool = false // NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 && newTenancyAgreement.isLandlord
    
    var isCase2: Bool = false // NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 && !newTenancyAgreement.isLandlord
    
    var isCase3: Bool = false // NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 && !newTenancyAgreement.isLandlord
    
    var isCase4: Bool = false // NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 && newTenancyAgreement.isLandlord
    
    @IBAction func IndividualCompanyRadioPressed(_ sender: DLRadioButton) {
        if sender == isIndividualRadio {
            landlordTenant.isCompany = false
        } else {
            landlordTenant.isCompany = true
        }
        tableView.reloadData()
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        if (isCase1 || isCase2) && NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration > 0 {
            
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration - 1
            
        } else if (isCase3 || isCase4) &&
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration > 0 {
            
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration - 1
            
        } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 {
            
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 1
            
        } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 {
            
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 2
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = 0
            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = 0
            
        }
        if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 1 {
            if let navController = self.navigationController, navController.viewControllers.count >= 2 {
                if navController.viewControllers[navController.viewControllers.count - 2] is NewTenancyPropertyDetailsViewController {
                    navController.popViewController(animated: true)
                } else if navController.viewControllers[navController.viewControllers.count - 2] is NewTenancySelectPartyViewController {
                    navController.popViewController(animated: true)
                }
            }
        } else {
            self.viewDidLoad()
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: AnyObject) {
        if verifyAllFields() {
            let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
            if isEditingLandlordTenant {
                if isCase1 || isCase2 {
                    newTenancyAgreement.landlords[NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration] = landlordTenant
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration + 1
                    
                    if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration == newTenancyAgreement.landlords.count {
                        if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 {
                            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 4
                            newTenancyAgreement.currentStep = 4
                            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
                            performSegue(withIdentifier: "proceedFromLandlordTenantDetails", sender: nil)
                            return
                        } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 {
                            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 3
                            newTenancyAgreement.currentStep = 3
                        }
                    }
                } else if isCase3 || isCase4 {
                    newTenancyAgreement.tenants[NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration] = landlordTenant
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration + 1
                    
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
                    if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration == newTenancyAgreement.tenants.count {
                        if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 {
                            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 4
                            newTenancyAgreement.currentStep = 4
                            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
                            performSegue(withIdentifier: "proceedFromLandlordTenantDetails", sender: nil)
                            return
                        } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 {
                            NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 3
                            newTenancyAgreement.currentStep = 3
                        }
                    }
                }
                
            } else {
                if isCase1 || isCase2{
                    newTenancyAgreement.landlords.append(landlordTenant)
                } else if isCase3 || isCase4 {
                    newTenancyAgreement.tenants.append(landlordTenant)
                }
                
                if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 {
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 4
                    newTenancyAgreement.currentStep = 4
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
                    performSegue(withIdentifier: "proceedFromLandlordTenantDetails", sender: nil)
                    return
                } else if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 {
                    NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 3
                    newTenancyAgreement.currentStep = 3
                }
            }
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
            self.viewDidLoad()
        }
    }
    
    @IBAction func addAnotherButtonPressed(_ sender: Any) {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        if isEditingLandlordTenant {
            if isCase1 || isCase2 {
                if newTenancyAgreement.landlords.count == NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration + 1 {
                    if verifyAllFields() {
                        newTenancyAgreement.landlords[NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration] = landlordTenant
                        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration + 1
                    }
                } else {
                    newTenancyAgreement.landlords.remove(at: NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration)
                    if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration > newTenancyAgreement.landlords.count {
                        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = newTenancyAgreement.landlords.count
                    }
                }
            } else if isCase3 || isCase4 {
                if newTenancyAgreement.tenants.count == NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration + 1 {
                    if verifyAllFields() {
                        newTenancyAgreement.tenants[NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration] = landlordTenant
                        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration + 1
                    }
                } else {
                    newTenancyAgreement.tenants.remove(at: NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration)
                    if NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration > newTenancyAgreement.tenants.count {
                        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = newTenancyAgreement.tenants.count
                    }
                }
            }
        } else if verifyAllFields() {
            if isCase1 || isCase2{
                newTenancyAgreement.landlords.append(landlordTenant)
                NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration + 1
            } else if isCase3 || isCase4 {
                newTenancyAgreement.tenants.append(landlordTenant)
                NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration + 1
            }
        }
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        self.viewDidLoad()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        self.isCase1 = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 && newTenancyAgreement.isLandlord
        
        self.isCase2 = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 && !newTenancyAgreement.isLandlord
        
        self.isCase3 = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 && !newTenancyAgreement.isLandlord
        
        self.isCase4 = NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 3 && newTenancyAgreement.isLandlord
        self.resetAllFields()
        if isCase1 || isCase2 {
            if newTenancyAgreement.landlords.count > NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration && NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration >= 0 {
                showCurrentInformation(landlordTenant: newTenancyAgreement.landlords[NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration])
                viewControllerTitle.text = "LandLord Details \(NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration + 1)"
                if newTenancyAgreement.landlords.count != NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration + 1 {
                    addAnotherButton.setTitle("Delete This", for: .normal)
                }
                isEditingLandlordTenant = true
            } else {
                if newTenancyAgreement.landlords.count == 0 && NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 {
                    prefillUserInformation()
                }
                viewControllerTitle.text = "LandLord Details \(newTenancyAgreement.landlords.count + 1)"
                isEditingLandlordTenant = false
            }
        } else if isCase3 || isCase4 {
            if newTenancyAgreement.tenants.count > NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration && NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration >= 0 {
                showCurrentInformation(landlordTenant: newTenancyAgreement.tenants[NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration])
                viewControllerTitle.text = "Tenant Details \(NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration + 1)"
                if newTenancyAgreement.tenants.count != NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration + 1 {
                    addAnotherButton.setTitle("Delete This", for: .normal)
                }
                isEditingLandlordTenant = true
            } else {
                if newTenancyAgreement.tenants.count == 0 && NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen == 2 {
                    prefillUserInformation()
                }
                viewControllerTitle.text = "Tenant Details \(newTenancyAgreement.tenants.count + 1)"
                isEditingLandlordTenant = false
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.countryCodeTextField.text = self.landlordTenant.countryCode.phoneCode
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectPhoneCodeSegue" {
            let countrySelectVC: CountrySelectViewController = segue.destination as! CountrySelectViewController
            countrySelectVC.country = self.landlordTenant.countryCode
            countrySelectVC.didSelectCountryProtocolDelegate = self
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 88.0
        } else if !self.landlordTenant.isCompany {
            if indexPath.row == 2 || indexPath.row == 3 {
                return 0.0
            }
        }
        return 44.0
    }
    
    func showCurrentInformation(landlordTenant: LandlordTenant) {
        nameTextField.text = landlordTenant.name
        emailTextField.text = landlordTenant.email
        self.landlordTenant.countryCode = landlordTenant.countryCode
        mobileTextField.text = landlordTenant.contactNum
        addressTextField.text = landlordTenant.address
        icTextField.text = landlordTenant.icNum
        self.landlordTenant.isCompany = landlordTenant.isCompany
        if landlordTenant.isCompany {
            isCompanyRadio.isSelected = true
            isIndividualRadio.isSelected = false
            companyNameTextField.text = landlordTenant.companyName
            companyRegistrationNumberTextField.text = landlordTenant.companyRegNum
        } else {
            isIndividualRadio.isSelected = true
            isCompanyRadio.isSelected = false
        }
        isIndividualRadio.setNeedsDisplay()
        isCompanyRadio.setNeedsDisplay()
        tableView.reloadData()
    }
    
    func resetAllFields() {
        self.landlordTenant = LandlordTenant()
        self.isEditingLandlordTenant = false
        addAnotherButton.setTitle("Add Another", for: .normal)
        nameTextField.text = ""
        emailTextField.text = ""
        self.landlordTenant.countryCode = Country(code: "MY")
        mobileTextField.text = ""
        addressTextField.text = ""
        icTextField.text = ""
        companyNameTextField.text = ""
        companyRegistrationNumberTextField.text = ""
        isIndividualRadio.isSelected = true
        isCompanyRadio.isSelected = false
        isIndividualRadio.setNeedsDisplay()
        isCompanyRadio.setNeedsDisplay()
        tableView.reloadData()
    }
    
    func prefillUserInformation() {
        nameTextField.text = UserData.sharedUserData.name
        emailTextField.text = UserData.sharedUserData.email
        
        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        do {
            var mobileNumber: NSString? = nil
            let countryCode: NSNumber = phoneUtil!.extractCountryCode(UserData.sharedUserData.mobile, nationalNumber: &mobileNumber)
            self.landlordTenant.countryCode = Country(phoneCode: "+\(countryCode)")
            if let phoneNumber = mobileNumber {
                mobileTextField.text = phoneNumber as! String
            }
        }
        catch let error as NSError {
        }
    }
    
    func verifyAllFields() -> Bool {
        
        if landlordTenant.isCompany {
            let companyName = companyNameTextField.text!
            let companyRegistrationNumber = companyRegistrationNumberTextField.text!
            if companyName.characters.count == 0 {
                showAlertMessage(errorMessage: "Company name can not be empty.")
                companyNameTextField.becomeFirstResponder()
                return false
            } else if companyRegistrationNumber.characters.count == 0 {
                showAlertMessage(errorMessage: "Company registration number can not be empty.")
                companyRegistrationNumberTextField.becomeFirstResponder()
                return false
            }
            landlordTenant.companyName = companyName
            landlordTenant.companyRegNum = companyRegistrationNumber
        }
        
        let name = nameTextField.text!
        let ic = icTextField.text!
        let address = addressTextField.text!
        let email = emailTextField.text!
        let mobile = mobileTextField.text!
        
        if name.characters.count == 0 {
            showAlertMessage(errorMessage: "Name can not be empty.")
            nameTextField.becomeFirstResponder()
            return false
        } else if ic.characters.count < 12 {
            showAlertMessage(errorMessage: "IC Number is invalid")
            icTextField.becomeFirstResponder()
            return false
        } else if address.characters.count == 0 {
            showAlertMessage(errorMessage: "Address can not be empty")
            addressTextField.becomeFirstResponder()
            return false
        } else if !isValidPhoneNumber(testNumber: mobile) {
            showAlertMessage(errorMessage: "Mobile Number is invalid")
            mobileTextField.becomeFirstResponder()
            return false
        } else if !isValidEmail(testStr: email) {
            showAlertMessage(errorMessage: "Email is invalid")
            emailTextField.becomeFirstResponder()
            return false
        }
        
        landlordTenant.name = name
        landlordTenant.icNum = ic
        landlordTenant.address = address
        landlordTenant.contactNum = mobile
        landlordTenant.email = email
        
        return true
    }
    
    func showAlertMessage(errorMessage: String) {
        let alert = UIAlertController(title: "", message: errorMessage, preferredStyle: .alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhoneNumber(testNumber:String) -> Bool {
        let phoneUtil = NBPhoneNumberUtil.sharedInstance()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil!.parse(testNumber, defaultRegion: landlordTenant.countryCode.code)
            return phoneUtil!.isValidNumber(phoneNumber)
        }
        catch let error as NSError {
            return false
        }
    }
}

extension NewTenancyLandlordTenantDetailsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn (_ textField: UITextField) -> Bool{
        if (textField == companyNameTextField){
            companyRegistrationNumberTextField.becomeFirstResponder()
        } else if (textField == companyRegistrationNumberTextField){
            nameTextField.becomeFirstResponder()
        } else if (textField == nameTextField){
            icTextField.becomeFirstResponder()
        } else if (textField == icTextField){
            addressTextField.becomeFirstResponder()
        } else if (textField == addressTextField){
            mobileTextField.becomeFirstResponder()
        } else if (textField == mobileTextField){
            emailTextField.becomeFirstResponder()
        } else if (textField == emailTextField){
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryCodeTextField {
            self.view.endEditing(true)
            self.performSegue(withIdentifier: "selectPhoneCodeSegue", sender: nil)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == countryCodeTextField {
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

extension NewTenancyLandlordTenantDetailsViewController : didSelectCountryProtocol {
    func didSelectCountry(selectedCountry: Country) {
        self.landlordTenant.countryCode = selectedCountry
    }
}
