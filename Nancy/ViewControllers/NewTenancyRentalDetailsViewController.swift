//
//  NewTenancyRentalDetailsViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 08/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import BEMCheckBox
import DLRadioButton

class NewTenancyRentalDetailsViewController: UITableViewController {
    @IBOutlet weak var tenancyRenewalSwitch: BEMCheckBox!
    @IBOutlet weak var carParkStepper: UIStepper!
    @IBOutlet weak var carParkAmount: UILabel!
    @IBOutlet weak var commencementDatePicker: UIDatePicker!
    @IBOutlet weak var termsOfTenancyMonths: UITextField!
    @IBOutlet weak var termsOfTenancyYears: UITextField!
    
    @IBOutlet weak var optionToRenewTableViewCell: UITableViewCell!
    @IBOutlet weak var optionToRenewMonths: UITextField!
    @IBOutlet weak var optionToRenewYears: UITextField!
    
    @IBOutlet weak var marketPrevailingRadio: DLRadioButton!
    @IBOutlet weak var increase15PercentRadio: DLRadioButton!
    @IBOutlet weak var increase10PercentRadio: DLRadioButton!
    @IBOutlet weak var mutuallyAgreeRadio: DLRadioButton!
    @IBOutlet weak var otherTermsOfRenewalRadio: DLRadioButton!
    
    @IBOutlet weak var otherTermsOfRenewalTextField: UITextField!
    
    @IBOutlet weak var rentalRinggitTextField: UITextField!
    @IBOutlet weak var rentalSenTextField: UITextField!
    
    @IBOutlet weak var advancedRentalRinggitTextField: UITextField!
    @IBOutlet weak var advancedRentalSenTextField: UITextField!
    
    @IBOutlet weak var securityDepositRentRinggitTextField: UITextField!
    @IBOutlet weak var securityDepositRentSenTextField: UITextField!
    
    @IBOutlet weak var securityDepositUtilitiesRinggitTextField: UITextField!
    @IBOutlet weak var securityDepositUtilitiesSenTextField: UITextField!
    
    @IBOutlet weak var securityDepositOthers1RinggitTextField: UITextField!
    @IBOutlet weak var securityDepositOthers1TextField: UITextField!
    @IBOutlet weak var securityDepositOthers1SenTextField: UITextField!
    @IBOutlet weak var securityDepositOthers1Button: UIButton!
    
    @IBOutlet weak var securityDepositOthers2RinggitTextField: UITextField!
    @IBOutlet weak var securityDepositOthers2TextField: UITextField!
    @IBOutlet weak var securityDepositOthers2SenTextField: UITextField!
    @IBOutlet weak var securityDepositOthers2Button: UIButton!
    
    @IBOutlet weak var securityDepositOthers3RinggitTextField: UITextField!
    @IBOutlet weak var securityDepositOthers3TextField: UITextField!
    @IBOutlet weak var securityDepositOthers3SenTextField: UITextField!
    @IBOutlet weak var securityDepositOthers3Button: UIButton!
    
    @IBOutlet weak var securityDepositOthers4RinggitTextField: UITextField!
    @IBOutlet weak var securityDepositOthers4TextField: UITextField!
    @IBOutlet weak var securityDepositOthers4SenTextField: UITextField!
    @IBOutlet weak var securityDepositOthers4Button: UIButton!
    
    @IBOutlet weak var securityDepositOthers5RinggitTextField: UITextField!
    @IBOutlet weak var securityDepositOthers5TextField: UITextField!
    @IBOutlet weak var securityDepositOthers5SenTextField: UITextField!
    @IBOutlet weak var securityDepositOthers5Button: UIButton!
    
    @IBOutlet weak var rentalFreeDurationRadio: DLRadioButton!
    @IBOutlet weak var rentalFreeSpecificDatesRadio: DLRadioButton!
    
    @IBOutlet weak var rentalFreeWeeksTextField: UITextField!
    @IBOutlet weak var rentalFreeMonthsTextField: UITextField!
    @IBOutlet weak var rentalFreeYearsTextField: UITextField!
    
    @IBOutlet weak var rentalFreeFromDatePicker: UIDatePicker!
    @IBOutlet weak var rentalFreeToDatePicker: UIDatePicker!
    
    @IBOutlet weak var carParkLotNumber1TextField: UITextField!
    @IBOutlet weak var carParkLotNumber2TextField: UITextField!
    @IBOutlet weak var carParkLotNumber3TextField: UITextField!
    @IBOutlet weak var carParkLotNumber4TextField: UITextField!
    @IBOutlet weak var carParkLotNumber5TextField: UITextField!
    
    var isOptionToRenew: Bool = false
    var isMarketPrevaillingRate: Bool = false
    var isIncreaseOf10percent: Bool = false
    var isIncreaseOf15percent: Bool = false
    var isMutuallyAgreedRate: Bool = false
    var isOtherPercent: Bool = false
    var isRentalFreePeriodDuration: Bool = true
    var otherTermsofRenewal: String = ""
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        if NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement.tenancyAgreementPDF != nil {
            performSegue(withIdentifier: "goToPreviewViewController", sender: nil)
        } else {
            performSegue(withIdentifier: "goToPaymentViewController", sender: nil)
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentlyShowingScreen = 3
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentLandlordIteration = newTenancyAgreement.landlords.count - 1
        NewTenancyAgreementData.sharedNewTenancyAgreementData.currentTenantIteration = newTenancyAgreement.tenants.count - 1
        if let navController = self.navigationController, navController.viewControllers.count >= 2 {
            if navController.viewControllers[navController.viewControllers.count - 2] is NewTenancyLandlordTenantDetailsViewController {
                navController.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func termsOfRenewalRadioSelected(_ sender: DLRadioButton) {
        isMarketPrevaillingRate = false
        isIncreaseOf10percent = false
        isIncreaseOf15percent = false
        isMutuallyAgreedRate = false
        isOtherPercent = false
        otherTermsOfRenewalTextField.isEnabled = false
        
        switch sender {
        case marketPrevailingRadio:
            isMarketPrevaillingRate = true
            marketPrevailingRadio.isSelected = true
        case increase10PercentRadio:
            isIncreaseOf10percent = true
            increase10PercentRadio.isSelected = true
        case increase15PercentRadio:
            isIncreaseOf15percent = true
            increase15PercentRadio.isSelected = true
        case mutuallyAgreeRadio:
            isMutuallyAgreedRate = true
            mutuallyAgreeRadio.isSelected = true
        case otherTermsOfRenewalRadio:
            otherTermsOfRenewalRadio.isSelected = true
            isOtherPercent = true
            otherTermsOfRenewalTextField.isEnabled = true
        default:
            break
        }
    }
    
    @IBAction func rentalFreeRadioSelected(_ sender: DLRadioButton) {
        isRentalFreePeriodDuration = false
        
        switch sender {
        case rentalFreeDurationRadio:
            isRentalFreePeriodDuration = true
        case rentalFreeSpecificDatesRadio:
            isRentalFreePeriodDuration = false
        default:
            break
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func securityDepositAddButton(_ sender: UIButton) {
        switch sender {
        case securityDepositOthers1Button:
            if numberOfSecurityDeposit == 1 {
                numberOfSecurityDeposit = 2
            } else {
                numberOfSecurityDeposit -= 1
                securityDepositOthers1TextField.text = securityDepositOthers2TextField.text
                securityDepositOthers1SenTextField.text = securityDepositOthers2SenTextField.text
                securityDepositOthers1RinggitTextField.text = securityDepositOthers2RinggitTextField.text
                
                securityDepositOthers2TextField.text = securityDepositOthers3TextField.text
                securityDepositOthers2SenTextField.text = securityDepositOthers3SenTextField.text
                securityDepositOthers2RinggitTextField.text = securityDepositOthers3RinggitTextField.text
                
                securityDepositOthers3TextField.text = securityDepositOthers4TextField.text
                securityDepositOthers3SenTextField.text = securityDepositOthers4SenTextField.text
                securityDepositOthers3RinggitTextField.text = securityDepositOthers4RinggitTextField.text
                
                securityDepositOthers4TextField.text = securityDepositOthers5TextField.text
                securityDepositOthers4SenTextField.text = securityDepositOthers5SenTextField.text
                securityDepositOthers4RinggitTextField.text = securityDepositOthers5RinggitTextField.text
            }
        case securityDepositOthers2Button:
            if numberOfSecurityDeposit == 2 {
                numberOfSecurityDeposit = 3
            } else {
                numberOfSecurityDeposit -= 1
                securityDepositOthers2TextField.text = securityDepositOthers3TextField.text
                securityDepositOthers2SenTextField.text = securityDepositOthers3SenTextField.text
                securityDepositOthers2RinggitTextField.text = securityDepositOthers3RinggitTextField.text
                
                securityDepositOthers3TextField.text = securityDepositOthers4TextField.text
                securityDepositOthers3SenTextField.text = securityDepositOthers4SenTextField.text
                securityDepositOthers3RinggitTextField.text = securityDepositOthers4RinggitTextField.text
                
                securityDepositOthers4TextField.text = securityDepositOthers5TextField.text
                securityDepositOthers4SenTextField.text = securityDepositOthers5SenTextField.text
                securityDepositOthers4RinggitTextField.text = securityDepositOthers5RinggitTextField.text
            }
        case securityDepositOthers3Button:
            if numberOfSecurityDeposit == 3 {
                numberOfSecurityDeposit = 4
            } else {
                numberOfSecurityDeposit -= 1
                securityDepositOthers3TextField.text = securityDepositOthers4TextField.text
                securityDepositOthers3SenTextField.text = securityDepositOthers4SenTextField.text
                securityDepositOthers3RinggitTextField.text = securityDepositOthers4RinggitTextField.text
                
                securityDepositOthers4TextField.text = securityDepositOthers5TextField.text
                securityDepositOthers4SenTextField.text = securityDepositOthers5SenTextField.text
                securityDepositOthers4RinggitTextField.text = securityDepositOthers5RinggitTextField.text
            }
        case securityDepositOthers4Button:
            if numberOfSecurityDeposit == 4 {
                numberOfSecurityDeposit = 5
            } else {
                numberOfSecurityDeposit -= 1
                securityDepositOthers4TextField.text = securityDepositOthers5TextField.text
                securityDepositOthers4SenTextField.text = securityDepositOthers5SenTextField.text
                securityDepositOthers4RinggitTextField.text = securityDepositOthers5RinggitTextField.text
            }
        case securityDepositOthers5Button:
            numberOfSecurityDeposit -= 1
            securityDepositOthers5TextField.text = ""
            securityDepositOthers5SenTextField.text = ""
            securityDepositOthers5RinggitTextField.text = ""
        default:
            break
        }
        updateAllButtons()
    }
    
    @IBAction func carParkStepperValueChanged(_ sender: Any) {
        numberOfCarPark = Int(carParkStepper.value)
        updateNumberOfCarParkUI()
    }
    
    var numberOfCarPark: Int = 0
    var numberOfSecurityDeposit: Int = 1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        commencementDatePicker.date = newTenancyAgreement.commencementDate
        termsOfTenancyYears.text = String(newTenancyAgreement.termsOfTenancyYears)
        termsOfTenancyMonths.text = String(newTenancyAgreement.termsOfTenancyMonths)
        
        tenancyRenewalSwitch.on = newTenancyAgreement.isOptionToRenew
        optionToRenewYears.text = String(newTenancyAgreement.optionToRenewYears)
        optionToRenewMonths.text = String(newTenancyAgreement.optionToRenewMonths)
        isMutuallyAgreedRate = newTenancyAgreement.isMutuallyAgreedRate
        isIncreaseOf15percent = newTenancyAgreement.isIncreaseOf15percent
        isIncreaseOf10percent = newTenancyAgreement.isIncreaseOf10percent
        isOtherPercent = newTenancyAgreement.isOtherPercent
        isMarketPrevaillingRate = newTenancyAgreement.isMarketPrevaillingRate
        
        if isMutuallyAgreedRate {
            mutuallyAgreeRadio.isSelected = true
        } else if isIncreaseOf10percent {
            increase10PercentRadio.isSelected = true
        } else if isIncreaseOf15percent {
            increase15PercentRadio.isSelected = true
        } else if isMarketPrevaillingRate {
            marketPrevailingRadio.isSelected = true
        } else if isOtherPercent {
            otherTermsOfRenewalRadio.isSelected = true
            otherTermsOfRenewalTextField.text = newTenancyAgreement.otherTermsofRenewal
        }
        
        
        rentalRinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.rental)
        rentalSenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.rental)
        
        advancedRentalRinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.advanceRental)
        advancedRentalSenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.advanceRental)
        
        securityDepositRentRinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.securityDepositRent)
        securityDepositRentSenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.securityDepositRent)
        
        securityDepositUtilitiesRinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.securityDepositUtilities)
        securityDepositUtilitiesSenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.securityDepositUtilities)
        
        numberOfSecurityDeposit = newTenancyAgreement.otherSecurityDeposit.count
        if numberOfSecurityDeposit < 1 {
            numberOfSecurityDeposit = 1
        }
        if newTenancyAgreement.otherSecurityDeposit.count > 0 {
            securityDepositOthers1TextField.text = newTenancyAgreement.otherSecurityDeposit[0].name
            securityDepositOthers1RinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.otherSecurityDeposit[0].amount)
            securityDepositOthers1SenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.otherSecurityDeposit[0].amount)
        }
        if newTenancyAgreement.otherSecurityDeposit.count > 1 {
            securityDepositOthers2TextField.text = newTenancyAgreement.otherSecurityDeposit[1].name
            securityDepositOthers2RinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.otherSecurityDeposit[1].amount)
            securityDepositOthers2SenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.otherSecurityDeposit[1].amount)
        }
        if newTenancyAgreement.otherSecurityDeposit.count > 2 {
            securityDepositOthers3TextField.text = newTenancyAgreement.otherSecurityDeposit[2].name
            securityDepositOthers3RinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.otherSecurityDeposit[2].amount)
            securityDepositOthers3SenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.otherSecurityDeposit[2].amount)
        }
        if newTenancyAgreement.otherSecurityDeposit.count > 3 {
            securityDepositOthers4TextField.text = newTenancyAgreement.otherSecurityDeposit[3].name
            securityDepositOthers4RinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.otherSecurityDeposit[3].amount)
            securityDepositOthers4SenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.otherSecurityDeposit[3].amount)
        }
        if newTenancyAgreement.otherSecurityDeposit.count > 4 {
            securityDepositOthers5TextField.text = newTenancyAgreement.otherSecurityDeposit[4].name
            securityDepositOthers5RinggitTextField.text = fromDoubleToRinggit(amount: newTenancyAgreement.otherSecurityDeposit[4].amount)
            securityDepositOthers5SenTextField.text = fromDoubleToSen(amount: newTenancyAgreement.otherSecurityDeposit[4].amount)
        }
        
        isRentalFreePeriodDuration = newTenancyAgreement.isRentalFreePeriodDuration
        rentalFreeYearsTextField.text = String(newTenancyAgreement.rentalFreePeriodYears)
        rentalFreeMonthsTextField.text = String(newTenancyAgreement.rentalFreePeriodMonths)
        rentalFreeWeeksTextField.text = String(newTenancyAgreement.rentalFreePeriodWeeks)
        rentalFreeFromDatePicker.date = newTenancyAgreement.rentalFreePeriodStartDate
        rentalFreeToDatePicker.date = newTenancyAgreement.rentalFreePeriodEndDate
        
        numberOfCarPark = newTenancyAgreement.carParkLots.count
        carParkStepper.value = Double(numberOfCarPark)
        if newTenancyAgreement.carParkLots.count > 0 {
            carParkLotNumber1TextField.text = newTenancyAgreement.carParkLots[0]
        }
        if newTenancyAgreement.carParkLots.count > 1 {
            carParkLotNumber2TextField.text = newTenancyAgreement.carParkLots[1]
        }
        if newTenancyAgreement.carParkLots.count > 2 {
            carParkLotNumber3TextField.text = newTenancyAgreement.carParkLots[2]
        }
        if newTenancyAgreement.carParkLots.count > 3 {
            carParkLotNumber4TextField.text = newTenancyAgreement.carParkLots[3]
        }
        if newTenancyAgreement.carParkLots.count > 4 {
            carParkLotNumber5TextField.text = newTenancyAgreement.carParkLots[4]
        }
        updateNumberOfCarParkUI()
        updateAllButtons()
        
        if (!isRentalFreePeriodDuration) {
            rentalFreeSpecificDatesRadio.isSelected = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateStorageContent()
    }
    
    func updateStorageContent() {
        let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
        newTenancyAgreement.commencementDate = commencementDatePicker.date
        newTenancyAgreement.termsOfTenancyYears = textFieldToInt(textField: termsOfTenancyYears)
        newTenancyAgreement.termsOfTenancyMonths = textFieldToInt(textField: termsOfTenancyMonths)
        
        newTenancyAgreement.isOptionToRenew = tenancyRenewalSwitch.on
        newTenancyAgreement.optionToRenewYears = textFieldToInt(textField: optionToRenewYears)
        newTenancyAgreement.optionToRenewMonths = textFieldToInt(textField: optionToRenewMonths)
        newTenancyAgreement.isMutuallyAgreedRate = isMutuallyAgreedRate
        newTenancyAgreement.isIncreaseOf15percent = isIncreaseOf15percent
        newTenancyAgreement.isIncreaseOf10percent = isIncreaseOf10percent
        newTenancyAgreement.isOtherPercent = isOtherPercent
        newTenancyAgreement.isMarketPrevaillingRate = isMarketPrevaillingRate
        newTenancyAgreement.otherTermsofRenewal = textFieldToString(textField: otherTermsOfRenewalTextField)
        
        newTenancyAgreement.rental = combineRinggitSensToDouble(ringgitTextField: rentalRinggitTextField, sensTextField: rentalSenTextField)
        newTenancyAgreement.advanceRental = combineRinggitSensToDouble(ringgitTextField: advancedRentalRinggitTextField, sensTextField: advancedRentalSenTextField)
        newTenancyAgreement.securityDepositRent = combineRinggitSensToDouble(ringgitTextField: securityDepositRentRinggitTextField, sensTextField: securityDepositRentSenTextField)
        newTenancyAgreement.securityDepositUtilities = combineRinggitSensToDouble(ringgitTextField: securityDepositUtilitiesRinggitTextField, sensTextField: securityDepositUtilitiesSenTextField)
        
        var tempOtherSecurityDeposit: [SecurityDeposit] = []
        let others1 = textFieldToString(textField: securityDepositOthers1TextField)
        let others1amount = combineRinggitSensToDouble(ringgitTextField: securityDepositOthers1RinggitTextField, sensTextField: securityDepositOthers1SenTextField)
        if others1 != "" && others1amount != 0.0 && self.numberOfSecurityDeposit >= 1 {
            tempOtherSecurityDeposit.append(SecurityDeposit(name: others1, amount: others1amount))
        }
        
        let others2 = textFieldToString(textField: securityDepositOthers2TextField)
        let others2amount = combineRinggitSensToDouble(ringgitTextField: securityDepositOthers2RinggitTextField, sensTextField: securityDepositOthers2SenTextField)
        if others2 != "" && others2amount != 0.0 && self.numberOfSecurityDeposit >= 2 {
            tempOtherSecurityDeposit.append(SecurityDeposit(name: others2, amount: others2amount))
        }
        
        let others3 = textFieldToString(textField: securityDepositOthers3TextField)
        let others3amount = combineRinggitSensToDouble(ringgitTextField: securityDepositOthers3RinggitTextField, sensTextField: securityDepositOthers3SenTextField)
        if others3 != "" && others3amount != 0.0 && self.numberOfSecurityDeposit >= 3 {
            tempOtherSecurityDeposit.append(SecurityDeposit(name: others3, amount: others3amount))
        }
        
        let others4 = textFieldToString(textField: securityDepositOthers4TextField)
        let others4amount = combineRinggitSensToDouble(ringgitTextField: securityDepositOthers4RinggitTextField, sensTextField: securityDepositOthers4SenTextField)
        if others4 != "" && others4amount != 0.0 && self.numberOfSecurityDeposit >= 4 {
            tempOtherSecurityDeposit.append(SecurityDeposit(name: others4, amount: others4amount))
        }
        
        let others5 = textFieldToString(textField: securityDepositOthers5TextField)
        let others5amount = combineRinggitSensToDouble(ringgitTextField: securityDepositOthers5RinggitTextField, sensTextField: securityDepositOthers5SenTextField)
        if others5 != "" && others5amount != 0.0 && self.numberOfSecurityDeposit >= 5 {
            tempOtherSecurityDeposit.append(SecurityDeposit(name: others5, amount: others5amount))
        }
        newTenancyAgreement.otherSecurityDeposit = tempOtherSecurityDeposit
        
        newTenancyAgreement.isRentalFreePeriodDuration = isRentalFreePeriodDuration
        newTenancyAgreement.rentalFreePeriodYears = textFieldToInt(textField: rentalFreeYearsTextField)
        newTenancyAgreement.rentalFreePeriodMonths = textFieldToInt(textField: rentalFreeMonthsTextField)
        newTenancyAgreement.rentalFreePeriodWeeks = textFieldToInt(textField: rentalFreeWeeksTextField)
        newTenancyAgreement.rentalFreePeriodStartDate = rentalFreeFromDatePicker.date
        newTenancyAgreement.rentalFreePeriodEndDate = rentalFreeToDatePicker.date
        
        var tempCarParkLots: [String] = []
        let carParkLot1 = textFieldToString(textField: carParkLotNumber1TextField)
        if carParkLot1 != "" && self.numberOfCarPark >= 1 {
            tempCarParkLots.append(carParkLot1)
        }
        let carParkLot2 = textFieldToString(textField: carParkLotNumber2TextField)
        if carParkLot2 != "" && self.numberOfCarPark >= 2 {
            tempCarParkLots.append(carParkLot2)
        }
        let carParkLot3 = textFieldToString(textField: carParkLotNumber3TextField)
        if carParkLot3 != "" && self.numberOfCarPark >= 3 {
            tempCarParkLots.append(carParkLot3)
        }
        let carParkLot4 = textFieldToString(textField: carParkLotNumber4TextField)
        if carParkLot4 != "" && self.numberOfCarPark >= 4 {
            tempCarParkLots.append(carParkLot4)
        }
        let carParkLot5 = textFieldToString(textField: carParkLotNumber5TextField)
        if carParkLot5 != "" && self.numberOfCarPark >= 5 {
            tempCarParkLots.append(carParkLot5)
        }
        newTenancyAgreement.carParkLots = tempCarParkLots
        
        NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tenancyRenewalSwitch.boxType = .square
        carParkStepper.tintColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1)
        termsOfTenancyMonths.delegate = self
        termsOfTenancyYears.delegate = self
        optionToRenewMonths.delegate = self
        optionToRenewYears.delegate = self
        otherTermsOfRenewalTextField.delegate = self
        rentalRinggitTextField.delegate = self
        rentalSenTextField.delegate = self
        advancedRentalRinggitTextField.delegate = self
        advancedRentalSenTextField.delegate = self
        securityDepositRentRinggitTextField.delegate = self
        securityDepositRentSenTextField.delegate = self
        securityDepositUtilitiesRinggitTextField.delegate = self
        securityDepositUtilitiesSenTextField.delegate = self
        securityDepositOthers1RinggitTextField.delegate = self
        securityDepositOthers1TextField.delegate = self
        securityDepositOthers1SenTextField.delegate = self
        securityDepositOthers2RinggitTextField.delegate = self
        securityDepositOthers2TextField.delegate = self
        securityDepositOthers2SenTextField.delegate = self
        securityDepositOthers3RinggitTextField.delegate = self
        securityDepositOthers3TextField.delegate = self
        securityDepositOthers3SenTextField.delegate = self
        securityDepositOthers4RinggitTextField.delegate = self
        securityDepositOthers4TextField.delegate = self
        securityDepositOthers4SenTextField.delegate = self
        securityDepositOthers5RinggitTextField.delegate = self
        securityDepositOthers5TextField.delegate = self
        securityDepositOthers5SenTextField.delegate = self
        rentalFreeWeeksTextField.delegate = self
        rentalFreeMonthsTextField.delegate = self
        rentalFreeYearsTextField.delegate = self
        carParkLotNumber1TextField.delegate = self
        carParkLotNumber2TextField.delegate = self
        carParkLotNumber3TextField.delegate = self
        carParkLotNumber4TextField.delegate = self
        carParkLotNumber5TextField.delegate = self
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 109.0
        } else if indexPath.row == 7 {
            return 90.0
        } else if indexPath.row == 1 {
            return 150.0
        } else if indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6 {
            return 70.0
        } else if indexPath.row == 4  && tenancyRenewalSwitch.on {
            return 260.0
        } else if indexPath.row == 4  && !tenancyRenewalSwitch.on ||
            indexPath.row == 9 && numberOfSecurityDeposit < 2 ||
            indexPath.row == 10 && numberOfSecurityDeposit < 3 ||
            indexPath.row == 11 && numberOfSecurityDeposit < 4 ||
            indexPath.row == 12 && numberOfSecurityDeposit < 5 ||
            indexPath.row == 15 && !isRentalFreePeriodDuration ||
            indexPath.row == 17 && isRentalFreePeriodDuration ||
            indexPath.row == 20 && numberOfCarPark < 1 ||
            indexPath.row == 21 && numberOfCarPark < 2 ||
            indexPath.row == 22 && numberOfCarPark < 3 ||
            indexPath.row == 23 && numberOfCarPark < 4 ||
            indexPath.row == 24 && numberOfCarPark < 5 {
            return 0.0
        } else if indexPath.row == 8 ||
            indexPath.row == 9 ||
            indexPath.row == 10 ||
            indexPath.row == 11 ||
            indexPath.row == 12 ||
            indexPath.row == 13 ||
            indexPath.row == 14 ||
            indexPath.row == 15 ||
            indexPath.row == 16 ||
            indexPath.row == 20 ||
            indexPath.row == 21 ||
            indexPath.row == 22 ||
            indexPath.row == 23 ||
            indexPath.row == 24 {
            return 35.0
        } else if indexPath.row == 17 {
            return 280.0
        }
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "proceedFromRentalDetails" {
            let newTenancyAgreement = NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement
            newTenancyAgreement.currentStep = 5
            NewTenancyAgreementData.sharedNewTenancyAgreementData.newTenancyAgreement = newTenancyAgreement
        }
    }
    
    func fromDoubleToSen(amount: Double) -> String {
        let sen = Int(amount * 100) % 100
        if sen < 10 {
            return "0\(sen)"
        }
        return String(sen)
    }
    
    func fromDoubleToRinggit(amount: Double) -> String {
        return String(Int(amount))
    }
    
    func updateNumberOfCarParkUI() {
        carParkAmount.text = "Amount: \(numberOfCarPark)"
        tableView.reloadData()
    }
    
    func combineRinggitSensToDouble(ringgitTextField: UITextField, sensTextField: UITextField) -> Double {
        if let ringgit: String = ringgitTextField.text {
            if let ringgitInt: Double = Double(ringgit) {
                if let sens: String = sensTextField.text {
                    if let sensInt: Double = Double(sens) {
                        return ringgitInt + ( sensInt / 100.0 )
                    }
                }
            }
        }
        return 0.0
    }
    
    func textFieldToInt(textField: UITextField) -> Int {
        if let text: String = textField.text {
            if let integer: Int = Int(text) {
                return integer
            }
        }
        return 0
    }
    
    func textFieldToString(textField: UITextField) -> String {
        if let text: String = textField.text {
            return text
        }
        return ""
    }
    
    func updateAllButtons() {
        securityDepositOthers1Button.setImage(UIImage(named: "deleteThisButton"), for: .normal)
        securityDepositOthers2Button.setImage(UIImage(named: "deleteThisButton"), for: .normal)
        securityDepositOthers3Button.setImage(UIImage(named: "deleteThisButton"), for: .normal)
        securityDepositOthers4Button.setImage(UIImage(named: "deleteThisButton"), for: .normal)
        securityDepositOthers5Button.setImage(UIImage(named: "deleteThisButton"), for: .normal)
        switch numberOfSecurityDeposit {
        case 1:
            securityDepositOthers1Button.setImage(UIImage(named: "addMoreButton"), for: .normal)
        case 2:
            securityDepositOthers2Button.setImage(UIImage(named: "addMoreButton"), for: .normal)
        case 3:
            securityDepositOthers3Button.setImage(UIImage(named: "addMoreButton"), for: .normal)
        case 4:
            securityDepositOthers4Button.setImage(UIImage(named: "addMoreButton"), for: .normal)
        default:
            break
        }
        tableView.reloadData()
    }
}

extension NewTenancyRentalDetailsViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString text: String) -> Bool {
        let str = (textField.text! + text)
        var textLimit = 255
        if textField == rentalFreeWeeksTextField {
            if let strInt: Int = Int(str), strInt >= 0 && strInt < 5 && str.characters.count < 2 {
                return true
            }
            return false
        }
        if textField == termsOfTenancyMonths || textField == optionToRenewMonths || textField == rentalFreeMonthsTextField {
            if let strInt: Int = Int(str), strInt >= 0 && strInt < 12 && str.characters.count < 3 {
                return true
            }
            return false
        }
        if textField == termsOfTenancyYears || textField == optionToRenewYears || textField == rentalFreeYearsTextField {
            if let strInt: Int = Int(str), strInt >= 0 && strInt < 100 && str.characters.count < 3 {
                return true
            }
            return false
        }
        if textField == rentalRinggitTextField || textField == advancedRentalRinggitTextField || textField == securityDepositRentRinggitTextField || textField == securityDepositUtilitiesRinggitTextField || textField == securityDepositOthers1RinggitTextField || textField == securityDepositOthers2RinggitTextField || textField == securityDepositOthers3RinggitTextField || textField == securityDepositOthers4RinggitTextField || textField == securityDepositOthers5RinggitTextField {
            if let strInt: Int = Int(str), strInt >= 0 && strInt < 1000000 {
                return true
            }
            return false
        }
        if textField == rentalSenTextField || textField == advancedRentalSenTextField || textField == securityDepositRentSenTextField || textField == securityDepositUtilitiesSenTextField || textField == securityDepositOthers1SenTextField || textField == securityDepositOthers2SenTextField || textField == securityDepositOthers3SenTextField || textField == securityDepositOthers4SenTextField || textField == securityDepositOthers5SenTextField {
            if let strInt: Int = Int(str), strInt >= 0 && strInt < 100 && str.characters.count < 3 {
                return true
            }
            return false
        }
        
        if str.characters.count <= textLimit {
            return true
        }
        textField.text = str.substring(to: str.index(str.startIndex, offsetBy: textLimit))
        return false
    }
}

extension NewTenancyRentalDetailsViewController: BEMCheckBoxDelegate {
    
    func animationDidStop(for checkBox: BEMCheckBox) {
        self.tableView.reloadData()
        if isMutuallyAgreedRate {
            mutuallyAgreeRadio.isSelected = true
        } else if isIncreaseOf10percent {
            increase10PercentRadio.isSelected = true
        } else if isIncreaseOf15percent {
            increase15PercentRadio.isSelected = true
        } else if isMarketPrevaillingRate {
            marketPrevailingRadio.isSelected = true
        } else if isOtherPercent {
            otherTermsOfRenewalRadio.isSelected = true
        } else {
            marketPrevailingRadio.isSelected = true
        }
        mutuallyAgreeRadio.setNeedsDisplay()
        marketPrevailingRadio.setNeedsDisplay()
        increase10PercentRadio.setNeedsDisplay()
        increase15PercentRadio.setNeedsDisplay()
        otherTermsOfRenewalRadio.setNeedsDisplay()
    }
}
