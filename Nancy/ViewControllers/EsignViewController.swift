//
//  EsignViewController.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 02/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class EsignViewController: UIViewController {
    
    var orderUserID: Int = 0
    var orderUserName: String = ""
    
    @IBOutlet weak var uviSignatureView: UviSignatureView!
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        self.uviSignatureView.erase()
    }
    @IBAction func submitButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "This action cannot be reversed after submission.", preferredStyle: .alert)
        let dismissAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(dismissAction)
        let alertAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default, handler: {_ in self.showAgreementAlert()})
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAgreementAlert() {
        let alert = UIAlertController(title: "", message: "I, \(self.orderUserName), have read and understand the content of this agreement and pledge to undertake the following without any duress:\n\n1. I fully understand the content, nature effects and implications of the agreements, and\n\n2. I signed the agreement voluntarily without coersion from any party.", preferredStyle: .alert)
        let dismissAction: UIAlertAction = UIAlertAction(title: "Decline", style: .default, handler: nil)
        alert.addAction(dismissAction)
        let alertAction: UIAlertAction = UIAlertAction(title: "I Accept", style: .default, handler: {_ in self.uploadTheSignature()})
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func uploadTheSignature() {
        self.uviSignatureView.captureSignature();
        let signedImage = self.uviSignatureView.signatureImage("", position: CGPoint(x: 0, y: 0))
        NetworkLoader.sharedLoader.submitESign(String(self.orderUserID), signatureUIImage: signedImage) { (networkError, success) in
            if let networkError = networkError {
                self.showAlertMessage(errorTitle: networkError.networkLoaderErrorTitle, errorMessage: networkError.networkLoaderErrorBody)
            } else if success != nil {
                let alert = UIAlertController(title: "SUCCESSFUL!", message: "Your signature has been submitted\nYour One Time Passcode (OTP) has expiered.", preferredStyle: .alert)
                let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: {_ in self.performSegue(withIdentifier: "logoutFromEsign", sender: nil)})
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showAlertMessage(errorTitle: String, errorMessage: String) {
        let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: .alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
}
