//
//  RoundEdgeOrangeColoredButton.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 22/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class RoundEdgeOrangeColoredButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = 5
        layer.backgroundColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        tintColor = UIColor.white
    }
    
}
