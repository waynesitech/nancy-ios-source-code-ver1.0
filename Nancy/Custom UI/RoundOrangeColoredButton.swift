//
//  RoundOrangeColoredButton.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 17/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class RoundOrangeColoredButton: UIButton {
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.cornerRadius = 15
        layer.backgroundColor = UIColor(red: 247/255, green: 145/255, blue: 23/255, alpha: 1).cgColor
        layer.borderWidth = 1.0
        layer.borderColor = UIColor.white.cgColor
        tintColor = UIColor.white
    }
    
}
