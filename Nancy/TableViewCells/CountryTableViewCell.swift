//
//  CountryTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 27/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    func setupPhoneCode(country: Country) {
        countryNameLabel.text = country.name
        countryCodeLabel.text = country.phoneCode
    }
    
}
