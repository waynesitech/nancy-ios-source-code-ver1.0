//
//  TenancyAgreementTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 19/05/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit
import AlamofireImage

protocol NavigateToRenewTAProtocol : NSObjectProtocol {
    func didSelectRenewButton(_ orderID: String) -> Void
}

class TenancyAgreementTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var propertyImageView: UIImageView!
    @IBOutlet weak var referenceNumber: UILabel!
    @IBOutlet weak var propertyNameLabel: UILabel!
    @IBOutlet weak var tenancyAgreementStatusLabel: UILabel!
    @IBOutlet weak var eSignStatusLabel: UILabel!
    @IBOutlet weak var eStampStatusLabel: UILabel!
    weak var delegate: NavigateToRenewTAProtocol?
    
    
    var tenancyAgreement: TenancyAgreement! {
        didSet {
            updateUI()
        }
    }
    
    @IBAction func renewButtonPressed(_ sender: Any) {
        if delegate?.didSelectRenewButton != nil {
            delegate?.didSelectRenewButton(self.tenancyAgreement.order_id)
        }
    }
    
    func updateUI() {
        let placeholderImage = UIImage(named: "noPropertyImagePlaceholder")
        
        let filter = AspectScaledToFillSizeCircleFilter (
            size: self.propertyImageView.frame.size
        )
        
        let url = URL(string: tenancyAgreement.propertyImageURL)
        self.propertyImageView.af_setImage(
            withURL: url!,
            placeholderImage: placeholderImage,
            filter: filter
        )
        
        self.referenceNumber.text = "Ref NO: \(tenancyAgreement.referenceNumber)"
        self.propertyNameLabel.text = tenancyAgreement.getPropertyName()
        self.tenancyAgreementStatusLabel.text = "Tenancy Agreement: \(tenancyAgreement.tenancyAgreementStatus)"
        self.eStampStatusLabel.text = "E-Stamp: \(tenancyAgreement.eStampStatus)"
        
        if tenancyAgreement.eSignStatus == "MANUALLY_SIGNED" {
            self.eSignStatusLabel.text = "Manual Sign: Done"
        } else if tenancyAgreement.eSignStatus == "-" &&
            tenancyAgreement.tenancyAgreementStatus == "Done" {
            self.eSignStatusLabel.text = "E-Sign: Ready"
        } else if tenancyAgreement.eSignStatus == "PARTIAL_SIGNED" {
            self.eSignStatusLabel.text = "E-Sign: Pending"
        } else if tenancyAgreement.eSignStatus == "ALL_SIGNED" {
            self.eSignStatusLabel.text = "E-Sign: Done"
        } else {
            self.eSignStatusLabel.text = "E-Sign: -"
        }
    }
    
}
