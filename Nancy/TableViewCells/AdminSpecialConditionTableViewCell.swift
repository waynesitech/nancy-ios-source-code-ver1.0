//
//  AdminSpecialConditionTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 22/08/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

protocol AdminSpecialConditionTextViewProtocol : NSObjectProtocol {
    func textChanged(_ newText: String, thisIndex: Int, needsReloadCell: Bool) -> Void
}

class AdminSpecialConditionTableViewCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var indexLabel2: UILabel!
    @IBOutlet weak var originalLabel: UILabel!
    @IBOutlet weak var editTextView: UITextView!
    weak var delegate: AdminSpecialConditionTextViewProtocol?
    
    var thisText: String = ""
    var thisEditText: String = ""
    var thisIndex: Int = -1
    
    func updateUI() {
        indexLabel.text = "\(thisIndex)."
        indexLabel2.text = "\(thisIndex)."
        originalLabel.text = thisText
        editTextView.text = thisEditText
        editTextView.delegate = self
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let startHeight = textView.frame.size.height
        let calcHeight = textView.sizeThatFits(textView.frame.size).height
        let shouldReload = startHeight != calcHeight
        
        if delegate?.textChanged != nil {
            delegate?.textChanged(self.editTextView.text!, thisIndex: self.thisIndex, needsReloadCell: shouldReload)
        }
    }
}
