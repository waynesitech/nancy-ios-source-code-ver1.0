//
//  SpecialConditionTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 09/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

protocol AddNewTextProtocol : NSObjectProtocol {
    func addNewText(_ newText: String) -> Void
    func deleteThisText(_ thisIndex: Int) -> Void
    func textChanged(_ newText: String, thisIndex: Int) -> Void
}

class SpecialConditionTableViewCell: UITableViewCell {
    @IBOutlet weak var indexLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var addOrDeleteButton: UIButton!
    weak var delegate: AddNewTextProtocol?
    
    var thisText: String = ""
    var thisIndex: Int = -1
    var isEditable: Bool = false
    
    @IBAction func addOrDeleteButtonPressed(_ sender: AnyObject) {
        if !isEditable {
            if delegate?.deleteThisText != nil {
                delegate?.deleteThisText(thisIndex)
            }
        } else if (contentTextField.text?.characters.count)! > 0 {
            if delegate?.addNewText != nil {
                delegate?.addNewText(contentTextField.text!)
            }
        }
        updateUI()
    }
    
    func updateUI() {
        indexLabel.text = "\(thisIndex)."
        contentTextField.text = thisText
        contentTextField.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: UIControlEvents.editingChanged)
        if isEditable {
            addOrDeleteButton.setImage(UIImage(named: "addMoreButton"), for: .normal)
        } else {
            addOrDeleteButton.setImage(UIImage(named: "deleteThisButton"), for: .normal)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if delegate?.textChanged != nil {
            delegate?.textChanged(self.contentTextField.text!, thisIndex: self.thisIndex)
        }
    }
}
