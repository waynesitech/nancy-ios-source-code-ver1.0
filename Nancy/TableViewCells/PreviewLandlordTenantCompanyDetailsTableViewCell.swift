//
//  PreviewLandlordTenantCompanyDetailsTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class PreviewLandlordTenantCompanyDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyRegNoLabel: UILabel!
    @IBOutlet weak var personInChargeLabel: UILabel!
    @IBOutlet weak var icNoLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var landlordTenant: LandlordTenant = LandlordTenant()
    var title: String = ""
    
    func updateUI() {
        titleLabel.text = title
        companyNameLabel.text = landlordTenant.companyName
        companyRegNoLabel.text = landlordTenant.companyRegNum
        personInChargeLabel.text = landlordTenant.name
        icNoLabel.text = landlordTenant.icNum
        addressLabel.text = landlordTenant.address
        contactNumberLabel.text = landlordTenant.countryCode.phoneCode + landlordTenant.contactNum
        emailLabel.text = landlordTenant.email
    }
}
