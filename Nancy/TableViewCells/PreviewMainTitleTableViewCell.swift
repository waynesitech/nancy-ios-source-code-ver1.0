//
//  PreviewMainTitleTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class PreviewMainTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    
    func setupTitle(title: String) {
        mainTitleLabel.text = title
    }
    
}
