//
//  PreviewPropertyDetailsTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class PreviewPropertyDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var propertyPreviewImageView: UIImageView!
    @IBOutlet weak var propertyTypeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    func setupContent(propertyPreviewImage: UIImage, propertyType: String, address: String) {
        propertyPreviewImageView.image = propertyPreviewImage
        propertyPreviewImageView.layer.cornerRadius = propertyPreviewImageView.frame.size.width / 2
        propertyPreviewImageView.layer.masksToBounds = true
        propertyPreviewImageView.clipsToBounds = true
        propertyPreviewImageView.contentMode = .scaleAspectFill
        
        propertyTypeLabel.text = propertyType
        addressLabel.text = address
    }
    
}
