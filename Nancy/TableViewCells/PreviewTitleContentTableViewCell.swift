//
//  PreviewTitleContentTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 12/06/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

class PreviewTitleContentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    func setupContent(title: String, content: String) {
        contentLabel.text = content
        titleLabel.text = title
    }
    
}
