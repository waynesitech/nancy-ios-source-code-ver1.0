//
//  ExplanationTableViewCell.swift
//  Nancy
//
//  Created by Tan Ji Sheng on 14/07/2017.
//  Copyright © 2017 Lepro System Berhad. All rights reserved.
//

import UIKit

protocol PlayExplanationProtocol : NSObjectProtocol {
    func playAudio(_ audioURL: String, rowNumber: Int) -> Void
    func stopPlaying() -> Void
}

class ExplanationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var spacingBetweenNumberAndText: NSLayoutConstraint!
    @IBOutlet weak var explanationNumberWidth: NSLayoutConstraint!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var playStopButton: UIButton!
    var audioURL: String = ""
    var isPlaying: Bool = false
    var rowNumber: Int = -1
    weak var delegate: PlayExplanationProtocol?
    
    func setupContent(explanation: ExplanationAudio) {
        numberLabel.text = explanation.explanationNumber
        if explanation.explanationNumber == "" {
            explanationNumberWidth.constant = 0
            spacingBetweenNumberAndText.constant = 0
        } else {
            explanationNumberWidth.constant = 30
            spacingBetweenNumberAndText.constant = 10
        }
        contentLabel.text = explanation.explanationText
        self.audioURL = explanation.explanationURL
        if self.isPlaying {
            self.playStopButton.setImage(UIImage(named: "stopExplanationIcon"), for: .normal)
        } else {
            self.playStopButton.setImage(UIImage(named: "playExplanationIcon"), for: .normal)
        }
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        if !self.isPlaying {
            if delegate?.playAudio != nil {
                delegate?.playAudio(self.audioURL, rowNumber: self.rowNumber)
            }
        } else {
            if delegate?.stopPlaying != nil {
                delegate?.stopPlaying()
            }
        }
    }
}
